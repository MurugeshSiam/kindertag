# keep getters/setters in RotatingDrawable so that animations can still work.
-keepclassmembers class com.kindertag.kindertag.Utils.FloatingActionsMenu$RotatingDrawable {
   void set*(***);
   *** get*();
}
