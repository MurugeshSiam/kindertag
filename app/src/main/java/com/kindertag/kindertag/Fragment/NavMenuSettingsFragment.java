package com.kindertag.kindertag.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.SharePref;

public class NavMenuSettingsFragment extends Fragment {
 String localTag;
 SwitchCompat switchCompat;

 // This fragment used to show setting fragment
 public NavMenuSettingsFragment() {}
 @Override
 public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
 }

 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
  View rootView = inflater.inflate(R.layout.fragment_nav_menu_settings, container, false);
 // localTag=SharePref.getnotification(getActivity());

  final TextView title = (TextView) rootView.findViewById(R.id.txt_nav_menu_settings_title);
  final FrameLayout fl_toggle = (FrameLayout) rootView.findViewById(R.id.fl_nav_menu_settings_toggle);
  final View toggle_back = rootView.findViewById(R.id.bg_nav_menu_settings_toggle_back);
       switchCompat = (SwitchCompat) rootView.findViewById(R.id.switchButton);
        switchCompat.setChecked(SharePref.getnotification(getActivity()));


  final ImageView toggle_off = (ImageView) rootView.findViewById(R.id.img_nav_menu_settings_toggle_off);
  final ImageView toggle_on = (ImageView) rootView.findViewById(R.id.img_nav_menu_settings_toggle_on);
//  toggle_back.setBackgroundResource(localTag.equals("on")?R.drawable.toggle_on_back:R.drawable.toggle_off_back);
 // toggle_off.setVisibility(localTag.equals("off")?View.VISIBLE:View.GONE);
 // toggle_on.setVisibility(localTag.equals("on")?View.VISIBLE:View.GONE);
  switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
   @Override
   public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
    //boolean Sd=b;
     SharePref.setnotification(getActivity(),b);
    switchCompat.setChecked(SharePref.getnotification(getActivity()));

//Toast.makeText(getActivity(),"test",Toast.LENGTH_LONG).show();
   }
  });
  fl_toggle.setOnClickListener(new View.OnClickListener() {
   @Override
   public void onClick(View view) {
    view.setTag(view.getTag().equals("on")?"off":"on");
    localTag=view.getTag().toString();
   // SharePref.setnotification(getActivity(),localTag);

   /* if(view.getTag().equals("on") && SharePref.getnotification(getActivity()).equalsIgnoreCase("1")){
     SharePref.setnotification(getActivity(),"1");
     toggle_back.setBackgroundResource(R.drawable.toggle_on_back);
     toggle_off.setVisibility(View.GONE);
     toggle_on.setVisibility(View.VISIBLE);
     Toast.makeText(getActivity(),"ON",Toast.LENGTH_LONG).show();
    }else if(view.getTag().equals("off")) {
     SharePref.setnotification(getActivity(),"0");

     Toast.makeText(getActivity(),"OFF",Toast.LENGTH_LONG).show();
     toggle_back.setBackgroundResource(R.drawable.toggle_off_back);
     toggle_off.setVisibility(View.VISIBLE);
     toggle_on.setVisibility(View.GONE);
    }*/
   // toggle_back.setBackgroundResource(localTag.equals("on")?R.drawable.toggle_on_back:R.drawable.toggle_off_back);
   // toggle_off.setVisibility(localTag.equals("off")?View.VISIBLE:View.GONE);
  //  toggle_on.setVisibility(localTag.equals("on")?View.VISIBLE:View.GONE);
  }});

  return rootView;
 }
}
