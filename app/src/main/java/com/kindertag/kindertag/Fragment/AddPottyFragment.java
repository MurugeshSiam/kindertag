package com.kindertag.kindertag.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.HomeNewActivtyStudentsGridAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


// this fragment is used to add Potty in activities

public class AddPottyFragment extends Fragment {

    final String types[] = {"Share With", "Photo", "Notes"};
    LinearLayout ll_new_learning, ll_students;
    ArrayList<String> students = new ArrayList<String>();
    ArrayList<String> sel_students = new ArrayList<String>();
    ArrayList<String> sel_students_temp = new ArrayList<String>();
    EditText notes;
    String type;
    String potty_type = "";
    String potty_value = "";
    TextView photo, txt_time;
    ImageView img_photo;
    Uri imageUri = null;
    GridView grid_students;
    Button btn_diaper, btn_potty, btn_accident, btn_1, btn_2, btn_3;

    int a11 = 1, a22 = 0, a33 = 0;

    Calendar et = Calendar.getInstance();
    Calendar cc = Calendar.getInstance();
    View rootView;
    CheckBox checkbox;

    public AddPottyFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            type = getArguments().getString("type", "");
        }
        students = ((HomeActivity) getActivity()).getStudentsforActivities();

    }

    public void setUpTypes() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_potty, container, false);

        // find view from add potty
        ll_new_learning = (LinearLayout) rootView.findViewById(R.id.ll_new_learning);
        ll_students = (LinearLayout) rootView.findViewById(R.id.ll_new_learning_students);

        photo = (TextView) rootView.findViewById(R.id.txt_new_learning_photo);
        img_photo = (ImageView) rootView.findViewById(R.id.img_new_learning_photo);
        notes = (EditText) rootView.findViewById(R.id.edit_new_learning_notes);

        final Button btn_create = (Button) rootView.findViewById(R.id.btn_new_learning_create);
        txt_time = (TextView) rootView.findViewById(R.id.txt_time);
        checkbox = (CheckBox) rootView.findViewById(R.id.checkbox);

        //=====================================================================================
        btn_diaper = (Button) rootView.findViewById(R.id.btn_diaper);
        btn_potty = (Button) rootView.findViewById(R.id.btn_potty);
        btn_accident = (Button) rootView.findViewById(R.id.btn_accident);
        btn_1 = (Button) rootView.findViewById(R.id.btn_wet);
        btn_2 = (Button) rootView.findViewById(R.id.btn_bm);
        btn_3 = (Button) rootView.findViewById(R.id.btn_dry);

        //===================================================================================================
        //click listener on photo to choose action of photo
        // can take photo from camera and gallery

        photo.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Photo", R.drawable.ic_gallery + ";" + "  Choose Image"};
                ListAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, android.R.id.text1, items) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        //Use super class to create the View
                        View v = super.getView(position, convertView, parent);
                        TextView tv = (TextView) v.findViewById(android.R.id.text1);
                        //Put the image on the TextView
                        tv.setText(items[position].split(";")[1]);
                        tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                        return v;
                    }
                };

                // show dialog for camera or gallery showing

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Action");
                builder.setCancelable(true);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddPottyFragment.this.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                } else {
                                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
                                    camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                    startActivityForResult(camera, 3);
                                }
                                break;
                            case 1:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddPottyFragment.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                } else {
                                    Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    gallery.setType("image/*");
                                    startActivityForResult(gallery, 7);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                });
                builder.show();
            }
        });

        grid_students = (GridView) rootView.findViewById(R.id.grid_new_learning_students);
        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));

        setStudents(ll_students);

        // call addpotty web service on button click
        // pass all parameters regarding potty
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("potty_type",potty_type);
                Log.e("potty_value",potty_value);
                int isStaff=0;

                if (checkbox.isChecked())
                    isStaff=1;

                if (sel_students.size() == 0)
                    Toast.makeText(getActivity(), "Select atleast 1 Student", Toast.LENGTH_SHORT).show();
                else {
                    String student_ids = getIds(sel_students);
                    Log.i("sel_cat", "");
                    RequestParams params = new RequestParams();
                    params.add("room_id", ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                    params.add("student_id", student_ids);
                    params.add("activity_date", txt_time.getText().toString().replace("Time : ", ""));
                    params.add("isstaff", isStaff+"");
                    params.add("potty_type", potty_type);
                    params.add("potty_value", potty_value);
                    params.add("note", notes.getText().toString());
                    params.add("photo", Common.genrateByteArrayString(getActivity(), imageUri));
                    params.add("activity_type", "potty");
                    String json = "{" + "\"note\":\"" + notes.getText().toString() + "\"" +"\"potty_type\":\"" + potty_type + "\"" +"\"potty_value\":\"" + potty_value + "\"" + "}";
                    Log.i("json", json);
                    Log.e("####", params.toString());
                    Common.callWebService((HomeActivity) getActivity(), Common.wb_addnapactivity, params, "AddPotty" + "%" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + ";" + type + ";" + ((HomeActivity) getActivity()).toString(sel_students) + ";" + json + ";" + "photos" + ";" + "Staff only");
                }
            }
        });

        //==================================================================================
        Date d = new Date();
        CharSequence s = DateFormat.format("yyyy-MM-dd HH:mm:ss", d.getTime());
        txt_time.setText("Time : " + s);

        // show DatePickerDialog and select date
        // and set selected date to txt_time
        txt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int MM, int dd) {
                        et.set(yyyy, MM, dd);
                        TimePickerDialog tpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int HH, int mm) {
                                et.set(Calendar.HOUR_OF_DAY, HH);
                                et.set(Calendar.MINUTE, mm);
                                et.set(Calendar.SECOND, 0);
                                txt_time.setText("Time : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(et.getTime()));
                            }
                        }, et.get(Calendar.HOUR_OF_DAY), et.get(Calendar.MINUTE), true);
                        tpd.setTitle("Set Time");
                        tpd.setCancelable(false);
                        tpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        tpd.show();
                    }
                }, et.get(Calendar.YEAR), et.get(Calendar.MONTH), et.get(Calendar.DATE));
                dpd.setTitle("Set Date");
                dpd.setCancelable(false);
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dpd.getDatePicker().setMaxDate(cc.getTimeInMillis());
                dpd.show();
            }
        });
        //====================================================================
        btn_diaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diperView();
                potty_value = "Wet";
            }
        });
        btn_potty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pottyView();
                potty_value = "Pee";
            }
        });
        btn_accident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accidentView();
                potty_value = "Pee";
            }
        });

        potty_type = "Diaper";
        potty_value = "Wet";
        diperView();
        subView();

        return rootView;
    }

    public void subView() {
        btn_1.setBackgroundResource(R.drawable.btn_fill_back);
        btn_1.setTextColor(getResources().getColor(R.color.white));

        btn_2.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_2.setBackgroundResource(R.drawable.btn_empty_back);
        btn_3.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_3.setBackgroundResource(R.drawable.btn_empty_back);

        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val=btn_1.getText().toString();
                if (a11 == 1) {
                    a11 = 0;
                    potty_value = potty_value.replace(val, "");
                    btn_1.setBackgroundResource(R.drawable.btn_empty_back);
                    btn_1.setTextColor(getResources().getColor(R.color.thene_blue));
                } else {
                    a11 = 1;
                    potty_value = potty_value + " "+val;
                    btn_1.setBackgroundResource(R.drawable.btn_fill_back);
                    btn_1.setTextColor(getResources().getColor(R.color.white));

                }
            }
        });

        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val=btn_2.getText().toString();
                if (a22 == 1) {
                    a22 = 0;
                    potty_value = potty_value.replace(val, "");
                    btn_2.setBackgroundResource(R.drawable.btn_empty_back);
                    btn_2.setTextColor(getResources().getColor(R.color.thene_blue));
                } else {
                    a22 = 1;
                    potty_value = potty_value + " "+val;
                    btn_2.setBackgroundResource(R.drawable.btn_fill_back);
                    btn_2.setTextColor(getResources().getColor(R.color.white));

                }
            }
        });

        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val=btn_3.getText().toString();
                if (a33 == 1) {
                    a33 = 0;
                    potty_value = potty_value.replace(val, "");
                    btn_3.setBackgroundResource(R.drawable.btn_empty_back);
                    btn_3.setTextColor(getResources().getColor(R.color.thene_blue));
                } else {
                    a33 = 1;
                    potty_value = potty_value + " "+val;
                    btn_3.setBackgroundResource(R.drawable.btn_fill_back);
                    btn_3.setTextColor(getResources().getColor(R.color.white));

                }
            }
        });
    }

    // show dialperivew
    public void diperView() {
        btn_diaper.setBackgroundResource(R.drawable.btn_fill_back);
        btn_diaper.setTextColor(getResources().getColor(R.color.white));

        btn_potty.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_potty.setBackgroundResource(R.drawable.btn_empty_back);
        btn_accident.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_accident.setBackgroundResource(R.drawable.btn_empty_back);

        btn_3.setVisibility(View.VISIBLE);
        btn_1.setText("Wet");
        btn_2.setText("BM");
        btn_3.setText("Dry");

        potty_type = "Diaper";
        subView();
    }

    // show potty view
    // show 3 buttons
    public void pottyView() {
        btn_potty.setBackgroundResource(R.drawable.btn_fill_back);
        btn_potty.setTextColor(getResources().getColor(R.color.white));

        btn_diaper.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_diaper.setBackgroundResource(R.drawable.btn_empty_back);
        btn_accident.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_accident.setBackgroundResource(R.drawable.btn_empty_back);

        btn_3.setVisibility(View.VISIBLE);
        btn_1.setText("Pee");
        btn_2.setText("BM");
        btn_3.setText("Tried");

        potty_type = "Potty";
        subView();
    }

    // show accident view
    public void accidentView() {
        btn_accident.setBackgroundResource(R.drawable.btn_fill_back);
        btn_accident.setTextColor(getResources().getColor(R.color.white));

        btn_diaper.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_diaper.setBackgroundResource(R.drawable.btn_empty_back);
        btn_potty.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_potty.setBackgroundResource(R.drawable.btn_empty_back);

        btn_3.setVisibility(View.GONE);
        btn_1.setText("Pee");
        btn_2.setText("BM");

        potty_type = "Accident";
        subView();
    }

    // get ids of selected students

    public String getIds(ArrayList<String> sel_students) {
        String str = "";
        for (int i = 0; i < sel_students.size(); i++)
            str += "," + sel_students.get(i).split(";")[0];
        return str.length() > 0 ? str.substring(1) : str;
    }

    public void reset() {
        getActivity().invalidateOptionsMenu();
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add " + type);
        grid_students.setVisibility(View.GONE);
        ll_new_learning.setVisibility(View.VISIBLE);
        setStudents(ll_students);
    }

    public void set() {
        sel_students.clear();
        for (int i = 0; i < sel_students_temp.size(); i++)
            sel_students.add(sel_students_temp.get(i));
        reset();
    }

//    public void refresh() {
//        this.foods = ((HomeActivity) getActivity()).foods;
//        if (sel_type == 0)
//            list_new_foods_vals.setAdapter(new FoodValsAdapter(getActivity(), search.getText().toString(), sel_type == 0, foods, sel_cat));
//    }
//
//    public void refreshBottol() {
//        this.bottols = ((HomeActivity) getActivity()).bottols;
//        if (sel_type_bottol == 0)
//            list_new_bottol_vals.setAdapter(new BottolValsAdapter(getActivity(), search.getText().toString(), sel_type_bottol == 0, bottols, sel_bottol));
//    }

    public void refresh_students(String search) {
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, search,true));
    }

    // set all students to gridview

    public void setStudents(final LinearLayout ll) {
        ll.setVisibility(View.VISIBLE);
        ll.removeAllViews();
        final int max = sel_students.size() > 5 ? 7 : sel_students.size() + 2;
        for (int i = 0; i < max; i++) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40), getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40));
            llp.setMargins(i > 0 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5), i < max - 1 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5));
            if (i == max - 1) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTypeface(((HomeActivity) getActivity()).tf_material);
                txt.setTextColor(Color.WHITE);
                txt.setText(R.string.icon_plus);
                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ll_new_learning.setVisibility(View.GONE);
                        grid_students.setVisibility(View.VISIBLE);
                        sel_students_temp.clear();
                        for (int i = 0; i < sel_students.size(); i++)
                            sel_students_temp.add(sel_students.get(i));
                        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
                        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Select Student");
                        getActivity().invalidateOptionsMenu();
                    }
                });
                ll.addView(txt);
            } else if (i == max - 2) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                //txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTextColor(Color.BLACK);
                txt.setText((sel_students.size() - (max - 2)) + "+");
                txt.setVisibility(sel_students.size() > 5 ? View.VISIBLE : View.GONE);
                ll.addView(txt);
            } else {
                final ImageView img = new ImageView(getActivity());
                img.setLayoutParams(llp);
                img.setBackgroundResource(R.drawable.black_circle_back2);
                Glide.with(getActivity()).load(sel_students.get(i).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        if (getActivity() != null && img != null) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            img.setImageDrawable(circularBitmapDrawable);
                        }
                    }
                });
                ll.addView(img);
            }
        }
    }
    public void selectCheckIn(boolean selectCheckin){
        if(selectCheckin){
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                if(students.get(i).split(";")[4].equalsIgnoreCase("true")){
                    sel_students_temp.add(students.get(i));

                }
        } else

            sel_students_temp.clear();
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",false));
    }
    // select all students

    public void setSelectionAll(boolean isSelectAll) {
        if (isSelectAll) {
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                sel_students_temp.add(students.get(i));
        } else
            sel_students_temp.clear();
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
    }

    // when capture photo or select photo then reach here onActivity result
    // get uri from data and set to image

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 3) {
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            imageUri = Uri.fromFile(photo);
            img_photo.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
        } else if (resultCode == Activity.RESULT_OK && requestCode == 7 && data != null) {
            imageUri = data.getData();
            img_photo.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
        }
    }

    // when permission granted then reach in request permission
    // according to permission fire that action

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Camera Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            startActivityForResult(camera, 3);
        } else if (requestCode == 3 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Camera Permission Not Granted!", Toast.LENGTH_SHORT).show();

        if (requestCode == 7 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Gallery Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, 7);
        } else if (requestCode == 7 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Gallery Permission Not Granted!", Toast.LENGTH_SHORT).show();
    }

}
