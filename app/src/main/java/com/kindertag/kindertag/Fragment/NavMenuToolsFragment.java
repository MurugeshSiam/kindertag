package com.kindertag.kindertag.Fragment;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

public class NavMenuToolsFragment extends Fragment {

    // This fragment used to show tools
    // means show all students, teacher and rooms
    ArrayList<String> counts = new ArrayList<String>();

    public NavMenuToolsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        counts = getArguments().getStringArrayList("tools");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nav_menu_tools, container, false);
        LinearLayout ll_count = (LinearLayout) rootView.findViewById(R.id.ll_nav_menu_tools_count);
        Button room_chk = (Button) rootView.findViewById(R.id.btn_nav_menu_tools_room_check);

        setCount(ll_count);

        room_chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestParams params = new RequestParams();
                params.add("user_id", ((HomeActivity) getActivity()).uid);
                Common.callWebService((HomeActivity) getActivity(), Common.wb_getratio, params, "Ratio");
            }
        });

        return rootView;
    }


    // set count to all textveiw
    public void setCount(LinearLayout ll) {
        ll.removeAllViews();
        for (int i = 0; i < counts.size(); i++) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.view_home_tab, null);
            TextView count = (TextView) view.findViewById(R.id.txt_home_tab_icon);
            TextView txt = (TextView) view.findViewById(R.id.txt_home_tab_name);
            count.setLayoutParams(new LinearLayout.LayoutParams(160, 160));
            count.setBackgroundResource(R.drawable.black_circle_count);
            count.setTextColor(Color.WHITE);
            count.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
            count.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            txt.setTextColor(Color.BLACK);
            txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            txt.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            count.setText(counts.get(i).toString().split(";")[1]);
            txt.setText(counts.get(i).toString().split(";")[0]);
            ll.addView(view, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
        }
    }
}
