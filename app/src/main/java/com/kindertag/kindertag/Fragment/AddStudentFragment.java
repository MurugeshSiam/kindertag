package com.kindertag.kindertag.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.kindertag.kindertag.Utils.KinderActivity.uid;

// this fragment is used to add student
public class AddStudentFragment extends Fragment {
    Calendar cc = Calendar.getInstance();
    Calendar ed = Calendar.getInstance();
    ImageView img_profile;
    EditText name, father_name, father_email, father_mobile, mother_name, mother_email, mother_mobile, guardian_name, guardian_mobile, emergency_name, emergency_mobile;
    EditText allergies, medication, doctor_name, doctor_mobile, address1, address2, city, state, country, zip, notes;
    TextView birthdate;
    Spinner room;
    String vals = "", utype = "";
    Uri imageUri = null;

    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> arrayListid = new ArrayList<>();
    ArrayList<String> arrayListname = new ArrayList<>();

    public AddStudentFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        cc.add(cc.DATE, -1);
        ed.add(ed.DATE, -1);
        vals = (getArguments() != null && getArguments().getString("vals", "") != null && !getArguments().getString("vals", "").equals("")) ? getArguments().getString("vals", "") : "";
        utype = ((HomeActivity) getActivity()).getUserType();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_student, container, false);

        // find views declared in xml fragment_add_student
        img_profile = (ImageView) rootView.findViewById(R.id.img_new_student_photo);
        name = (EditText) rootView.findViewById(R.id.edit_new_student_name);
        birthdate = (TextView) rootView.findViewById(R.id.txt_new_student_date);
        father_name = (EditText) rootView.findViewById(R.id.edit_new_student_father_name);
        father_email = (EditText) rootView.findViewById(R.id.edit_new_student_father_email);
        father_mobile = (EditText) rootView.findViewById(R.id.edit_new_student_father_number);
        mother_name = (EditText) rootView.findViewById(R.id.edit_new_student_mother_name);
        mother_email = (EditText) rootView.findViewById(R.id.edit_new_student_mother_email);
        mother_mobile = (EditText) rootView.findViewById(R.id.edit_new_student_mother_number);
        guardian_name = (EditText) rootView.findViewById(R.id.edit_new_student_guardian_name);
        guardian_mobile = (EditText) rootView.findViewById(R.id.edit_new_student_guardian_mobile);
        emergency_name = (EditText) rootView.findViewById(R.id.edit_new_student_emergency_contact_name);
        emergency_mobile = (EditText) rootView.findViewById(R.id.edit_new_student_emergency_contact_mobile);
        allergies = (EditText) rootView.findViewById(R.id.edit_new_student_allergies);
        medication = (EditText) rootView.findViewById(R.id.edit_new_student_medication);
        doctor_name = (EditText) rootView.findViewById(R.id.edit_new_student_doctor_name);
        doctor_mobile = (EditText) rootView.findViewById(R.id.edit_new_student_doctor_number);
        address1 = (EditText) rootView.findViewById(R.id.edit_new_student_address1);
        address2 = (EditText) rootView.findViewById(R.id.edit_new_student_address2);
        city = (EditText) rootView.findViewById(R.id.edit_new_student_city);
        state = (EditText) rootView.findViewById(R.id.edit_new_student_state);
        country = (EditText) rootView.findViewById(R.id.edit_new_student_country);
        zip = (EditText) rootView.findViewById(R.id.edit_new_student_zip);
        notes = (EditText) rootView.findViewById(R.id.edit_new_student_notes);
        final TextView txt_section3 = (TextView) rootView.findViewById(R.id.txt_new_student_section_3);
        //final View line_section3 = rootView.findViewById(R.id.view_new_student_section_3);
        room = (Spinner) rootView.findViewById(R.id.spin_new_student_room);
        final Button btn_add = (Button) rootView.findViewById(R.id.btn_new_student_add);

        birthdate.setBackgroundDrawable(new EditText(getActivity()).getBackground());
String parent_type=Common.chkNull(vals, ";", 28, "");
//Toast.makeText(getContext(),parent_type,Toast.LENGTH_LONG).show();
    /*    father_name.setVisibility(!utype.equals("Parent") ? View.VISIBLE : View.GONE);
        father_email.setVisibility(!utype.equals("Parent") ? View.VISIBLE : View.GONE);
        father_mobile.setVisibility(!utype.equals("Parent") ? View.VISIBLE : View.GONE);*/

        father_name.setVisibility(parent_type.equals("1") ? View.VISIBLE : View.GONE);
        father_email.setVisibility(parent_type.equals("1") ? View.VISIBLE : View.GONE);
        father_mobile.setVisibility(parent_type.equals("1") ? View.VISIBLE : View.GONE);

        mother_name.setVisibility(parent_type.equals("2") ? View.VISIBLE : View.GONE);
        mother_email.setVisibility(parent_type.equals("2") ? View.VISIBLE : View.GONE);
        mother_mobile.setVisibility(parent_type.equals("2") ? View.VISIBLE : View.GONE);
        txt_section3.setVisibility(vals.equals("") ? View.VISIBLE : View.GONE);
        room.setVisibility(vals.equals("") ? View.VISIBLE : View.GONE);

        btn_add.setText(vals.equals("") ? "Add Student" : "Update");
        btn_add.setVisibility(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")) ? View.VISIBLE : View.GONE);

        //Set Edit Text Vals Here
        if (vals != null && vals.length() > 0) {
            Glide.with(getActivity()).load(vals.split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_profile) {
                @Override
                protected void setResource(Bitmap resource) {
                    if (getActivity() != null && resource != null && img_profile != null) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img_profile.setImageDrawable(circularBitmapDrawable);
                    }
                }
            });
        }

        name.setText(Common.chkNull(vals, ";", 1, ""));
        birthdate.setText(Common.chkNull(vals, ";", 2, ""));
        father_name.setText(Common.chkNull(vals, ";", 6, ""));
        father_email.setText(Common.chkNull(vals, ";", 7, ""));
        father_mobile.setText(Common.chkNull(vals, ";", 8, ""));
        mother_name.setText(Common.chkNull(vals, ";", 9, ""));
        mother_email.setText(Common.chkNull(vals, ";", 10, ""));
        mother_mobile.setText(Common.chkNull(vals, ";", 8, ""));
        guardian_name.setText(Common.chkNull(vals, ";", 12, ""));
        guardian_mobile.setText(Common.chkNull(vals, ";", 13, ""));
        emergency_name.setText(Common.chkNull(vals, ";", 14, ""));
        emergency_mobile.setText(Common.chkNull(vals, ";", 15, ""));
        allergies.setText(Common.chkNull(vals, ";", 16, ""));
        medication.setText(Common.chkNull(vals, ";", 17, ""));
        doctor_name.setText(Common.chkNull(vals, ";", 18, ""));
        doctor_mobile.setText(Common.chkNull(vals, ";", 19, ""));
        address1.setText(Common.chkNull(vals, ";", 20, ""));
        address2.setText(Common.chkNull(vals, ";", 21, ""));
        city.setText(Common.chkNull(vals, ";", 22, ""));
        state.setText(Common.chkNull(vals, ";", 23, ""));
        country.setText(Common.chkNull(vals, ";", 24, ""));
        zip.setText(Common.chkNull(vals, ";", 25, ""));
        notes.setText(Common.chkNull(vals, ";", 26, ""));

        name.addTextChangedListener(clear);
        birthdate.addTextChangedListener(clear);
        father_name.addTextChangedListener(clear);
        father_email.addTextChangedListener(clear);
        father_mobile.addTextChangedListener(clear);
        mother_name.addTextChangedListener(clear);
        mother_email.addTextChangedListener(clear);
        mother_mobile.addTextChangedListener(clear);
        guardian_name.addTextChangedListener(clear);
        guardian_mobile.addTextChangedListener(clear);
        emergency_name.addTextChangedListener(clear);
        emergency_mobile.addTextChangedListener(clear);
        allergies.addTextChangedListener(clear);
        medication.addTextChangedListener(clear);
        doctor_name.addTextChangedListener(clear);
        doctor_mobile.addTextChangedListener(clear);
        address1.addTextChangedListener(clear);
        address2.addTextChangedListener(clear);
        city.addTextChangedListener(clear);
        state.addTextChangedListener(clear);
        country.addTextChangedListener(clear);
        zip.addTextChangedListener(clear);
        notes.addTextChangedListener(clear);

        img_profile.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        name.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        birthdate.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        father_name.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        father_email.setEnabled(vals.equals(""));
        father_mobile.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        mother_name.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        mother_email.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        mother_mobile.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        guardian_name.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        guardian_mobile.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        emergency_name.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        emergency_mobile.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        allergies.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        medication.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        doctor_name.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        doctor_mobile.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        address1.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        address2.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        city.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        state.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        country.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        zip.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        notes.setEnabled(!(utype.equalsIgnoreCase("Teacher") && !vals.equalsIgnoreCase("")));
        //room.setEnabled(!(utype.equalsIgnoreCase("Owner") && !vals.equalsIgnoreCase("")));

        arrayList.clear();
        arrayListid.clear();
        arrayListname.clear();

        arrayList.addAll(((HomeActivity) getActivity()).getRooms());

        for (int i = 0; i < arrayList.size(); i++) {
            String[] arr = arrayList.get(i).split(";");
            arrayListid.add(arr[0]);
            arrayListname.add(arr[1]);
        }

        if (vals.equalsIgnoreCase("")) {

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arrayListname);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);

            room.setAdapter(arrayAdapter);

        }

        // select birth date from DatePickerDialog

        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int MM, int dd) {
                        ed.set(yyyy, MM, dd);
                        ((TextView) view).setText(new SimpleDateFormat("yyyy-MM-dd").format(ed.getTime()));
                    }
                }, ed.get(Calendar.YEAR), ed.get(Calendar.MONTH), ed.get(Calendar.DATE));
                dpd.setTitle("Set Student  BirthDate");
                dpd.setCancelable(false);
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                if (((TextView) view).getText().length() > 0)
                    dpd.setButton(DialogInterface.BUTTON_NEUTRAL, "Remove", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((TextView) view).setText("");
                            dialogInterface.dismiss();
                        }
                    });
                dpd.getDatePicker().setMaxDate(cc.getTimeInMillis());
                dpd.show();

            }
        });

        //click listener on img_profile to choose action of photo
        // can take photo from camera and gallery

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Photo", R.drawable.ic_gallery + ";" + "  Choose Image"};
                ListAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, android.R.id.text1, items) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        //Use super class to create the View
                        View v = super.getView(position, convertView, parent);
                        TextView tv = (TextView) v.findViewById(android.R.id.text1);
                        //Put the image on the TextView
                        tv.setText(items[position].split(";")[1]);
                        tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                        return v;
                    }
                };

                // show dialog for camera or gallery showing

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Action");
                builder.setCancelable(true);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddStudentFragment.this.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                } else {
                                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
                                    camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                    AddStudentFragment.this.startActivityForResult(camera, 3);
                                }
                                break;
                            case 1:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddStudentFragment.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                } else {
                                    Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    gallery.setType("image/*");
                                    AddStudentFragment.this.startActivityForResult(gallery, 7);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                });
                builder.show();
            }
        });


        //call addstudent webservice
       // and pass all parameters regarding students
        notes.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_DONE) {
                    AddStudent();
                    return true;
                }

                return false;
            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddStudent();
            }
        });

        return rootView;
    }

public void AddStudent(){
    clearAllErrors();
    String Father_Name=father_name.getText().toString();
    String Mother_Name=mother_name.getText().toString();
    String Father_Email=father_email.getText().toString();
    String Mother_Email=mother_email.getText().toString();
    String Father_Mobile=father_mobile.getText().toString();
    String Mother_Mobile=mother_mobile.getText().toString();
    if(Father_Name.length()>0 && Father_Email.length()>0 && Father_Mobile.length()>0 ||Mother_Name.length()>0 &&
            Mother_Email.length()>0 && Mother_Mobile.length()>0){
        //    Toast.makeText(getContext(),"Thank U",Toast.LENGTH_LONG).show();
        if (Common.validate(name, 2, true) && Common.validate(birthdate, 10, true) &&
                Common.validate(father_name, 2, false) && Common.validate(father_email, 6, false) && Common.validate(father_mobile, 10, false) &&
                Common.validate(mother_name, 2, false) && Common.validate(mother_email, 6, false) && Common.validate(mother_mobile, 10, false) &&
                Common.validate(guardian_name, 2, false) && Common.validate(guardian_mobile, 10, false) &&
                Common.validate(emergency_name, 2, false) && Common.validate(emergency_mobile, 10, false) &&
                Common.validate(allergies, 1, false) && Common.validate(medication, 1, false) &&
                Common.validate(doctor_name, 2, false) && Common.validate(doctor_mobile, 10, false) &&
                Common.validate(address1, 1, false) && Common.validate(address2, 1, false) &&
                Common.validate(city, 1, false) && Common.validate(state, 1, false) && Common.validate(country, 1, false) && Common.validate(zip, 1, false) &&
                Common.validate(notes, 1, false)) {
            RequestParams params = new RequestParams();
            if (vals.equals(""))
                params.add("room_id", arrayListid.get(room.getSelectedItemPosition()));
            else if (!vals.equals(""))
                params.add("student_id", vals.split(";")[0]);
            params.add("studentName", name.getText().toString());
            params.add("birthdate", birthdate.getText().toString());
            if (imageUri != null)
                params.add("photo", Common.genrateByteArrayString(getActivity(), imageUri));
            params.add("parentName", father_name.getText().toString());
            if (vals.equals(""))
                params.add("parentEmail", father_email.getText().toString());
            params.add("parentPhone", mother_mobile.getText().toString());
            params.add("motherName", mother_name.getText().toString());
            params.add("motherEmail", mother_email.getText().toString());
            params.add("motherPhone", mother_mobile.getText().toString());
            params.add("gurdianName", guardian_name.getText().toString());
            params.add("gurdianPhone", guardian_mobile.getText().toString());
            params.add("contactName", emergency_name.getText().toString());
            params.add("contactPhone", emergency_mobile.getText().toString());
            params.add("allergy", allergies.getText().toString());
            params.add("medication", medication.getText().toString());
            params.add("doctorName", doctor_name.getText().toString());
            params.add("doctorPhone", doctor_mobile.getText().toString());
            params.add("address", address1.getText().toString());
            params.add("address2", address2.getText().toString());
            params.add("city", city.getText().toString());
            params.add("state", state.getText().toString());
            params.add("country", country.getText().toString());
            params.add("zipcode", zip.getText().toString());
            params.add("notes", notes.getText().toString());

            if(!vals.equals("")){
                params.add("user_id",  uid);
                params.add("parent_type",  Common.chkNull(vals, ";", 28, "0"));
            }


            String ss = Common.chkNull(vals, ";", 0, "id") + ";" + name.getText().toString() + ";" + birthdate.getText().toString() + ";" + (imageUri != null ? "photos" : Common.chkNull(vals, ";", 3, "null")) + ";" + (!vals.equals("") ? vals.split(";")[4] : "false") + ";" +
                    Common.chkNull(vals, ";", 5, "0") + ";" + father_name.getText().toString() + ";" + father_email.getText().toString() + ";" + father_mobile.getText().toString() + ";" +
                    mother_name.getText().toString() + ";" + mother_email.getText().toString() + ";" + mother_mobile.getText().toString() + ";" +
                    guardian_name.getText().toString() + ";" + guardian_mobile.getText().toString() + ";" +
                    emergency_name.getText().toString() + ";" + emergency_mobile.getText().toString() + ";" +
                    allergies.getText().toString() + ";" + medication.getText().toString() + ";" +
                    doctor_name.getText().toString() + ";" + doctor_mobile.getText().toString() + ";" +
                    address1.getText().toString() + ";" + address2.getText().toString() + ";" +
                    city.getText().toString() + ";" + state.getText().toString() + ";" + country.getText().toString() + ";" + zip.getText().toString() + ";" +
                    notes.getText().toString() + ";" + Common.chkNull(vals, ";", 27, "0") + ";" + Common.chkNull(vals, ";", 28, "0");
            Common.callWebService((HomeActivity) getActivity(), vals.equals("") ? Common.wb_addstudent : Common.wb_editstudent, params, (vals.equals("") ? "AddStudent" : "EditStudent") + "%" + ss + "%" + (vals.equals("") ? room.getSelectedItem().toString().split(";")[0] : ""));
            //}
        }
    }else {
        Toast.makeText(getContext(),"Please Enter The Mandatory Details",Toast.LENGTH_LONG).show();
    }
}
    // when capture photo or select photo then reach here onActivity result
    // get uri from data and set to image


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 3) {
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            imageUri = Uri.fromFile(photo);
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_profile) {
                @Override
                protected void setResource(Bitmap resource) {
                    if (resource != null && img_profile != null) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img_profile.setImageDrawable(circularBitmapDrawable);
                    }
                }
            });
        } else if (resultCode == Activity.RESULT_OK && requestCode == 7 && data != null) {
            imageUri = data.getData();
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_profile) {
                @Override
                protected void setResource(Bitmap resource) {
                    if (resource != null && img_profile != null) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img_profile.setImageDrawable(circularBitmapDrawable);
                    }
                }
            });
        }
    }

    // when permission granted then reach in request permission
    // according to permission fire that action

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Camera Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            AddStudentFragment.this.startActivityForResult(camera, 3);
        } else if (requestCode == 3 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Camera Permission Not Granted!", Toast.LENGTH_SHORT).show();

        if (requestCode == 7 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Gallery Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            AddStudentFragment.this.startActivityForResult(gallery, 7);
        } else if (requestCode == 7 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Gallery Permission Not Granted!", Toast.LENGTH_SHORT).show();
    }

    void clearAllErrors() {
        name.setError(null);
        birthdate.setError(null);
        birthdate.setFocusableInTouchMode(false);
        father_name.setError(null);
        father_email.setError(null);
        father_mobile.setError(null);
        mother_name.setError(null);
        mother_email.setError(null);
        mother_mobile.setError(null);
        guardian_name.setError(null);
        guardian_mobile.setError(null);
        emergency_name.setError(null);
        emergency_mobile.setError(null);
        allergies.setError(null);
        medication.setError(null);
        doctor_name.setError(null);
        doctor_mobile.setError(null);
        address1.setError(null);
        address2.setError(null);
        city.setError(null);
        state.setError(null);
        country.setError(null);
        zip.setError(null);
        notes.setError(null);
    }

    // when enter text in edittext in then clear all set errors on edittext
    final TextWatcher clear = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            clearAllErrors();
        }
    };

}
