package com.kindertag.kindertag.Fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.RecyclerAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.GPSTracker;
import com.kindertag.kindertag.module.New_items;
import com.loopj.android.http.RequestParams;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.kindertag.kindertag.Utils.MyApplication.TAG;


// This fragment is used for Add event
public class AddEventFragment extends Fragment implements LocationListener {
    Calendar cc = Calendar.getInstance();
    Calendar ed = Calendar.getInstance();
    Calendar et = Calendar.getInstance();
    EditText title, desc;
    TextView date, time;
    TextView txt_select_room, ok, cancel;
    //    MultiSelectionSpinner spin_rooms;
    AutoCompleteTextView location;
    Geocoder geocoder;
    List<Address> addresses;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    ArrayList<New_items> arr_name = new ArrayList<>();
    ArrayList<String> arr_ids = new ArrayList<>();
    public static List<String> arrindices = new ArrayList<>();

    List<Integer> arr_allselect = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Dialog dialog;
    String ids = "";
    String names = "";

    public AddEventFragment() {
    }

    int k = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        cc.add(cc.DATE, 1);
        ed.add(ed.DATE, 1);
        et.add(et.DATE, 1);
    }
    LocationManager locationManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_event, container, false);

        // find view write in xml named as fragment_add_event
        title = (EditText) rootView.findViewById(R.id.edit_new_event_title);
        desc = (EditText) rootView.findViewById(R.id.edit_new_event_desc);
        date = (TextView) rootView.findViewById(R.id.txt_new_event_date);
        time = (TextView) rootView.findViewById(R.id.txt_new_event_time);
        location = (AutoCompleteTextView) rootView.findViewById(R.id.autotxt_new_event_location);
        txt_select_room = (TextView) rootView.findViewById(R.id.txt_select_room);
        try {
            GPSTracker gpsTracker = new GPSTracker(getActivity());
            if (gpsTracker.canGetLocation())
            {
                double latitude=gpsTracker.getLatitude();
                double longitude=gpsTracker.getLongitude();
                geocoder = new Geocoder(getContext(), Locale.getDefault());

                addresses = geocoder.getFromLocation(13.091047,80.2837313, 1);
                String city = addresses.get(0).getLocality();
                location.setText(city);

            }else {
                displayLocationSettingsRequest(getContext());
            }

            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Button btn_create = (Button) rootView.findViewById(R.id.btn_new_event_create);

        time.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        date.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        txt_select_room.setBackgroundDrawable(new EditText(getActivity()).getBackground());

        showdialog();
        // To Add All Rooms Option (Now Commented)

        // when click on select room fragment show rooms dialog
        txt_select_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });


        // get selected rooms id when click on dialog ok button
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
                ids = "";
                names = "";
                for (int i = 0; i < arr_name.size(); i++) {
                    if (arr_name.get(i).ischeck == 1) {
                        if (ids.equalsIgnoreCase("")) {
                            ids = arr_name.get(i).id;
                        } else {
                            ids = ids + "," + arr_name.get(i).id;
                        }

                        names = names + "," + arr_name.get(i).name;
                    }
                }
if(names.length()==0){

    txt_select_room.setHint("Select Room");
txt_select_room.setText("");
}else {
                    String allRoom = names.split(",")[1];
                    if(allRoom.equalsIgnoreCase("All Rooms")){
                        txt_select_room.setText("All Rooms");

                    }else {
                        txt_select_room.setText(names.substring(1, names.length()));

                    }

}
             //   Log.e("ids", ids.substring(1, ids.length()));
            }
        });

        // when click on cancel hide dialog
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });

        Log.e("utype", ((HomeActivity) getActivity()).user_type);
        final ArrayList<String> rooms = ((HomeActivity) getActivity()).getRooms();
        if (((HomeActivity) getActivity()).user_type.equalsIgnoreCase("Owner")/* && !rooms.contains("-1;All Rooms")*/) {

            if (!rooms.contains("-1;All Rooms"))
                rooms.add(0, "-1;All Rooms");

            for (int i = 0; i < rooms.size(); i++) {

                String arr[] = rooms.get(i).split(";");
                New_items new_items = new New_items();
                new_items.id = arr[0];
                new_items.name = arr[1];

                arr_name.add(new_items);
            }

            // set rooms to recyclerview
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(new RecyclerAdapter(getActivity(), arr_name));
        } else if (((HomeActivity) getActivity()).user_type.equalsIgnoreCase("Teacher")) {

            rooms.add(0, "-1;All Rooms");

            for (int i = 0; i < rooms.size(); i++) {
                String arr[] = rooms.get(i).split(";");
                New_items new_items = new New_items();
                new_items.id = arr[0];
                new_items.name = arr[1];

                arr_name.add(new_items);
            }
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(new RecyclerAdapter(getActivity(), arr_name));
        }

//        arr_allselect.clone();
        arr_allselect.clear();
        for (int i = 0; i < rooms.size(); i++) {

            arr_allselect.add(i);
        }


        title.addTextChangedListener(clear);
        desc.addTextChangedListener(clear);
        date.addTextChangedListener(clear);
        time.addTextChangedListener(clear);
        txt_select_room.addTextChangedListener(clear);
        // select date for activity

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int MM, int dd) {
                        ed.set(yyyy, MM, dd);
                        ((TextView) view).setText(new SimpleDateFormat("yyyy-MM-dd").format(ed.getTime()));
                    }
                }, ed.get(Calendar.YEAR), ed.get(Calendar.MONTH), ed.get(Calendar.DATE));
                dpd.setTitle("Set Event Date");
                dpd.setCancelable(false);
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                if (((TextView) view).getText().length() > 0)
                    dpd.setButton(DialogInterface.BUTTON_NEUTRAL, "Remove", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((TextView) view).setText("");
                            dialogInterface.dismiss();
                        }
                    });
                dpd.getDatePicker().setMinDate(cc.getTimeInMillis());
                dpd.show();
            }
        });

        // DatePickerDialog dialog to select time

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                TimePickerDialog tpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int HH, int mm) {
                        String am_pm = "";

                        et.set(Calendar.HOUR_OF_DAY, HH);
                        et.set(Calendar.MINUTE, mm);
                        et.set(Calendar.SECOND, 0);
                        if (et.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else if (et.get(Calendar.AM_PM) == Calendar.PM)
                            am_pm = "PM";
                      //  et.set(Calendar.AM_PM, amapm);
                        ((TextView) view).setText(new SimpleDateFormat("hh:mm:ss").format(et.getTime())+ am_pm);
                    }
                }, et.get(Calendar.HOUR_OF_DAY), et.get(Calendar.MINUTE), false);
                tpd.setTitle("Set Event Time");
                tpd.setCancelable(false);
                tpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                if (((TextView) view).getText().length() > 0)
                    tpd.setButton(DialogInterface.BUTTON_NEUTRAL, "Remove", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((TextView) view).setText("");
                            dialogInterface.dismiss();
                        }
                    });
                tpd.show();
            }
        });

        // create button on click listener
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllErrors();

                // call add event web servive
                // and pass all parameter regarding this
                if (Common.validate(title, 2, true) && Common.validate(desc, 1, true) && Common.validate(date, 10, true) && Common.validate(time, 8, true) && Common.validate(txt_select_room,1,true) && Common.validate(location, 1, true)) {
                    RequestParams params = new RequestParams();
                    params.add("user_id", ((HomeActivity) getActivity()).uid);
                    params.add("room_id", Common.chkNull(HomeActivity.sel_room.split(";")[0], "0"));
                    params.add("eventTitle", title.getText().toString());
                    params.add("eventDesc", desc.getText().toString());
                    params.add("eventDate", date.getText().toString() + " " + time.getText().toString());
                    params.add("event_location", location.getText().toString());
                    Common.callWebService((HomeActivity) getActivity(), Common.wb_addevent, params, "AddEvent" + "%" + title.getText().toString() + ";" + desc.getText().toString() + ";" + date.getText().toString() + " " + time.getText().toString() + ";" + 0 + ";" + 0);
                }
            }
        });

        return rootView;
    }
    public void getlocation(){

    }
    void clearAllErrors() {
        title.setError(null);
        desc.setError(null);
        date.setError(null);
        time.setError(null);
        txt_select_room.setError(null);
        date.setFocusableInTouchMode(false);
        time.setFocusableInTouchMode(false);
       txt_select_room.setFocusableInTouchMode(false);
    }

    // add textwatcher to edittext
    final TextWatcher clear = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            clearAllErrors();
        }
    };
    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }
    public static int[] convertIntegers(List<Integer> integers) {
        int[] ret = new int[integers.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = integers.get(i).intValue();
        }
        return ret;
    }


    // setup dailog view
    // to show room list, show this dialog
    public void showdialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_spin_layout);

        ok = (TextView) dialog.findViewById(R.id.ok);
        cancel = (TextView) dialog.findViewById(R.id.cancel);

        recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_room);
        layoutManager = new LinearLayoutManager(getActivity());

//        dialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
