package com.kindertag.kindertag.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.kindertag.kindertag.Activity.MyChildrenActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

public class AddChildFragment extends Fragment {

    // this fragment is used to join school for teacher
    EditText code;
    Button submit;

    public AddChildFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_child, container, false);
        code = (EditText) rootView.findViewById(R.id.edit_new_child_code);
        submit = (Button) rootView.findViewById(R.id.btn_new_child_code);
        code.addTextChangedListener(clear);
        final boolean isTeach = ((MyChildrenActivity) getActivity()).userType.equalsIgnoreCase("Teacher");
        code.setHint("Enter your " + (isTeach ? "room" : "child") + " code");

        // click listener on submit button
        // call webservice joinSchoolForTecher and pass parameter
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllErrors();
                if (Common.validate(code, 4, true)) {
                    RequestParams params = new RequestParams();
                    params.add("user_id", ((MyChildrenActivity) getActivity()).uid);
                    params.add(isTeach ? "room_code" : "invite_code", code.getText().toString());
                    Log.i("name", isTeach ? "room_code" : "invite_code");
                    Log.i("url", isTeach ? Common.wb_joinschooolforteacher : Common.wb_joincode);
                    Log.i("tag", isTeach ? "JoinCodeTeacher" : "JoinCode");
                    Common.callWebService((MyChildrenActivity) getActivity(), isTeach ? Common.wb_joinschooolforteacher : Common.wb_joincode, params, isTeach ? "JoinCodeTeacher" : "JoinCode");
                }
            }
        });

        return rootView;
    }

    void clearAllErrors() {
        code.setError(null);
    }

    final TextWatcher clear = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            clearAllErrors();
        }
    };

}
