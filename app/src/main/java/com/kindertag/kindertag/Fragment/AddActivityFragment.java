package com.kindertag.kindertag.Fragment;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.HomeNewActivtyStudentsGridAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.WebAPIService;
import com.loopj.android.http.RequestParams;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.sandrios.sandriosCamera.internal.manager.CameraOutputModel;


import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;
import static com.kindertag.kindertag.Utils.MyApplication.TAG;
import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;



// this fragment use to add activity fragment
// this fragment use to add PHOTO activity
public class AddActivityFragment extends Fragment {
    private final String API_URL_BASE = "http://kindertag.underdev.in";
     ProgressBar simpleProgressBar;
    // declare variables
    Calendar cc = Calendar.getInstance();
    Calendar et = Calendar.getInstance();
    TextView date, staff_only, photo,video;
    EditText notes;
    LinearLayout ll_new_activity, ll_students, ll_photos;
    ImageView img_photo;
    VideoView video_view;
    ArrayList<String> students = new ArrayList<String>();
    ArrayList<String> sel_students = new ArrayList<String>();
    ArrayList<String> sel_students_temp = new ArrayList<String>();
    GridView grid_students;
    String type;
    ArrayList<Uri> imgs = new ArrayList<Uri>();
    ArrayList<Uri> videos = new ArrayList<Uri>();
    Uri imageUri = null;
    Uri videoUri = null;
    View rootView;
    private VideoView videoView;
    private TextView path;
    Context context;
    private List<String> mPath;

    public AddActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            type = getArguments().getString("type", "");

        }

        // get student list from Homeactivity
        students = ((HomeActivity) getActivity()).getStudentsforActivities();
    }

    @Override
    public void onPause() {
        super.onPause();
        ((HomeActivity) getActivity()).menuMultipleActions.collapse();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_activity, container, false);


        ll_new_activity = (LinearLayout) rootView.findViewById(R.id.ll_new_activity);
        ll_students = (LinearLayout) rootView.findViewById(R.id.ll_new_activity_students);
        date = (TextView) rootView.findViewById(R.id.txt_new_activity_date);
        staff_only = (TextView) rootView.findViewById(R.id.txt_new_activity_staff_only);
        photo = (TextView) rootView.findViewById(R.id.txt_new_activity_photo);
        video = (TextView) rootView.findViewById(R.id.txt_new_activity_video);
        notes = (EditText) rootView.findViewById(R.id.edit_new_activity_notes);
         simpleProgressBar = (ProgressBar) rootView.findViewById(R.id.simpleProgressBar);
        if (type.equalsIgnoreCase("photo")) {
            // video_view.setVisibility(View.VISIBLE);
            video.setVisibility(View.VISIBLE);
        }else {
            video.setVisibility(View.GONE);

        }
        if(type=="Med"){
            notes.setHint("Medicine");
        }
        img_photo = (ImageView) rootView.findViewById(R.id.img_new_activity_photo);
        video_view = (VideoView) rootView.findViewById(R.id.img_new_activity_video);
        ll_photos = (LinearLayout) rootView.findViewById(R.id.ll_new_activity_photos);
        final Button btn_create = (Button) rootView.findViewById(R.id.btn_new_activity_create);
        grid_students = (GridView) rootView.findViewById(R.id.grid_new_activity_students);
        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));

        date.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        staff_only.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        photo.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        video.setBackgroundDrawable(new EditText(getActivity()).getBackground());

        // select date for activity
        date.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(et.getTime()));
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Video", R.drawable.ic_gallery + ";" + "  Choose Video"};
                    ListAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, android.R.id.text1, items) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            //Use super class to create the View
                            View v = super.getView(position, convertView, parent);
                            TextView tv = (TextView) v.findViewById(android.R.id.text1);
                            //Put the image on the TextView
                            tv.setText(items[position].split(";")[1]);
                            tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                            return v;
                        }
                    };


                    // show dialog for camera or gallery showing
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Choose Action");
                    builder.setCancelable(true);
                    builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    // check camera permission and open camera
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                        AddActivityFragment.this.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                    } else {
                                        HomeActivity.img_name = HomeActivity.img_name + 1;
                                        Log.e("strt cm iii", i + "");
                                        Intent video = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                        File photo = new File(Environment.getExternalStorageDirectory(), "temp" + HomeActivity.img_name + ".png");
                                        video.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                        startActivityForResult(video, 10);
                                    }
                                    break;
                                case 1:
                                    // check gallery permission and open gallery
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                        AddActivityFragment.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                    } else {
                                        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                                        gallery.setType("video/*");
                                        startActivityForResult(gallery, 11);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                    builder.show();
                }

        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                    // DatePickerDialog dialog to select date
                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int MM, int dd) {
                        et.set(yyyy, MM, dd);

                        // TimePickerDialog to select time
                        TimePickerDialog tpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int HH, int mm) {
                                et.set(Calendar.HOUR_OF_DAY, HH);
                                et.set(Calendar.MINUTE, mm);
                                et.set(Calendar.SECOND, 0);
                                ((TextView) view).setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(et.getTime()));
                            }
                        }, et.get(Calendar.HOUR_OF_DAY), et.get(Calendar.MINUTE), true);
                        tpd.setTitle("Set Activity Time");
                        tpd.setCancelable(false);
                        tpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });

                        tpd.show();
                    }

                    // set date and time to edittext
                }, et.get(Calendar.YEAR), et.get(Calendar.MONTH), et.get(Calendar.DATE));
                dpd.setTitle("Set Activity Date");
                dpd.setCancelable(false);
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dpd.getDatePicker().setMaxDate(cc.getTimeInMillis());
                dpd.show();
            }
        });

        // choose one staff only or staff and parents
        staff_only.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean b = !Boolean.parseBoolean(view.getTag().toString());
                view.setTag(String.valueOf(b));
                staff_only.setCompoundDrawablesWithIntrinsicBounds(0, 0, b ? android.R.drawable.checkbox_on_background : android.R.drawable.checkbox_off_background, 0);
            }
        });

        //click listener on photo to choose action of photo
        // can take photo from camera and gallery
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imgs.size() < 4) {
                    final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Photo", R.drawable.ic_gallery + ";" + "  Choose Image"};
                    ListAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, android.R.id.text1, items) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            //Use super class to create the View
                            View v = super.getView(position, convertView, parent);
                            TextView tv = (TextView) v.findViewById(android.R.id.text1);
                            //Put the image on the TextView
                            tv.setText(items[position].split(";")[1]);
                            tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                            return v;
                        }
                    };


                    // show dialog for camera or gallery showing
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Choose Action");
                    builder.setCancelable(true);
                    builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    // check camera permission and open camera
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                        AddActivityFragment.this.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                    } else {
                                        HomeActivity.img_name = HomeActivity.img_name + 1;
                                        Log.e("strt cm iii", i + "");
                                        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        File photo = new File(Environment.getExternalStorageDirectory(), "temp" + HomeActivity.img_name + ".png");
                                        camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                        startActivityForResult(camera, 3);
                                    }
                                    break;
                                case 1:
                                    // check gallery permission and open gallery
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                            ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                        AddActivityFragment.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                    } else {
                                        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        gallery.setType("image/*");
                                        startActivityForResult(gallery, 7);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                    builder.show();
                } else
                    Toast.makeText(getActivity(), "Maximum 4 Images are allowed", Toast.LENGTH_SHORT).show();
            }
        });

        setStudents(ll_students);

        if (type.equalsIgnoreCase("photo"))
            setPhotos(ll_photos, imgs);

        // add onclcik listener on create button
        // call addActivity webservice
        // pass all parameters to create photo activity.

        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if (sel_students.size() == 0)
                    Toast.makeText(getActivity(), "Select atleast 1 Student", Toast.LENGTH_SHORT).show();
                else if (type.equalsIgnoreCase("photo") && imgs.size() == 0)
                    Toast.makeText(getActivity(), "Add atleast 1 Photo", Toast.LENGTH_SHORT).show();
                else {
                    String student_ids = getIds(sel_students);
                    RequestParams params = new RequestParams();
                    params.add("room_id", ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                    params.add(type.equalsIgnoreCase("photo") ? "student_ids" : "student_id", student_ids);
                    params.add("isstaff", Boolean.parseBoolean(staff_only.getTag().toString()) ? "1" : "0");
                    params.add("activity_date", date.getText().toString());
                    if(type.equalsIgnoreCase("Med") || type.equalsIgnoreCase("medicine"))
                    {
                        type="medicine";
                        params.add("activity_type", type.toLowerCase());
                        params.add("photo", Common.genrateByteArrayString(getActivity(), imageUri));
                        params.add("medicine", notes.getText().toString());
                        Common.callWebService((HomeActivity) getActivity(), Common.wb_medicine, params, "AddActivityMed" + "%" + date.getText().toString() + ";" + type + ";" + ((HomeActivity) getActivity()).toString(sel_students) + ";" + notes.getText().toString() + ";" + "photos" + ";" + Boolean.parseBoolean(staff_only.getTag().toString()));
                    }else if (!type.equalsIgnoreCase("photo")) {
                         params.add("activity_type", type.toLowerCase());
                         params.add("photo", Common.genrateByteArrayString(getActivity(), imageUri));
                         params.add("note", notes.getText().toString());
                         Common.callWebService((HomeActivity) getActivity(), Common.wb_addactivity, params, "AddActivity" + "%" + date.getText().toString() + ";" + type + ";" + ((HomeActivity) getActivity()).toString(sel_students) + ";" + notes.getText().toString() + ";" + "photos" + ";" + Boolean.parseBoolean(staff_only.getTag().toString()));
                     }
                     else if (type.equalsIgnoreCase("photo")) {

                        simpleProgressBar.setVisibility(View.VISIBLE);

                        final ProgressDialog pd = new ProgressDialog(view.getContext());//,R.style.MyTheme);
                        //pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        pd.setMessage("Please Wait...");
                        pd.setIndeterminate(true);
                        pd.setCancelable(false);

                  //      uploadPhotoVideo();
                        String filePath1 = getRealPathFromUri(imgs.get(0));
                        if(imgs.size()>0){

                        }
                        String filePath2 = getRealPathFromUri(imgs.get(1));
                        String filePath3 = getRealPathFromUri(imgs.get(2));
                        String filePath4 = getRealPathFromUri(imgs.get(3));
                        params.put("activity_text", notes.getText().toString());
                        params.put("image1", filePath1);
                        params.put("image2", filePath2);
                        params.put("image3", filePath3);
                        params.put("image4", filePath4);
                        params.put("video", filePath4);
                        Common.callWebService((HomeActivity) getActivity(), Common.wb_addphoto, params, "AddActivity" + "%" + date.getText().toString() + ";" + type + ";" + ((HomeActivity) getActivity()).toString(sel_students) + ";" + notes.getText().toString() + ";" + "photos" + ";" + Boolean.parseBoolean(staff_only.getTag().toString()));
                    }
                }
            }
        });

        return rootView;
    }
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    public String getRealPathFromUri(final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(getContext(), uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(getContext()
                        , contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(getContext(), contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(getContext(), uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
    private void uploadPhotoVideo() {
       // ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add " + type);

        Intent intent = new Intent( getActivity(),HomeActivity.class);
        startActivity(intent);
        Toast.makeText(getContext(), "A  Photo Video activity has been added", Toast.LENGTH_SHORT).show();
        final ProgressDialog pd = new ProgressDialog(getContext());//,R.style.MyTheme);
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pd.setMessage("Please Wait...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        String filePath1 = getRealPathFromUri(imgs.get(0));
        if(imgs.size()>0){

        }
        String filePath2 = getRealPathFromUri(imgs.get(1));
        String filePath3 = getRealPathFromUri(imgs.get(2));
        String filePath4 = getRealPathFromUri(imgs.get(3));
       // String videopath = getRealPathFromUri(videos.get(0));
        if (filePath1 != null && !filePath1.isEmpty()) {
            File file1 = new File(filePath1);
            File file2 = new File(filePath2);
            File file3 = new File(filePath3);
            File file4 = new File(filePath4);
         //   File videofile = new File(videopath);
            if (file1.exists()) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(API_URL_BASE)
                        .build();

                WebAPIService service = retrofit.create(WebAPIService.class);

                // creates RequestBody instance from file
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
                RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
                RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file3);
                RequestBody requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file4);
                RequestBody requestFilevideo = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                // MultipartBody.Part is used to send also the actual filename
                MultipartBody.Part image1 = MultipartBody.Part.createFormData("image1", file1.getName(), requestFile1);
                MultipartBody.Part image2 = MultipartBody.Part.createFormData("image2", file2.getName(), requestFile2);
                MultipartBody.Part image3 = MultipartBody.Part.createFormData("image3", file3.getName(), requestFile3);
                MultipartBody.Part image4 = MultipartBody.Part.createFormData("image4", file4.getName(), requestFile4);
                MultipartBody.Part video = MultipartBody.Part.createFormData("video", file4.getName(), requestFilevideo);
                // adds another part within the multipart request
                RequestBody activity_date = RequestBody.create(MediaType.parse("multipart/form-data"), date.getText().toString());
                RequestBody activity_text = RequestBody.create(MediaType.parse("multipart/form-data"), notes.getText().toString());
                RequestBody isstaff = RequestBody.create(MediaType.parse("multipart/form-data"), Boolean.parseBoolean(staff_only.getTag().toString()) ? "1" : "0");
                RequestBody room_id = RequestBody.create(MediaType.parse("multipart/form-data"), ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                String student_ids = getIds(sel_students);

                RequestBody student_id = RequestBody.create(MediaType.parse("multipart/form-data"),student_ids);
                // executes the request
                Call<ResponseBody> call = service.postFile_A(image1, image2,image3,image4,activity_date,activity_text,isstaff,room_id,student_id,video);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           retrofit2.Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            if (pd != null && pd.isShowing()) pd.dismiss();
                           // JSONObject jobj = new JSONObject(response);
                            simpleProgressBar.setVisibility(View.GONE);

                            String output =response.body().toString();
                            Log.i(LOG_TAG, output);
                           Intent intent = new Intent(getActivity(),HomeActivity.class);
                            startActivity(intent);
                            Toast.makeText(getContext(), "A  Photo Video activity has been added", Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (pd != null && pd.isShowing()) pd.dismiss();
                        simpleProgressBar.setVisibility(View.GONE);

                        Intent intent = new Intent(getActivity(),HomeActivity.class);
                        startActivity(intent);
                        Toast.makeText(getContext(), "A  Photo Video activity has been added", Toast.LENGTH_SHORT).show();

                        Log.e(LOG_TAG, t.getMessage());
                    }
                });
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();


    }


    // get ids of selected students
    public String getIds(ArrayList<String> sel_students) {
        String str = "";
        for (int i = 0; i < sel_students.size(); i++)
            str += "," + sel_students.get(i).split(";")[0];
        return str.length() > 0 ? str.substring(1) : str;
    }

    public void reset() {
        getActivity().invalidateOptionsMenu();
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add " + type);
        grid_students.setVisibility(View.GONE);
        ll_new_activity.setVisibility(View.VISIBLE);
        setStudents(ll_students);
    }

    public void set() {
        sel_students.clear();
        for (int i = 0; i < sel_students_temp.size(); i++)
            sel_students.add(sel_students_temp.get(i));
        reset();
    }

    public void refresh_students(String search) {
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, search,true));
    }

    // set all students to gridview

    public void setStudents(final LinearLayout ll) {
        ll.setVisibility(View.VISIBLE);
        ll.removeAllViews();
        final int max = sel_students.size() > 5 ? 7 : sel_students.size() + 2;
        for (int i = 0; i < max; i++) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40), getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40));
            llp.setMargins(i > 0 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5), i < max - 1 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5));
            if (i == max - 1) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTypeface(((HomeActivity) getActivity()).tf_material);
                txt.setTextColor(Color.WHITE);
                txt.setText(R.string.icon_plus);
                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ll_new_activity.setVisibility(View.GONE);
                        grid_students.setVisibility(View.VISIBLE);
                        sel_students_temp.clear();
                        for (int i = 0; i < sel_students.size(); i++)
                            sel_students_temp.add(sel_students.get(i));
                        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
                        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Select Student");
                        getActivity().invalidateOptionsMenu();
                    }
                });
                ll.addView(txt);
            } else if (i == max - 2) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTextColor(Color.BLACK);
                txt.setText((sel_students.size() - (max - 2)) + "+");
                txt.setVisibility(sel_students.size() > 5 ? View.VISIBLE : View.GONE);
                ll.addView(txt);
            } else {
                final ImageView img = new ImageView(getActivity());
                img.setLayoutParams(llp);
                img.setBackgroundResource(R.drawable.black_circle_back2);
                Glide.with(getActivity()).load(sel_students.get(i).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        if (resource != null && img != null) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            img.setImageDrawable(circularBitmapDrawable);
                        }
                    }
                });
                ll.addView(img);
            }
        }
    }

    // set captured and selected photos to imageveiw
    public void setPhotos(final LinearLayout ll, ArrayList<Uri> urls) {
        ll.setVisibility(View.VISIBLE);
        ll.removeAllViews();
        int max = urls.size() % 2 != 0 ? urls.size() + 1 : urls.size();
        for (int i = 0; i < urls.size(); i++) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_home_activity_photos_new, null);

            ImageView img0 = (ImageView) view.findViewById(R.id.img_home_activity_photos_photo1);
            ImageView img1 = (ImageView) view.findViewById(R.id.img_home_activity_photos_photo2);
            ImageView img2 = (ImageView) view.findViewById(R.id.img_home_activity_photos_photo3);
            ImageView img3 = (ImageView) view.findViewById(R.id.img_home_activity_photos_photo4);

            if (urls.size() == 1) {
                Glide.with(getActivity()).load(urls.get(i)).into(img0);
                ll.addView(view);
            } else if (urls.size() == 2) {
                Glide.with(getActivity()).load(urls.get(0)).into(img0);
                Glide.with(getActivity()).load(urls.get(1)).into(img1);
                ll.addView(view);
            } else if (urls.size() == 3) {
                Glide.with(getActivity()).load(urls.get(0)).into(img0);
                Glide.with(getActivity()).load(urls.get(1)).into(img1);
                Glide.with(getActivity()).load(urls.get(2)).into(img2);
                ll.addView(view);

            } else if (urls.size() == 4) {
                Glide.with(getActivity()).load(urls.get(0)).into(img0);
                Glide.with(getActivity()).load(urls.get(1)).into(img1);
                Glide.with(getActivity()).load(urls.get(2)).into(img2);
                Glide.with(getActivity()).load(urls.get(3)).into(img3);
                ll.addView(view);
            }

            break;

        }
    }
public void selectCheckIn(boolean selectCheckin){
        if(selectCheckin){
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                if(students.get(i).split(";")[4].equalsIgnoreCase("true")){
                    sel_students_temp.add(students.get(i));

                }
        } else

            sel_students_temp.clear();
              if (grid_students.getVisibility() == View.VISIBLE)
        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",false));
        }

    // select all students
    public void setSelectionAll(boolean isSelectAll) {
        if (isSelectAll) {
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                sel_students_temp.add(students.get(i));
        } else
            sel_students_temp.clear();
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
    }

    // when capture photo or select photo then reach here onActivity result
    // get uri from data and set to image
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MediaController mediaController = new MediaController(getContext());

        File photo = new File(Environment.getExternalStorageDirectory(), "temp" + HomeActivity.img_name + ".png");

          if (resultCode == RESULT_OK && requestCode == 3) {
            Log.e("get pht iii", HomeActivity.img_name + "");
            if (type.equalsIgnoreCase("photo")) {
                imgs.add(Uri.fromFile(photo));
                HomeActivity.img_name++;
                setPhotos(ll_photos, imgs);
            } else if (!type.equalsIgnoreCase("photo")) {
                Log.e("set ph iii", HomeActivity.img_name + "");
                imageUri = Uri.fromFile(photo);
                img_photo.setVisibility(View.VISIBLE);
                Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
            }
        } else if (resultCode == RESULT_OK && requestCode == 7 && data != null) {
            if (type.equalsIgnoreCase("photo")) {
                imgs.add(data.getData());
                HomeActivity.img_name++;
                setPhotos(ll_photos, imgs);
            } else if (!type.equalsIgnoreCase("photo")) {
                imageUri = data.getData();
                img_photo.setVisibility(View.VISIBLE);
                Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
            }
        } else if(resultCode ==RESULT_OK && requestCode ==10 && data !=null){
              File video = new File(Environment.getExternalStorageDirectory(), "temp" + HomeActivity.img_name + ".mp4");
             // String uriPath = "android.resource://"+getActivity().getPackageName()+"/"+HomeActivity.img_name+ ".mp4";
           //   Uri uri = Uri.parse(uriPath);
              videoUri =data.getData();
              video_view.setVisibility(View.VISIBLE);
              video_view.setVideoURI(videoUri);
              video_view.seekTo(0);

             // video_view.start();
              videos.add(data.getData());

              video_view.setMediaController(mediaController);

          }else  if(resultCode ==RESULT_OK && requestCode ==11 && data !=null){
              videoUri =data.getData();
              video_view.setVisibility(View.VISIBLE);
              video_view.setVideoURI(videoUri);
              video_view.seekTo(0);

              //video_view.start();
              videos.add(data.getData());

              video_view.setMediaController(mediaController);
          }
    }

    // when permission granted then reach in request permission
    // according to permission fire that action
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Camera Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            HomeActivity.img_name = HomeActivity.img_name + 1;
            Log.e("iii", HomeActivity.img_name + "");
            File photo = new File(Environment.getExternalStorageDirectory(), "temp" + HomeActivity.img_name + ".png");
            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            startActivityForResult(camera, 3);
        } else if (requestCode == 3 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Camera Permission Not Granted!", Toast.LENGTH_SHORT).show();

        if (requestCode == 7 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Gallery Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, 7);
        } else if (requestCode == 7 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Gallery Permission Not Granted!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
