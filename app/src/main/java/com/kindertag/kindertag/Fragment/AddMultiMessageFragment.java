package com.kindertag.kindertag.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.HomeNewActivtyStudentsGridAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

// this frament is used to send message to multiple students
public class AddMultiMessageFragment extends Fragment {

    EditText message;
    TextView send;
    GridView grid_receivers;
    ArrayList<String> receivers = new ArrayList<String>();
    ArrayList<String> sel_receivers = new ArrayList<String>();

    public AddMultiMessageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        receivers = ((HomeActivity) getActivity()).getReceivers();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_multi_messages, container, false);
        grid_receivers = (GridView) rootView.findViewById(R.id.grid_multi_message_receivers);
        message = (EditText) rootView.findViewById(R.id.edit_multi_message);
        send = (TextView) rootView.findViewById(R.id.btn_multi_message_send);

        grid_receivers.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), receivers, sel_receivers, "",true));
        grid_receivers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (sel_receivers.contains(((HomeActivity) getActivity()).getStudents().get(i)))
                    sel_receivers.remove(((HomeActivity) getActivity()).getStudents().get(i));
                else
                    sel_receivers.add(((HomeActivity) getActivity()).getStudents().get(i));
            }
        });

        send.setTypeface(((HomeActivity) getActivity()).tf_material);
        send.setText(R.string.icon_send);


        // when click on send button call sendmessage webservice
        // pass sender id, receiver id and messageText to send message
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.validate(message, 1, true)) {
                    String ids = getIds(sel_receivers);
                    RequestParams params = new RequestParams();
                    params.add("sender_id", ((HomeActivity) getActivity()).uid);
                    params.add("receiver_id", ids);
                    params.add("messageText", message.getText().toString());
                    Common.callWebService((HomeActivity) getActivity(), Common.wb_sendmessage, params, "SendMessages" + "%" + message.getText().toString() + "%" + ids);
                }
            }
        });

        return rootView;
    }

    // get ids of selected students

    public String getIds(ArrayList<String> sel_students) {
        String str = "";
        for (int i = 0; i < sel_students.size(); i++)
            str += "," + sel_students.get(i).split(";")[0];
        return str.length() > 0 ? str.substring(1) : str;
    }

    // select all students

    public void setSelectionAll(boolean isSelectAll) {
        if (isSelectAll) {
            sel_receivers.clear();
            for (int i = 0; i < receivers.size(); i++)
                sel_receivers.add(receivers.get(i));
        } else
            sel_receivers.clear();
        grid_receivers.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), receivers, sel_receivers, "",true));
    }

 /*@Override
 public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
  super.onCreateOptionsMenu(menu, inflater);
  inflater.inflate(R.menu.home, menu);
  menu.findItem(R.id.action_update).setVisible(((HomeActivity)getActivity()).getSupportActionBar().getTitle().toString().equalsIgnoreCase("Select Student"));
  menu.findItem(R.id.action_select_all).setVisible(((HomeActivity)getActivity()).getSupportActionBar().getTitle().toString().equalsIgnoreCase("Select Student"));
 }*/

 /*@Override
 public boolean onOptionsItemSelected(MenuItem item) {
  switch(item.getItemId()){
   *//*case R.id.action_search:
    SearchManager searchManager = (SearchManager) HomeActivity.this.getSystemService(Context.SEARCH_SERVICE);
    searchView = (SearchView) item.getActionView();
    searchView.setSearchableInfo(searchManager.getSearchableInfo(HomeActivity.this.getComponentName()));
    return true;*//*
   case R.id.action_update:
    if(grid_students.getVisibility()==View.VISIBLE)
     reset();
    return true;
   case R.id.action_select_all:
    if(item.getTitle().toString().equalsIgnoreCase("Select All"))
     for(int i=0;i<students.size();i++)
      sel_students.add(students.get(i).split(";")[0]);
    else
     sel_students.clear();
    item.setTitle(item.getTitle().toString().equalsIgnoreCase("Select All")?"Select None":"Select All");
    if(grid_students.getVisibility()==View.VISIBLE)
     grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(),students,sel_students));
    return true;
   case R.id.action_settings:return true;
   default:return super.onOptionsItemSelected(item);
  }
 }*/

 /*void clearAllErrors(){
  fname.setError(null);lname.setError(null);
  birthdate.setError(null);time.setError(null);
  birthdate.setFocusableInTouchMode(false);
  time.setFocusableInTouchMode(false);
 }

 final TextWatcher clear = new TextWatcher() {
  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

  @Override
  public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

  @Override
  public void afterTextChanged(Editable editable) {
   clearAllErrors();
  }
 };*/

}
