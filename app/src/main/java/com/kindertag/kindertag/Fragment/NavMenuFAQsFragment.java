package com.kindertag.kindertag.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;

public class NavMenuFAQsFragment extends Fragment {

 @Override
 public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
 }

 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
  View rootView = inflater.inflate(R.layout.fragment_nav_menu_faqs, container, false);
  return rootView;
 }

 @Override
 public void onDetach() {
  super.onDetach();
  ((HomeActivity)getActivity()).setActionTitle("Help");
 }
}
