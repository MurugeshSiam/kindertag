package com.kindertag.kindertag.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.FoodTypeAdapter;
import com.kindertag.kindertag.Adapter.Food_Meal_Adapter;
import com.kindertag.kindertag.Adapter.Food_Type_Adapter;
import com.kindertag.kindertag.Adapter.HomeNewActivtyStudentsGridAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.module.Item_Food;
import com.kindertag.kindertag.module.Item_Type;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AddFoodFragment extends Fragment {

    Dialog myDialog = null;

    // create arrAYs of types of food items
    // create types of bottols
    final String types[] = {"Meal Item", "Consumption", "Meal Type", "Share With", "Photo", "Notes"};
    final String types_bottol[] = {"Meal Items", "MLs"};
    LinearLayout ll_new_learning, ll_students;
    EditText search;
    View view_search;
    TextView add_cat;
    ArrayList<String> students = new ArrayList<String>();
    ArrayList<String> sel_students = new ArrayList<String>();
    ArrayList<String> sel_students_temp = new ArrayList<String>();
    ListView list_food_type, list_bottol_type;
    int sel_type = 0;
    int sel_type_bottol = 0;
    String sel_consption = "", sel_meal_type = "", sel_share_with = "";
    EditText notes, edt_mls;
    TextView photo, txt_time;
    ImageView img_photo;
    Uri imageUri = null;
    GridView grid_students;
    String type;
    LinearLayout line_mls;
    Button btn_food, btn_bottol;
    ArrayList<String> foods = new ArrayList<String>();
    public ArrayList<Item_Food> food_list = new ArrayList<Item_Food>();

    ArrayList<Item_Type> list_consumption = new ArrayList<Item_Type>();
    ArrayList<Item_Type> list_meal_type = new ArrayList<Item_Type>();
    ArrayList<Item_Type> list_share_with = new ArrayList<Item_Type>();

    ArrayList<String> bottols = new ArrayList<String>();
    ArrayList<Item_Food> bottol_list = new ArrayList<Item_Food>();

    String food_type = "food";

    Calendar et = Calendar.getInstance();
    Calendar cc = Calendar.getInstance();
    String mls_entered = "0";

    public AddFoodFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            type = getArguments().getString("type", "");
        }
        students = ((HomeActivity) getActivity()).getStudentsforActivities();
        foods = ((HomeActivity) getActivity()).getFoods();
        bottols = ((HomeActivity) getActivity()).getBottols();

        // add items in list consumption
        list_consumption.add(new Item_Type("All"));
        list_consumption.add(new Item_Type("Most"));
        list_consumption.add(new Item_Type("Some"));
        list_consumption.add(new Item_Type("None"));
        list_consumption.get(0).setItem_isCheck(true);

        // add items in meal type
        list_meal_type.add(new Item_Type("Breakfast"));
        list_meal_type.add(new Item_Type("AM Snack"));
        list_meal_type.add(new Item_Type("Lunch"));
        list_meal_type.add(new Item_Type("PM Snack"));
        list_meal_type.add(new Item_Type("Dinner"));
        list_meal_type.add(new Item_Type("Late Snack"));
        list_meal_type.get(0).setItem_isCheck(true);

        // add share with items in list
        list_share_with.add(new Item_Type("Parents and Staff"));
        list_share_with.add(new Item_Type("Staff only"));
        list_share_with.get(0).setItem_isCheck(true);

    }

    public void setUpTypes() {

    }

    RecyclerView recycler_food_meal_itm, recycler_food_cnsmptn, recycler_food_meal_type, recycler_share_with, recycler_botle_meal_item;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_food, container, false);
        ll_new_learning = (LinearLayout) rootView.findViewById(R.id.ll_new_learning);
        ll_students = (LinearLayout) rootView.findViewById(R.id.ll_new_learning_students);
        search = (EditText) rootView.findViewById(R.id.edit_new_learning_search);
        view_search = rootView.findViewById(R.id.view_edit_new_learning_search);
        add_cat = (TextView) rootView.findViewById(R.id.txt_list_vals_category_add);

        list_food_type = (ListView) rootView.findViewById(R.id.list_food_type);
        list_bottol_type = (ListView) rootView.findViewById(R.id.list_bottol_type);

        recycler_food_meal_itm = (RecyclerView) rootView.findViewById(R.id.recycler_food_meal_itm);
        recycler_food_cnsmptn = (RecyclerView) rootView.findViewById(R.id.recycler_food_cnsmptn);
        recycler_food_meal_type = (RecyclerView) rootView.findViewById(R.id.recycler_food_meal_type);
        recycler_share_with = (RecyclerView) rootView.findViewById(R.id.recycler_share_with);
        recycler_botle_meal_item = (RecyclerView) rootView.findViewById(R.id.recycler_botle_meal_item);

        recycler_food_meal_itm.setHasFixedSize(true);
        recycler_food_cnsmptn.setHasFixedSize(true);
        recycler_food_meal_type.setHasFixedSize(true);
        recycler_share_with.setHasFixedSize(true);
        recycler_botle_meal_item.setHasFixedSize(true);

        recycler_food_meal_itm.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_food_cnsmptn.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_food_meal_type.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_share_with.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_botle_meal_item.setLayoutManager(new LinearLayoutManager(getActivity()));

        recycler_food_cnsmptn.setAdapter(new Food_Type_Adapter(getActivity(), list_consumption, "c"));
        recycler_food_meal_type.setAdapter(new Food_Type_Adapter(getActivity(), list_meal_type, "mt"));
        recycler_share_with.setAdapter(new Food_Type_Adapter(getActivity(), list_share_with, "sw"));

        photo = (TextView) rootView.findViewById(R.id.txt_new_learning_photo);
        img_photo = (ImageView) rootView.findViewById(R.id.img_new_learning_photo);
        notes = (EditText) rootView.findViewById(R.id.edit_new_learning_notes);

        final Button btn_create = (Button) rootView.findViewById(R.id.btn_new_learning_create);
        line_mls = (LinearLayout) rootView.findViewById(R.id.line_mls);
        edt_mls = (EditText) rootView.findViewById(R.id.edt_mls);
        txt_time = (TextView) rootView.findViewById(R.id.txt_time);
        //=====================================================================================
        btn_food = (Button) rootView.findViewById(R.id.btn_food);
        btn_bottol = (Button) rootView.findViewById(R.id.btn_bottol);

        foodView();

        btn_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                foodView();
            }
        });

        btn_bottol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottolView();
            }
        });

        //=====================================================================================

        // add textwatcher in search view
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (search.getVisibility() == View.VISIBLE) {

                    // filter recyclerview regarding enter text in searchview
                    if (editable.length() == 0) {
                        if (food_type.equals("food"))
                            recycler_food_meal_itm.setAdapter(new Food_Meal_Adapter(getActivity(), food_list, "f"));
                        else
                            recycler_botle_meal_item.setAdapter(new Food_Meal_Adapter(getActivity(), bottol_list, "b"));
                    }

                    ArrayList<Item_Food> arrayList_temp = new ArrayList<>();
                    if (food_type.equals("food")) {
                        for (int i = 0; i < food_list.size(); i++) {
                            if (food_list.get(i).getItem_name().toLowerCase().contains(editable.toString().toLowerCase()))
                                arrayList_temp.add(food_list.get(i));
                        }
                        recycler_food_meal_itm.setAdapter(new Food_Meal_Adapter(getActivity(), arrayList_temp, "f"));
                    } else {
                        for (int i = 0; i < bottol_list.size(); i++) {
                            if (bottol_list.get(i).getItem_name().toLowerCase().contains(editable.toString().toLowerCase()))
                                arrayList_temp.add(bottol_list.get(i));
                        }
                        recycler_botle_meal_item.setAdapter(new Food_Meal_Adapter(getActivity(), arrayList_temp, "b"));
                    }

                }
            }
        });

        // add milk in millies edittext
        edt_mls.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mls_entered = edt_mls.getText().toString().trim();
            }
        });

        // click listener on add category
        add_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // show dialog of add meal items
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(true);
                builder.setTitle("Add Meal Items");
                final EditText cat_name = new EditText(getActivity());
                cat_name.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                cat_name.setHint("Meal Item Name");
                cat_name.setTextColor(Color.BLACK);
                cat_name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                cat_name.setMaxLines(1);
                builder.setView(cat_name);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                // here we add two types of meal items
                // bottols and food items

                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        // call addfooditem web serviec and pass parameters
                        // pass item_type food
                        if (food_type.equals("food")) {
                            if (cat_name.getText().toString().trim().length() > 0) {
                                RequestParams params = new RequestParams();
                                params.add("room_id", ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                                params.add("item_name", cat_name.getText().toString().trim());
                                params.add("item_type", "food");
                                Common.callWebService((HomeActivity) getActivity(), Common.wb_addFooditem, params, "AddFoodItem" + "%" + cat_name.getText().toString().trim());
                            }

                            // call addfooditem web service and pass parametersn
                            // pass item type parameter bottol
                        } else {
                            RequestParams params = new RequestParams();
                            params.add("room_id", ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                            params.add("item_name", cat_name.getText().toString().trim());
                            params.add("item_type", "bottle");
                            Common.callWebService((HomeActivity) getActivity(), Common.wb_addFooditem, params, "AddFoodItem" + "%" + cat_name.getText().toString().trim());
                        }
                    }
                });
                builder.create().show();
            }
        });

        for (int i = 0; i < foods.size(); i++) {
            String[] fd = foods.get(i).split(";");
            food_list.add(new Item_Food(fd[0], fd[1], false));
        }

        for (int i = 0; i < bottols.size(); i++) {
            String[] fd = bottols.get(i).split(";");
            bottol_list.add(new Item_Food(fd[0], fd[1], false));
        }

        list_food_type.setAdapter(new FoodTypeAdapter(getActivity(), types, sel_type));
        recycler_food_meal_itm.setAdapter(new Food_Meal_Adapter(getActivity(), food_list, "f"));

        list_bottol_type.setAdapter(new FoodTypeAdapter(getActivity(), types_bottol, sel_type_bottol));
        recycler_botle_meal_item.setAdapter(new Food_Meal_Adapter(getActivity(), bottol_list, "b"));

        // set On ItemClickListener list food type
        // select food type
        list_food_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                sel_type = i;
                list_food_type.setAdapter(new FoodTypeAdapter(getActivity(), types, sel_type));
                String name = list_food_type.getAdapter().getItem(i).toString();
                photo.setVisibility(name.equalsIgnoreCase("photo") ? View.VISIBLE : View.GONE);
                img_photo.setVisibility((name.equalsIgnoreCase("photo") && img_photo.getDrawable() != null) ? View.VISIBLE : View.GONE);
                notes.setVisibility(name.equalsIgnoreCase("notes") ? View.VISIBLE : View.GONE);
                search.setVisibility(name.equalsIgnoreCase("Meal Item") ? View.VISIBLE : View.GONE);
                search.setText("");
                view_search.setVisibility(name.equalsIgnoreCase("Meal Item") ? View.VISIBLE : View.GONE);
                add_cat.setVisibility(name.equalsIgnoreCase("Meal Item") ? View.VISIBLE : View.GONE);

                recycler_food_meal_itm.setVisibility(name.equalsIgnoreCase("Meal Item") ? View.VISIBLE : View.GONE);
                recycler_food_cnsmptn.setVisibility(name.equalsIgnoreCase("Consumption") ? View.VISIBLE : View.GONE);
                recycler_food_meal_type.setVisibility(name.equalsIgnoreCase("Meal Type") ? View.VISIBLE : View.GONE);
                recycler_share_with.setVisibility(name.equalsIgnoreCase("Share With") ? View.VISIBLE : View.GONE);
            }
        });



        //===================================================================================================

        // select bottol type
        list_bottol_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                sel_type_bottol = i;
                list_bottol_type.setAdapter(new FoodTypeAdapter(getActivity(), types_bottol, sel_type_bottol));
                String name = list_bottol_type.getAdapter().getItem(i).toString();
                line_mls.setVisibility(View.GONE);
                search.setVisibility(name.equalsIgnoreCase("Meal Items") ? View.VISIBLE : View.GONE);
                search.setText("");
                view_search.setVisibility(name.equalsIgnoreCase("Meal Items") ? View.VISIBLE : View.GONE);
                add_cat.setVisibility(name.equalsIgnoreCase("Meal Items") ? View.VISIBLE : View.GONE);

                if (i == 1) {
                    line_mls.setVisibility(View.VISIBLE);
                }
            }
        });


        // add photo in food activity
        photo.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Photo", R.drawable.ic_gallery + ";" + "  Choose Image"};
                ListAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, android.R.id.text1, items) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        //Use super class to create the View
                        View v = super.getView(position, convertView, parent);
                        TextView tv = (TextView) v.findViewById(android.R.id.text1);
                        //Put the image on the TextView
                        tv.setText(items[position].split(";")[1]);
                        tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                        return v;
                    }
                };

                // show AlertDialog for show dialog for camera and gallery
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Action");
                builder.setCancelable(true);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddFoodFragment.this.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                } else {
                                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
                                    camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                    startActivityForResult(camera, 3);
                                }
                                break;
                            case 1:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddFoodFragment.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                } else {
                                    Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    gallery.setType("image/*");
                                    startActivityForResult(gallery, 7);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                });
                builder.show();
            }
        });

        grid_students = (GridView) rootView.findViewById(R.id.grid_new_learning_students);
        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));

        setStudents(ll_students);

        // call addFoodActivity web service pass all food related parameters
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // get food ids from food list
                String foodids = "";
                for (int i = 0; i < food_list.size(); i++) {
                    if (food_list.get(i).isItem_isCheck()) {
                        foodids = foodids + "," + food_list.get(i).getItem_id();
                    }
                }

                // get bottol ids from selecte bottol items
                String bottolids = "";
                for (int i = 0; i < bottol_list.size(); i++) {
                    if (bottol_list.get(i).isItem_isCheck()) {
                        bottolids = bottolids + "," + bottol_list.get(i).getItem_id();
                    }
                }

                if (bottolids.length() > 1)
                bottolids=bottolids.substring(1);

                // select consption
                for (int i = 0; i < list_consumption.size(); i++) {
                    if (list_consumption.get(i).isItem_isCheck()) {
                        sel_consption=list_consumption.get(i).getItem_name();
                    }
                }

                // select meal type
                for (int i = 0; i < list_meal_type.size(); i++) {
                    if (list_meal_type.get(i).isItem_isCheck()) {
                        sel_meal_type=list_meal_type.get(i).getItem_name();
                    }
                }


                for (int i = 0; i < list_share_with.size(); i++) {
                    if (list_share_with.get(i).isItem_isCheck()) {
                        sel_share_with=list_share_with.get(i).getItem_name();
                    }
                }

                if (sel_students.size() == 0)
                    Toast.makeText(getActivity(), "Select atleast 1 Student", Toast.LENGTH_SHORT).show();
                else if (foodids.length() == 0)
                    Toast.makeText(getActivity(), "Select atleast 1 Meal Item", Toast.LENGTH_SHORT).show();
                else {

                    // pass parameters for add food activity
                    String student_ids = getIds(sel_students);
                    RequestParams params = new RequestParams();
                    params.add("room_id", ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                    params.add("student_ids", student_ids);
                    params.add("activity_date", txt_time.getText().toString().replace("Time : ", ""));

                    params.add("food_item_id", foodids.substring(1));
                    params.add("food_consumption", sel_consption);
                    params.add("food_meal_type", sel_meal_type);
                    params.add("bootle_item_id", bottolids);
                    params.add("bootle_ounce", edt_mls.getText().toString().trim());
                    params.add("isstaff", sel_share_with.equalsIgnoreCase("Staff only") ? "1" : "0");
                    params.add("note", notes.getText().toString());
                    params.add("photo", Common.genrateByteArrayString(getActivity(), imageUri));

                    Log.e("params",params.toString());

                    Common.callWebService((HomeActivity) getActivity(), Common.wb_addfoodactivity, params, "AddFood" + "%" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + ";" + type + ";" + ((HomeActivity) getActivity()).toString(sel_students) + ";" + "" + ";" + "photos" + ";" + sel_share_with.equalsIgnoreCase("Staff only"));
                }
            }
        });

        //==================================================================================
        Date d = new Date();
        CharSequence s = DateFormat.format("yyyy-MM-dd HH:mm:ss", d.getTime());
        txt_time.setText("Time : " + s);

        // DatePickerDialog dialog to select date

        txt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int MM, int dd) {
                        et.set(yyyy, MM, dd);
                        TimePickerDialog tpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int HH, int mm) {
                                et.set(Calendar.HOUR_OF_DAY, HH);
                                et.set(Calendar.MINUTE, mm);
                                et.set(Calendar.SECOND, 0);
                            //    et.set(Calendar.AM_PM,)
                                txt_time.setText("Time : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(et.getTime()));
                            }
                        }, et.get(Calendar.HOUR_OF_DAY), et.get(Calendar.MINUTE), true);
                        tpd.setTitle("Set Time");
                        tpd.setCancelable(false);
                        tpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        tpd.show();
                    }
                }, et.get(Calendar.YEAR), et.get(Calendar.MONTH), et.get(Calendar.DATE));
                dpd.setTitle("Set Date");
                dpd.setCancelable(false);
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

             //   dpd.getDatePicker().setMaxDate(cc.getTimeInMillis());
                dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dpd.show();
            }
        });

        return rootView;
    }

    // show food view when click on food tab
    public void foodView() {
        btn_food.setBackgroundResource(R.drawable.btn_fill_back);
        btn_food.setTextColor(getResources().getColor(R.color.white));
        btn_bottol.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_bottol.setBackgroundResource(R.drawable.btn_empty_back);

        list_bottol_type.setVisibility(View.GONE);
        list_food_type.setVisibility(View.VISIBLE);

        recycler_botle_meal_item.setVisibility(View.GONE);
        recycler_food_meal_itm.setVisibility(View.VISIBLE);

        list_food_type.setAdapter(new FoodTypeAdapter(getActivity(), types, 0));
        search.setVisibility(View.VISIBLE);
        line_mls.setVisibility(View.GONE);

        food_type = "food";
    }


    // show bottol view when click on bottol tab
    public void bottolView() {
        btn_bottol.setBackgroundResource(R.drawable.btn_fill_back);
        btn_bottol.setTextColor(getResources().getColor(R.color.white));
        btn_food.setTextColor(getResources().getColor(R.color.thene_blue));
        btn_food.setBackgroundResource(R.drawable.btn_empty_back);

        list_food_type.setVisibility(View.GONE);
        list_bottol_type.setVisibility(View.VISIBLE);

        recycler_botle_meal_item.setVisibility(View.VISIBLE);

        recycler_food_meal_itm.setVisibility(View.GONE);
        recycler_food_cnsmptn.setVisibility(View.GONE);
        recycler_food_meal_type.setVisibility(View.GONE);
        recycler_share_with.setVisibility(View.GONE);

        list_bottol_type.setAdapter(new FoodTypeAdapter(getActivity(), types_bottol, 0));
        search.setVisibility(View.VISIBLE);

        food_type = "bottol";

    }


    // get selected student ids
    public String getIds(ArrayList<String> sel_students) {
        String str = "";
        for (int i = 0; i < sel_students.size(); i++)
            str += "," + sel_students.get(i).split(";")[0];
        return str.length() > 0 ? str.substring(1) : str;
    }

    public void reset() {
        if(sel_students.size()==0){

            showDialog("Kindertag","Are you sure  without selecting student");

        }else {
            getActivity().invalidateOptionsMenu();
            ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add " + type);
            grid_students.setVisibility(View.GONE);
            ll_new_learning.setVisibility(View.VISIBLE);
            setStudents(ll_students);
        }


    }
    private void showDialog(String title, String message) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().invalidateOptionsMenu();
                        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add " + type);
                        grid_students.setVisibility(View.GONE);
                        ll_new_learning.setVisibility(View.VISIBLE);
                        setStudents(ll_students);

                    }
                }

                );


        if (myDialog != null) {
            myDialog.dismiss();
        }

        // Show the new dialog.
        myDialog = dialogBuilder.show();
    }

    public void set() {
        sel_students.clear();
        for (int i = 0; i < sel_students_temp.size(); i++)
            sel_students.add(sel_students_temp.get(i));
        reset();
    }

    public void refresh() {
        this.foods = ((HomeActivity) getActivity()).foods;
        food_list.clear();
        for (int i = 0; i < foods.size(); i++) {
            String[] fd = foods.get(i).split(";");
            food_list.add(new Item_Food(fd[0], fd[1], false));
        }
        if (sel_type == 0)
            recycler_food_meal_itm.setAdapter(new Food_Meal_Adapter(getActivity(), food_list, "f"));
    }


  // when click on add studetn set student to gridview
    // select students from that list
    public void setStudents(final LinearLayout ll) {

            ll.setVisibility(View.VISIBLE);
        ll.removeAllViews();
        final int max = sel_students.size() > 5 ? 7 : sel_students.size() + 2;
        for (int i = 0; i < max; i++) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40), getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40));
            llp.setMargins(i > 0 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5), i < max - 1 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5));
            if (i == max - 1) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTypeface(((HomeActivity) getActivity()).tf_material);
                txt.setTextColor(Color.WHITE);
                txt.setText(R.string.icon_plus);
                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ll_new_learning.setVisibility(View.GONE);
                        grid_students.setVisibility(View.VISIBLE);
                        sel_students_temp.clear();
                        for (int i = 0; i < sel_students.size(); i++)
                            sel_students_temp.add(sel_students.get(i));
                        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
                        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Select Student");
                        getActivity().invalidateOptionsMenu();
                    }
                });
                ll.addView(txt);
            } else if (i == max - 2) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                //txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTextColor(Color.BLACK);
                txt.setText((sel_students.size() - (max - 2)) + "+");
                txt.setVisibility(sel_students.size() > 5 ? View.VISIBLE : View.GONE);
                ll.addView(txt);
            } else {
                final ImageView img = new ImageView(getActivity());
                img.setLayoutParams(llp);
                img.setBackgroundResource(R.drawable.black_circle_back2);
                Glide.with(getActivity()).load(sel_students.get(i).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        if (getActivity() != null && img != null) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            img.setImageDrawable(circularBitmapDrawable);
                        }
                    }
                });
                ll.addView(img);
            }
        }
    }
    public void selectCheckIn(boolean selectCheckin){
        if(selectCheckin){
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                if(students.get(i).split(";")[4].equalsIgnoreCase("true")){
                    sel_students_temp.add(students.get(i));

                }
        } else

            sel_students_temp.clear();
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",false));
    }
    // select all students method
    public void setSelectionAll(boolean isSelectAll) {
        if (isSelectAll) {
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                sel_students_temp.add(students.get(i));
        } else
            sel_students_temp.clear();
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
    }


    // when capture photo or select photo then reach here onActivity result
    // get uri from data and set to image

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 3) {
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            imageUri = Uri.fromFile(photo);
            img_photo.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
        } else if (resultCode == Activity.RESULT_OK && requestCode == 7 && data != null) {
            imageUri = data.getData();
            img_photo.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
        }
    }

    // when permission granted then reach in request permission
    // according to permission fire that action
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Camera Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            startActivityForResult(camera, 3);
        } else if (requestCode == 3 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Camera Permission Not Granted!", Toast.LENGTH_SHORT).show();

        if (requestCode == 7 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Gallery Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, 7);
        } else if (requestCode == 7 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Gallery Permission Not Granted!", Toast.LENGTH_SHORT).show();
    }
}
