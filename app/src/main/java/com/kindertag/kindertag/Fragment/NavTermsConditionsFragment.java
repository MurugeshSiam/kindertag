package com.kindertag.kindertag.Fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.HelpListAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Alerts;

public class NavTermsConditionsFragment extends Fragment {
    Dialog myDialog = null;
    private long mLastClickTime = 0;
    private boolean clickable = true;

    public NavTermsConditionsFragment() {
    }

    String helps[];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            helps = getArguments().getStringArray("helps");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nav_menu_help_list, container, false);
        final ListView navMenuHeplList = (ListView) rootView.findViewById(R.id.list_nav_menu_help);
        navMenuHeplList.setAdapter(new HelpListAdapter(getActivity(), helps));
        navMenuHeplList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String s = navMenuHeplList.getAdapter().getItem(i).toString();
String lowercase=s.toLowerCase().split(" ")[0];
                switch (lowercase) {
                    case "terms":

                        terms();
                        break;
                    case "privacy":
                        policy();
                        break;
                    case "App version":
                        //about();
                        break;
                    case "video":
                     //   showDialog("Kindertag","Coming Soon...");
                        //  Alerts.showAlert("Coming Soon...",getActivity());
                        break;

                    default:
                        break;
                }
            }
        });
        return rootView;
    }
    public void about(){
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        String[] ss = {"Terms of service", "Privacy policy", "App version"};
        NavMenuHelpFragment nmhf = new NavMenuHelpFragment();
        Bundle b = new Bundle();
        b.putStringArray("helps", ss);
        nmhf.setArguments(b);
        ((HomeActivity) getActivity()).setActionTitle("About");
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fl_container, nmhf, "Help").addToBackStack("Help").commit();

    }

    public void terms(){
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        String terms = "terms";
        NavTermsPoilcyFragment nmhf = new NavTermsPoilcyFragment();
        Bundle b = new Bundle();
        b.putString("terms", terms);
        nmhf.setArguments(b);

        ((HomeActivity) getActivity()).setActionTitle("Terms of service");
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fl_container, nmhf, "FAQs").addToBackStack("FAQs").commit();
    }
    public void policy(){
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        String policy = "policy";
        NavTermsPoilcyFragment nmhf = new NavTermsPoilcyFragment();
        Bundle b = new Bundle();
        b.putString("terms", policy);
        nmhf.setArguments(b);

        ((HomeActivity) getActivity()).setActionTitle("Privacy Policy");
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fl_container, nmhf, "FAQs").addToBackStack("FAQs").commit();
    }

    public void Report(){
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@kidertag.in"});
        startActivity(intent);




    }
    private void showDialog(String title, String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });


        // Dismiss any old dialog.
        if (myDialog != null) {
            myDialog.dismiss();
        }

        // Show the new dialog.
        myDialog = dialogBuilder.show();
    }
    @Override
    public void onDetach() {
        super.onDetach();
        if (((HomeActivity) getActivity()).getActionTitle().equalsIgnoreCase("About"))
            ((HomeActivity) getActivity()).setActionTitle("Help");
    }
}
