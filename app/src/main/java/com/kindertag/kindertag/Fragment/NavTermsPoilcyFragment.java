package com.kindertag.kindertag.Fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.SharePref;
import com.kindertag.kindertag.Utils.SmartTextView;

import static com.kindertag.kindertag.Utils.KinderActivity.uid;

public class NavTermsPoilcyFragment extends Fragment {
TextView q1;
    SmartTextView terms_txt;
    public SharedPreferences pref = null;
String TermsPolicy;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            TermsPolicy = getArguments().getString("terms");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.termsconditions, container, false);
        terms_txt = (SmartTextView) rootView.findViewById(R.id.terms_txt);
        q1 = (TextView) rootView.findViewById(R.id.q1);
        if(TermsPolicy.equalsIgnoreCase("terms")){
            String terms= SharePref.getTerms(getContext());
            q1.setText("Terms of service");
            terms_txt.setText(terms);
        }else {
            String policy= SharePref.getPolicy(getContext());
            q1.setText("Policy Privacy");

            terms_txt.setText(policy);
        }
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivity)getActivity()).setActionTitle("About");
    }
}
