package com.kindertag.kindertag.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.RoomCheckListAdapter;
import com.kindertag.kindertag.R;

import java.util.ArrayList;

// show Room check fragment
public class NavMenuToolsRoomCheckFragment extends Fragment {
    ArrayList<String> ratios = new ArrayList<String>();

    public NavMenuToolsRoomCheckFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ratios = getArguments().getStringArrayList("ratio");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nav_menu_tools_room_check, container, false);
        final ListView list_room_check = (ListView) rootView.findViewById(R.id.list_nav_menu_tools_room_check);
        list_room_check.setAdapter(new RoomCheckListAdapter(getActivity(), ratios));
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivity) getActivity()).setActionTitle("Tools");
    }
}
