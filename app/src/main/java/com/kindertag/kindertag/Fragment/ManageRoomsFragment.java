package com.kindertag.kindertag.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.AllStudentsAdapter;
import com.kindertag.kindertag.Adapter.ManageRoomsAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

// manage rooms to use this Fragment
public class ManageRoomsFragment extends Fragment {

    // declare all varibales
    ArrayList<String> rooms = new ArrayList<String>();
    ArrayList<String> room_students = new ArrayList<String>();
    ListView list_rooms;
    GridView grid_students;
    String sel_room_id;
    String search = "";

    public ManageRoomsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            rooms = getArguments().getStringArrayList("rooms");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_manage_rooms, container, false);

        // find views declared in xml
        list_rooms = (ListView) rootView.findViewById(R.id.list_manage_rooms);
        grid_students = (GridView) rootView.findViewById(R.id.grid_manage_rooms_students);
        list_rooms.setAdapter(new ManageRoomsAdapter((HomeActivity) getActivity(), rooms));
        list_rooms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                sel_room_id = list_rooms.getAdapter().getItem(i).toString().split(";")[0];
                RequestParams params = new RequestParams();
                params.add("user_id", ((HomeActivity) getActivity()).uid);
                Common.callWebService((HomeActivity) getActivity(), Common.wb_getallstudent, params, "AllStudents");
            }
        });
        return rootView;
    }

    public void reset() {
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Manage Rooms");
        getActivity().invalidateOptionsMenu();
        grid_students.setVisibility(View.GONE);
        list_rooms.setVisibility(View.VISIBLE);
    }

    // set all students to gridview
    public void set(ArrayList<String> all_students) {
        this.room_students = all_students;
        this.search = "";
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("All Students");
        getActivity().invalidateOptionsMenu();
        list_rooms.setVisibility(View.GONE);
        grid_students.setVisibility(View.VISIBLE);
        grid_students.setAdapter(new AllStudentsAdapter(getActivity(), sel_room_id, room_students, this.search));
    }


    public void refresh_students(ArrayList<String> all_students, int pos) {
        this.room_students = all_students;
        if (grid_students.getVisibility() == View.VISIBLE) {
            grid_students.setAdapter(new AllStudentsAdapter(getActivity(), sel_room_id, room_students, this.search));
            grid_students.setSelection(pos);
        }
    }

    // refresh student list when add students
    public void refresh_students(String search) {
        this.search = search;
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new AllStudentsAdapter(getActivity(), sel_room_id, room_students, this.search));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((HomeActivity) getActivity()).setActionDrawer(false, "Rooms");
    }
}
