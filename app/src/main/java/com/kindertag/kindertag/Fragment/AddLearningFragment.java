package com.kindertag.kindertag.Fragment;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Adapter.HomeNewActivtyStudentsGridAdapter;
import com.kindertag.kindertag.Adapter.LearningTypeAdapter;
import com.kindertag.kindertag.Adapter.LearningValsAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

// this fragment use to add learning activity

public class AddLearningFragment extends Fragment {

    LinearLayout ll_new_learning, ll_students;
    EditText search;
    View view_search;
    TextView add_cat;
    ArrayList<String> students = new ArrayList<String>();
    ArrayList<String> sel_students = new ArrayList<String>();
    ArrayList<String> sel_students_temp = new ArrayList<String>();
    ListView list_type, list_vals;
    int sel_type = 0;
    String sel_cat = "", sel_scale = "", sel_progress = "", sel_share_with = "";
    EditText notes;
    TextView photo,txt_time;
    ImageView img_photo;
    Uri imageUri = null;
    GridView grid_students;
    String type;

    // create array for learning type
    final String types[] = {"Category", "Introduce", "Progress", "Share With", "Photo", "Notes"};
    ArrayList<String> catrgories = new ArrayList<String>();

    ArrayList<String> scale = new ArrayList<String>();
    ArrayList<String> progress = new ArrayList<String>();
    ArrayList<String> share = new ArrayList<String>();

    Calendar et = Calendar.getInstance();
    Calendar cc = Calendar.getInstance();
    //ArrayList<String> vals= new ArrayList<String>();
    public AddLearningFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            type = getArguments().getString("type", "");
        }
        students = ((HomeActivity) getActivity()).getStudentsforActivities();
        catrgories = ((HomeActivity) getActivity()).getCategories();
        Log.i("cat 0", catrgories.get(0));

        // add value in scale
        scale.add("Earlier");
        scale.add("On-Time");
        scale.add("Later");
        sel_scale = scale.get(0);

        // add value in progress list
        progress.add("Introduced");
        progress.add("Observing");
        progress.add("Developing");
        progress.add("Mastered");
        sel_progress = progress.get(0);

        share.add("Parents and Staff");
        share.add("Staff only");
        sel_share_with = share.get(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_learning, container, false);
        ll_new_learning = (LinearLayout) rootView.findViewById(R.id.ll_new_learning);
        ll_students = (LinearLayout) rootView.findViewById(R.id.ll_new_learning_students);
        search = (EditText) rootView.findViewById(R.id.edit_new_learning_search);
        view_search = rootView.findViewById(R.id.view_edit_new_learning_search);
        add_cat = (TextView) rootView.findViewById(R.id.txt_list_vals_category_add);
        txt_time = (TextView) rootView.findViewById(R.id.txt_time);
        list_type = (ListView) rootView.findViewById(R.id.list_new_learning_type);
        list_vals = (ListView) rootView.findViewById(R.id.list_new_learning_sel_vals);
        photo = (TextView) rootView.findViewById(R.id.txt_new_learning_photo);
        img_photo = (ImageView) rootView.findViewById(R.id.img_new_learning_photo);
        notes = (EditText) rootView.findViewById(R.id.edit_new_learning_notes);
        final Button btn_create = (Button) rootView.findViewById(R.id.btn_new_learning_create);

        // add textwathcer in search view
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (search.getVisibility() == View.VISIBLE && sel_type == 0) {
                    list_vals.setAdapter(new LearningValsAdapter(getActivity(), search.getText().toString(), sel_type == 0, catrgories, sel_cat));
                }
            }
        });

        // set click listener add category
        add_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // show dialog for add category
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(true);
                builder.setTitle("Add New Category");
                final EditText cat_name = new EditText(getActivity());
                cat_name.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                cat_name.setHint("Category Name");
                cat_name.setTextColor(Color.BLACK);
                cat_name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                cat_name.setMaxLines(1);
                builder.setView(cat_name);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (cat_name.getText().toString().trim().length() > 0) {
                            RequestParams params = new RequestParams();
                            params.add("room_id", ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                            params.add("categoryName", cat_name.getText().toString().trim());
                            Common.callWebService((HomeActivity) getActivity(), Common.wb_addcategory, params, "AddCategory" + "%" + cat_name.getText().toString().trim());
                        }
                    }
                });
                builder.create().show();
            }
        });


        // set adapter to list types
        list_type.setAdapter(new LearningTypeAdapter(getActivity(), types, sel_type));

        // set adapter to values
        list_vals.setAdapter(new LearningValsAdapter(getActivity(), search.getText().toString(), sel_type == 0, catrgories, sel_cat));

        // set On ItemClickListener to select to type
        list_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                sel_type = i;
                list_type.setAdapter(new LearningTypeAdapter(getActivity(), types, sel_type));
                String name = list_type.getAdapter().getItem(i).toString();
                list_vals.setVisibility(!name.toLowerCase().matches("(photo|notes)") ? View.VISIBLE : View.GONE);
                photo.setVisibility(name.equalsIgnoreCase("photo") ? View.VISIBLE : View.GONE);
                img_photo.setVisibility((name.equalsIgnoreCase("photo") && img_photo.getDrawable() != null) ? View.VISIBLE : View.GONE);
                notes.setVisibility(name.equalsIgnoreCase("notes") ? View.VISIBLE : View.GONE);
                search.setVisibility(name.equalsIgnoreCase("category") ? View.VISIBLE : View.GONE);
                search.setText("");
                view_search.setVisibility(name.equalsIgnoreCase("category") ? View.VISIBLE : View.GONE);
                add_cat.setVisibility(name.equalsIgnoreCase("category") ? View.VISIBLE : View.GONE);
                if (!name.matches("(Photo|Notes)")) {
                    ArrayList<String> vals = name.equalsIgnoreCase("category") ? catrgories :
                            name.equalsIgnoreCase("introduce") ? scale :
                                    name.equalsIgnoreCase("progress") ? progress :
                                            name.equalsIgnoreCase("share with") ? share : new ArrayList<String>();

                    String sel = name.equalsIgnoreCase("category") ? sel_cat :
                            name.equalsIgnoreCase("introduce") ? sel_scale :
                                    name.equalsIgnoreCase("progress") ? sel_progress :
                                            name.equalsIgnoreCase("share with") ? sel_share_with : "";
                    list_vals.setAdapter(new LearningValsAdapter(getActivity(), search.getText().toString(), sel_type == 0, vals, sel));
                    if (name.equalsIgnoreCase("category"))
                        list_vals.setSelection(i);
                }

            }
        });

        // set On ItemClickListener on list values for select category values

        list_vals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long l) {
                final String ss = list_vals.getAdapter().getItem(pos).toString();
                final String name = list_type.getAdapter().getItem(sel_type).toString();
                if (!name.matches("(Photo|Notes)")) {
                    if (name.equalsIgnoreCase("category")) {
                        if (!sel_cat.contains(ss))
                            sel_cat += "," + ss;
                        else
                            sel_cat = sel_cat.replace("," + ss, "");
                    } else if (name.equalsIgnoreCase("introduce"))
                        sel_scale = ss;
                    else if (name.equalsIgnoreCase("progress"))
                        sel_progress = ss;
                    else if (name.equalsIgnoreCase("share with"))
                        sel_share_with = ss;

                    ArrayList<String> vals = name.equalsIgnoreCase("category") ? catrgories :
                            name.equalsIgnoreCase("introduce") ? scale :
                                    name.equalsIgnoreCase("progress") ? progress :
                                            name.equalsIgnoreCase("share with") ? share : new ArrayList<String>();

                    String sel = name.equalsIgnoreCase("category") ? sel_cat :
                            name.equalsIgnoreCase("introduce") ? sel_scale :
                                    name.equalsIgnoreCase("progress") ? sel_progress :
                                            name.equalsIgnoreCase("share with") ? sel_share_with : "";
                    list_vals.setAdapter(new LearningValsAdapter(getActivity(), search.getText().toString(), sel_type == 0, vals, sel));
                } else if (name.equalsIgnoreCase("Photo")) {
                    //Code Here
                }
            }
        });


        // add photo in learning activity

        photo.setBackgroundDrawable(new EditText(getActivity()).getBackground());
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Photo", R.drawable.ic_gallery + ";" + "  Choose Image"};
                ListAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, android.R.id.text1, items) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        //Use super class to create the View
                        View v = super.getView(position, convertView, parent);
                        TextView tv = (TextView) v.findViewById(android.R.id.text1);
                        //Put the image on the TextView
                        tv.setText(items[position].split(";")[1]);
                        tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                        return v;
                    }
                };

                // show AlertDialog for show dialog for camera and gallery

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Action");
                builder.setCancelable(true);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddLearningFragment.this.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                } else {
                                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
                                    camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                    startActivityForResult(camera, 3);
                                }
                                break;
                            case 1:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    AddLearningFragment.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                } else {
                                    Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    gallery.setType("image/*");
                                    startActivityForResult(gallery, 7);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                });
                builder.show();
            }
        });

        grid_students = (GridView) rootView.findViewById(R.id.grid_new_learning_students);
        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));

        setStudents(ll_students);

        // call add learning activity
        // pass all parameters related add learning activity

        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //clearAllErrors();
                if (sel_students.size() == 0)
                    Toast.makeText(getActivity(), "Select atleast 1 Student", Toast.LENGTH_SHORT).show();
                else if (sel_cat.length() <= 1)
                    Toast.makeText(getActivity(), "Select atleast 1 Category", Toast.LENGTH_SHORT).show();
                else {
                    String student_ids = getIds(sel_students);
                    Log.i("sel_cat", sel_cat);
                    RequestParams params = new RequestParams();
                    params.add("room_id", ((HomeActivity) getActivity()).sel_room.split(";")[0]);
                    params.add("student_ids", student_ids);
                    params.add("category_name", sel_cat.substring(1));
                    params.add("scale", sel_scale);
                    params.add("progress", sel_progress);
                    params.add("isstaff", sel_share_with.equalsIgnoreCase("Staff only") ? "1" : "0");
                    params.add("note", notes.getText().toString());
                    params.add("photo", Common.genrateByteArrayString(getActivity(), imageUri));
                    params.add("activity_date", txt_time.getText().toString().replace("Time : ", ""));

                    Log.e("params",params.toString());
                    Common.callWebService((HomeActivity) getActivity(), Common.wb_addlearningactivity, params, "AddLearning" + "%" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + ";" + type + ";" + ((HomeActivity) getActivity()).toString(sel_students) + ";" + "" + ";" + "photos" + ";" + sel_share_with.equalsIgnoreCase("Staff only"));
                }
            }
        });


        Date d = new Date();
        CharSequence s = DateFormat.format("yyyy-MM-dd HH:mm:ss", d.getTime());
        txt_time.setText("Time : " + s);

        // set on click on time text
        txt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int MM, int dd) {
                        et.set(yyyy, MM, dd);
                        TimePickerDialog tpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int HH, int mm) {
                                et.set(Calendar.HOUR_OF_DAY, HH);
                                et.set(Calendar.MINUTE, mm);
                                et.set(Calendar.SECOND, 0);
                                txt_time.setText("Time : " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(et.getTime()));
                            }
                        }, et.get(Calendar.HOUR_OF_DAY), et.get(Calendar.MINUTE), true);
                        tpd.setTitle("Set Time");
                        tpd.setCancelable(false);
                        tpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        tpd.show();

                        //set date and time to editext
                    }
                }, et.get(Calendar.YEAR), et.get(Calendar.MONTH), et.get(Calendar.DATE));
                dpd.setTitle("Set Date");
                dpd.setCancelable(false);
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dpd.getDatePicker().setMaxDate(cc.getTimeInMillis());
                dpd.show();
            }
        });


        return rootView;
    }

    // get ids of selected students
    public String getIds(ArrayList<String> sel_students) {
        String str = "";
        for (int i = 0; i < sel_students.size(); i++)
            str += "," + sel_students.get(i).split(";")[0];
        return str.length() > 0 ? str.substring(1) : str;
    }

    public void reset() {
        getActivity().invalidateOptionsMenu();
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add " + type);
        grid_students.setVisibility(View.GONE);
        ll_new_learning.setVisibility(View.VISIBLE);
        setStudents(ll_students);
    }

    public void set() {
        sel_students.clear();
        for (int i = 0; i < sel_students_temp.size(); i++)
            sel_students.add(sel_students_temp.get(i));
        reset();
    }

    public void refresh() {
        this.catrgories = ((HomeActivity) getActivity()).categories;
        if (sel_type == 0)
            list_vals.setAdapter(new LearningValsAdapter(getActivity(), search.getText().toString(), sel_type == 0, catrgories, sel_cat));
    }

    public void refresh_students(String search) {
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, search,true));
    }

    // set students to gridveiw for selection
    public void setStudents(final LinearLayout ll) {
        ll.setVisibility(View.VISIBLE);
        ll.removeAllViews();
        final int max = sel_students.size() > 5 ? 7 : sel_students.size() + 2;
        for (int i = 0; i < max; i++) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40), getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_40));
            llp.setMargins(i > 0 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5), i < max - 1 ? getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.dp_5));
            if (i == max - 1) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTypeface(((HomeActivity) getActivity()).tf_material);
                txt.setTextColor(Color.WHITE);
                txt.setText(R.string.icon_plus);
                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ll_new_learning.setVisibility(View.GONE);
                        grid_students.setVisibility(View.VISIBLE);
                        sel_students_temp.clear();
                        for (int i = 0; i < sel_students.size(); i++)
                            sel_students_temp.add(sel_students.get(i));
                        grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
                        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Select Student");
                        getActivity().invalidateOptionsMenu();
                    }
                });
                ll.addView(txt);
            } else if (i == max - 2) {
                TextView txt = new TextView(getActivity());
                txt.setLayoutParams(llp);
                //txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txt.setTextColor(Color.BLACK);
                txt.setText((sel_students.size() - (max - 2)) + "+");
                txt.setVisibility(sel_students.size() > 5 ? View.VISIBLE : View.GONE);
                ll.addView(txt);
            } else {
                final ImageView img = new ImageView(getActivity());
                img.setLayoutParams(llp);
                img.setBackgroundResource(R.drawable.black_circle_back2);
                Glide.with(getActivity()).load(sel_students.get(i).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        if (getActivity() != null && img != null) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            img.setImageDrawable(circularBitmapDrawable);
                        }
                    }
                });
                ll.addView(img);
            }
        }
    }

    public void selectCheckIn(boolean selectCheckin){
        if(selectCheckin){
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                if(students.get(i).split(";")[4].equalsIgnoreCase("true")){
                    sel_students_temp.add(students.get(i));

                }
        } else

            sel_students_temp.clear();
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",false));
    }
    // select all students

    public void setSelectionAll(boolean isSelectAll) {
        if (isSelectAll) {
            sel_students_temp.clear();
            for (int i = 0; i < students.size(); i++)
                sel_students_temp.add(students.get(i));
        } else
            sel_students_temp.clear();
        if (grid_students.getVisibility() == View.VISIBLE)
            grid_students.setAdapter(new HomeNewActivtyStudentsGridAdapter(getActivity(), students, sel_students_temp, "",true));
    }

    // when capture photo or select photo then reach here onActivity result
    // get uri from data and set to image

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 3) {
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            imageUri = Uri.fromFile(photo);
            img_photo.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
        } else if (resultCode == Activity.RESULT_OK && requestCode == 7 && data != null) {
            imageUri = data.getData();
            img_photo.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(imageUri).asBitmap().centerCrop().into(img_photo);
        }
    }

    // when permission granted then reach in request permission
    // according to permission fire that action

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Camera Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photo = new File(Environment.getExternalStorageDirectory(), "temp.png");
            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            startActivityForResult(camera, 3);
        } else if (requestCode == 3 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Camera Permission Not Granted!", Toast.LENGTH_SHORT).show();

        if (requestCode == 7 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Gallery Permission Granted!", Toast.LENGTH_SHORT).show();
            Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, 7);
        } else if (requestCode == 7 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(getActivity(), "Gallery Permission Not Granted!", Toast.LENGTH_SHORT).show();
    }

}
