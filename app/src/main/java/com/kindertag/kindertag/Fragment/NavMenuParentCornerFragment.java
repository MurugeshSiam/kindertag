package com.kindertag.kindertag.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kindertag.kindertag.R;

public class NavMenuParentCornerFragment extends Fragment {

 public NavMenuParentCornerFragment() {}
 @Override
 public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
 }

 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
  View rootView = inflater.inflate(R.layout.fragment_nav_menu_parent_corner, container, false);

  return rootView;
 }
}
