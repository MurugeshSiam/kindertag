package com.kindertag.kindertag.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.mime.MultipartEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

// declare all webservices here

public class Common {
    public static final String PUSH_NOTIFICATION = "pushNotification";
    //Domain Name
   // public static String domain = "https://kindertag.in/admin/webapi/";
    public static String domain = "http://kindertag.underdev.in/webapi/";
    //Webservice Urls
    public static String wb_register = domain + "register";//namesurname=Su dam&email=su_dam@gmail.com&phone=9403034830&pass=sudam@123&userType=2
    public static String wb_login = domain + "login";//uname=nileshpoman.969@gmail.com&pass=admin@123
    public static String wb_roomlist = domain + "roomlist";//user_id=2
    public static String wb_addroom = domain + "addroom";//user_id=2,roomName="Sudam's Room",roomAddress="Pune"
    public static String wb_editroom = domain + "editroom";//user_id=2,room_id=3&roomName="Room 1"&roomAddress="Pune"
    public static String wb_deleteroom = domain + "deleteroom";//room_id=3
    public static String wb_activityType = domain + "activityType";//user_id=3 & role_id=2
    public static String wb_getSettings = domain + "getSettings";
    public static String wb_studentlist = domain + "studentlist";//room_id=1
    public static String wb_addstudent = domain + "addstudent";//room_id=1&firstName="Sudam"&lastName="Chole"&birthdate="1989-03-02"&photo="bytestring"
    public static String wb_editstudent = domain + "editStudent";//student_id=6&firstName="Sudam"&lastName="Chole"&birthdate="1989-03-02"&photo="bytestring"
    public static String wb_deletestudent = domain + "deleteStudent";//student_id=6;
    public static String wb_eventlist = domain + "eventlist";//room_id=1&user_id=2
    public static String wb_addevent = domain + "addevent";//room_id=1&eventTitle="Parent Meeting"&eventDesc="Description"&eventDate="yyyy-MM-dd"&user_id=2&event_location="pune"
    public static String wb_updateevent = domain + "update_event";//user_id=26&eventid=3&status=accept/decline
    public static String wb_getactivity = domain + "getActivity";//student_id=1
    public static String wb_getroomactivity = domain + "getroomactivity";//room_id=1
    public static String wb_getlearningcategory = domain + "getLearningCategory";//room_id=1
    public static String wb_getfooditems = domain + "getFooditem";//room_id=1
    public static String wb_addcategory = domain + "addCategory";//room_id=1&categoryName="Arts"
    public static String wb_addFooditem = domain + "addFooditem";//room_id=1&categoryName="Arts"
    public static String wb_addfoodactivity = domain + "addFoodActivity";//room_id=1,student_ids=1,2,3,category_name="Arts",scale="Earlier",progress="Introduced",note="Note",isstaff=0,photo=<ByteArrayStr>
    public static String wb_addnapactivity = domain + "addActivity";//room_id=1,student_ids=1,2,3,category_name="Arts",scale="Earlier",progress="Introduced",note="Note",isstaff=0,photo=<ByteArrayStr>
    public static String wb_addlearningactivity = domain + "addLearningActivity";//room_id=1,student_ids=1,2,3,category_name="Arts",scale="Earlier",progress="Introduced",note="Note",isstaff=0,photo=<ByteArrayStr>
    public static String wb_addactivity = domain + "addActivity";//room_id=1&student_id=1,2,3&note="blahblah"&isstaff=0/1&activity_type=praise/photo/notes&activity_date=2017-06-06
    public static String wb_medicine = domain + "addMedicine";//room_id=1&student_id=1,2,3&note="blahblah"&isstaff=0/1&activity_type=praise/photo/notes&activity_date=2017-06-06
    public static String wb_addphoto = domain + "addPhoto";//room_id=1&student_ids=1,2,3&isstaff=0/1&activity_date=2017-06-06&image1,&image2,&image3,&image4
    public static String wb_getteacher = domain + "getTeacher";//room_id=1
    public static String wb_getusers = domain + "getUsers";//room_id=1&user_type=parent/teacher/owner
    public static String wb_getmessage = domain + "getMessages";//user_id=1
    public static String wb_sendmessage = domain + "sendMessage";//sender_id=uid&receiver_id=oud&messageText="msg"
    public static String wb_attendance = domain + "attendance";//room_id=1,student_ids=1,2,3
    public static String wb_absent = domain + "absent";//room_id=1,student_ids=1,2,3
    public static String wb_absent_teacher = domain + "absentTeacher";//room_id=1,student_ids=1,2,3
    public static String wb_checkout = domain + "checkOut";//room_id=1,student_ids=1,2,3
    public static String wb_checkin_teacher = domain + "checkinTeacher";//room_id=1,teacher_ids=1,2,3
    public static String wb_checkout_teacher = domain + "checkoutTeacher";//teacher_ids=1,2,3
    public static String wb_joincode = domain + "joinSchool";//user_id=2,invite_code=1159
    public static String wb_joinschooolforteacher = domain + "joinSchoolForTecher";//user_id=2,room_code=1159
    public static String wb_getstudent = domain + "getStudent";//parent_id=1
    public static String wb_getlearningactivity = domain + "getLearningActivity";//room_id=1&student_id=1
    public static String wb_sendotp = domain + "sendOtp";//user_id=1
    public static String wb_checkotp = domain + "checkOtp";//user_id=1
    public static String wb_parentCheckOtp = domain + "parentCheckOtp";//user_id=1
    public static String wb_resetPass = domain + "resetPass";//user_id=1
    public static String wb_getadmindata = domain + "getAdminData";//user_id=1
    public static String wb_getratio = domain + "getRatio";//user_id=1
    public static String wb_getallstudent = domain + "getAllStudent";//user_id=1
    public static String wb_addstudentroom = domain + "addStudentRoom";//room_id=1,student_id=1
    public static String wb_removeroom = domain + "removeRoom";//student_id=1
    public static String seprator = "%#%";
    public static String rootPath = Environment.getExternalStorageDirectory() + File.separator + ".Kindertag";

    public static String chkNull(String str, String def) {
        return (str != null && str.trim().length() > 0 && !str.trim().equalsIgnoreCase("null")) ? str : def;
    }

    public static String chkNull(JSONObject jobj, String key, String def) {
        try {
            return (jobj != null && jobj.has(key) && jobj.getString(key) != null && jobj.getString(key).trim().length() > 0 && !jobj.getString(key).trim().equalsIgnoreCase("null")) ? jobj.getString(key).trim() : def;
        } catch (JSONException e) {
            e.printStackTrace();
            return def;
        }
    }

 /*public static boolean validate(EditText et,int minLen,boolean isMandatory){
  String hint = et.getHint().toString().replace("Choose","").replace("Enter","").replace("New","");
  et.setText(et.getText().toString().trim());
  String txt=et.getText().toString();
  String tag=(et.getTag()!=null)?et.getTag().toString():"";
  if(txt.length()==0){
   if(isMandatory) {
    //til.setErrorEnabled(true);
    et*//*til*//*.setError(hint + " is Mandatory");
    et*//*til*//*.requestFocus();
   }
  }
  else if(txt.length()<minLen){
   //til.setErrorEnabled(true);
   et*//*til*//*.setError(tag.equals("password")?"Minimum password length is "+minLen+" and it must contain at least 1 number,1 letter & 1 special symbol":"Invalid "+hint);
   et*//*til*//*.requestFocus();
  }
  else if(!tag.equals("")){
   if((tag.equals("name") && !txt.matches("^[a-zA-Z]+$")) ||
    (tag.equals("email") && !txt.matches("^[A-Za-z0-9._%+-]+@[A-Za-z]+\\.[A-Za-z]{2,}")) ||
    (tag.equals("mobile") && !txt.matches("^[789]\\d{9}$")) ||
    (tag.equals("password") && !txt.matches("^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])([!@#$%^&*a-zA-Z0-9]+)$"))){
    //til.setErrorEnabled(true);
    et*//*til*//*.setError(tag.equals("password")?"Password must contain at least 1 number,1 letter & 1 special symbol":"Invalid "+hint);
    et*//*til*//*.requestFocus();
   }
  }
  return et*//*til*//*.getError()==null;
 }*/

    public static String chkNull(String str, String seprator, int index, String def) {
        return (str != null && str.trim().length() > 0 && !str.trim().equalsIgnoreCase("null") &&
                str.contains(seprator) && str.split(seprator).length > index && str.split(seprator)[index].trim().length() > 0 &&
                !str.split(seprator)[index].trim().equalsIgnoreCase("null")) ? str.split(seprator)[index].trim() : def;
    }

    public static boolean validate(TextInputLayout til, int minLen, boolean isMandatory) {
        EditText et = til.getEditText();
        String hint = til.getHint().toString().replace("Choose", "").replace("Enter", "").replace("New", "");
        et.setText(et.getText().toString().trim());
        String txt = et.getText().toString();
        String tag = (et.getTag() != null) ? et.getTag().toString() : "";
        if (txt.length() == 0) {
            if (isMandatory) {
                //til.setErrorEnabled(true);
                et/*til*/.setError(hint + " is Mandatory");
                et/*til*/.requestFocus();
            }
        } else if (txt.length() < minLen) {
            //til.setErrorEnabled(true);
            et/*til*/.setError(tag.equals("password") ? "Minimum password length is " + minLen + " and it must contain at least 1 number,1 letter & 1 special symbol" : "Invalid " + hint);
            et/*til*/.requestFocus();
        } else if (!tag.equals("")) {
            if ((tag.equals("name") && !txt.matches("^[a-zA-Z]+$")) ||
                    (tag.equals("email") && !txt.matches("^[A-Za-z0-9._%+-]+@[A-Za-z]+\\.[A-Za-z]{2,}")) ||
                    (tag.equals("mobile") && !txt.matches("^[789]\\d{9}$")) ||
                    (tag.equals("password") && !txt.matches("^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])([!@#$%^&*a-zA-Z0-9]+)$"))) {
                //til.setErrorEnabled(true);
                et/*til*/.setError(tag.equals("password") ? "Password must contain at least 1 number,1 letter & 1 special symbol" : "Invalid " + hint);
                et/*til*/.requestFocus();
            }
        }
        return et/*til*/.getError() == null;
    }

    public static String genrateByteArrayString(Context context, Uri imgUri) {
        if (imgUri == null || imgUri.equals(""))
            return "";
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(imgUri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            Bitmap b;
            if (height > 500 || width > 500) {
                double ratio = (width > height) ? bitmap.getWidth() / bitmap.getHeight() : bitmap.getHeight() / bitmap.getWidth();
                int nwid = 0, nhei = 0;
                if (width > height) {
                    nwid = width / (width / 500);
                    nhei = (int) (nwid / ratio);
                } else if (height > width) {
                    nhei = height / (height / 500);
                    nwid = (int) (nhei / ratio);
                } else if (height == width) {
                    nwid = width / (width / 500);
                    nhei = height / (height / 500);
                }
                b = Bitmap.createScaledBitmap(bitmap, nwid, nhei, false);
            } else
                b = bitmap;

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG, 100, baos);
            return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean validate(TextView et, int minLen, boolean isMandatory) {
        String hint = et.getHint().toString().replace("Choose", "").replace("Enter", "").replace("New", "");
        et.setText(et.getText().toString().trim());
        String txt = et.getText().toString();
        String tag = (et.getTag() != null) ? et.getTag().toString() : "";
        if (txt.length() == 0) {
            if (isMandatory) {
                if (tag.equalsIgnoreCase("dialog"))
                    et.setFocusableInTouchMode(true);
                //til.setErrorEnabled(true);
                et/*til*/.setError(hint + " is Mandatory");
                et/*til*/.requestFocus();
            }
        } else if (txt.length() < minLen) {
            //til.setErrorEnabled(true);
            et/*til*/.setError(tag.equals("password") ? "Minimum password length is " + minLen + " and it must contain at least 1 number,1 letter & 1 special symbol" : "Invalid " + hint);
            et/*til*/.requestFocus();
        } else if (!tag.equals("")) {
            if ((tag.equals("name") && !txt.matches("^[a-zA-Z]+$")) ||
                    (tag.equals("email") && !txt.matches("^[A-Za-z0-9._%+-]+@[A-Za-z]+\\.[A-Za-z]{2,}")) ||
                    (tag.equals("mobile") && !txt.matches("^[789]\\d{9}$")) ||
                    (tag.equals("password") && !txt.matches("^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])([!@#$%^&*a-zA-Z0-9]+)$"))) {
                //til.setErrorEnabled(true);
                et/*til*/.setError(tag.equals("password") ? "Password must contain at least 1 number,1 letter & 1 special symbol" : "Invalid " + hint);
                et/*til*/.requestFocus();
            }
        }
        return et/*til*/.getError() == null;
    }

    public static String toString(ArrayList<String> list) {
        String str = "";
        for (int i = 0; i < list.size(); i++)
            str += seprator + list.get(i);
        return str.length() > 0 ? str.substring(seprator.length()) : str;
    }

    public static ArrayList<String> toList(String str) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < str.split(seprator).length; i++)
            list.add(str.split(seprator)[i]);
        return list;
    }

    public static boolean saveImage(Bitmap image, String name) {

        if (image == null)
            return false;
        Log.i("name1", name);
        File directory = new File(rootPath);
        directory.mkdirs();
        File pictureFile = new File(rootPath, name);
        if (pictureFile.exists() && pictureFile.length() > 0)
            return true;
        else if (!pictureFile.exists()) {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static String getImageFilePath(String name) {
        File directory = new File(rootPath);
        directory.mkdirs();
        File pictureFile = new File(rootPath, name);
        if (pictureFile.exists() && pictureFile.length() > 0)
            return pictureFile.getAbsolutePath();
        return "";
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }


    public static void callWebService(final KinderActivity activity, final String url, final RequestParams params, final String tag) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
            final ProgressDialog pd = new ProgressDialog(activity);//,R.style.MyTheme);
            //pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            pd.setMessage("Please Wait...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);

            new AsyncHttpClient().post(activity, url, params, new AsyncHttpResponseHandler() {
                public void onStart() {
                    super.onStart();
                    pd.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    if (pd != null && pd.isShowing()) pd.dismiss();
                    activity.onSuccess(new String(responseBody), tag);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    if (pd != null && pd.isShowing()) pd.dismiss();
                    String msg = statusCode == 400 ? "Bad Request" : statusCode == 401 ? "Unauthorized" : statusCode == 403 ? "Forbidden" : statusCode == 404 ? "Not Found" :
                            statusCode == 500 ? "Internal Server Error" : statusCode == 550 ? "Permission denied" : "Something went wrong at server side";
                  //  Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (pd != null && pd.isShowing()) {
                        pd.dismiss();
                        Toast.makeText(activity, "Connection timed out!\nData may not be updated!", Toast.LENGTH_SHORT).show();
                    }
                }
            }, 60000);
        } else
            Toast.makeText(activity, "Please check the Internet Connection!", Toast.LENGTH_SHORT).show();
    }
}
