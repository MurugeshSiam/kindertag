package com.kindertag.kindertag.Utils;

import android.app.Application;
import android.os.StrictMode;

/**
 * Created by Nysa on 29-Jan-18.
 */


// this class is used for open camera for NOUGHT specially
public class Myapp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

    }
}
