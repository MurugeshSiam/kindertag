package com.kindertag.kindertag.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Nysa on 27-Sep-17.
 */

// this is used for save value
// login values and other values used in webservice.
public class SharePref {

    public static void setRecever(Context context, String receiver, String student_id, String roomid, String parent) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("receiver_id", receiver);
        editor.putString("student_id", student_id);
        editor.putString("room_id", roomid);
        editor.putString("parent", parent);
        editor.commit();
    }

    public static void setstdid(Context context, String stdid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("stdid", stdid);
        editor.commit();
    }

    public static void setfirstlogin(Context context, String stdid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("flogin", stdid);
        editor.commit();
    }

    public static String getfirstlogin(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("flogin", "1");
    }
    public static void setnotification(Context context, Boolean stdid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("notification", stdid);
        editor.commit();
    }

    public static boolean getnotification(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getBoolean("notification", true);
    }
    public static void setTerms(Context context, String stdid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("terms", stdid);
        editor.commit();
    }

    public static String getTerms(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("terms", "0");
    }
    public static void setPolicy(Context context, String stdid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Policy", stdid);
        editor.commit();
    }

    public static String getPolicy(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("Policy", "0");
    }
    public static String getReceiver(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("receiver_id", "0");
    }


    public static String getStudent(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("student_id", "0");
    }

    public static String getParent(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("parent", "");
    }

    public static String getRoomid(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("room_id", "1");
    }

    public static String getstdid(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("stdid", "0");
    }


    public static void setParentPin(Context context, String stdid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("pin", stdid);
        editor.commit();
    }

    public static String getParentPin(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("newkinder", MODE_PRIVATE);

        return sharedPreferences.getString("pin", "00");
    }
}
