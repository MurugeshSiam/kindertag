package com.kindertag.kindertag.Utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by bhupen on 19/7/17.
 */

// This is used for show notification
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    //private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("From: ",remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("Notification Body: ",remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e("Data",remoteMessage.getData().toString());
            Log.e("Data Change",remoteMessage.getData().toString().replace("{","{\"").replace("}","\"}").replace(", ","\",\"").replace("=","\":\""));

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString().replace("{","{\"").replace("}","\"}").replace(", ","\",\"").replace("=","\":\""));
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e("Exception: ",e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Common.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject data) {
        //Log.e("push json: ",json.toString());

        try {
            //JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String type = data.getString("notifytype");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            //String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            String notidata = Common.chkNull(data,"notifdata","1");//data.getString("notifdata");
            //JSONObject payload = data.getJSONObject("payload");

            SharePref.setRecever(getBaseContext(),"","","","");

            if (type.equalsIgnoreCase("message"))
            {
                SharePref.setRecever(getBaseContext(),data.getString("receiver_id"),data.getString("student_id"),data.getString("room_id"),"Parent");
            }

            Log.e("title: ",title);
            Log.e("message: ",message);
            Log.e("type: ",type);
            Log.e("isBackground: ",""+isBackground);
            //Log.e("payload: ",payload.toString());
            //Log.e("imageUrl: ",imageUrl);
            Log.e("timestamp: ",timestamp);
            Log.e("notidata: ",notidata);


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Common.PUSH_NOTIFICATION);
                pushNotification.putExtra("title", title);
                pushNotification.putExtra("message", message);
                pushNotification.putExtra("type",type);
                pushNotification.putExtra("notidata",notidata);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);


                // play notification sound
                //NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                //notificationUtils.playNotificationSound();
            } else {
                // app is in background, show the notification in notification tray
                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("tab",type);
                PendingIntent pIntent = PendingIntent.getActivity(this,(int) System.currentTimeMillis(),intent, PendingIntent.FLAG_CANCEL_CURRENT);
                //resultIntent.putExtra("message", message);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {

                    Notification noti = new Notification.Builder(getApplicationContext())
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(pIntent)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .build();

                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                    notificationManager.notify(Integer.parseInt(notidata),noti);
                }

                // check for image attachment
                //if (TextUtils.isEmpty(imageUrl)) {
                    //showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                //} else {
                    // image is present, show notification with image
                 //   showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                //}
            }
        } catch (JSONException e) {
            Log.e("Json Exception: ",e.getMessage());
        } catch (Exception e) {
            Log.e("Exception: ",e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
