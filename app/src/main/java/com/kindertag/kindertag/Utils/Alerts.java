package com.kindertag.kindertag.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

// this is used to show alert dialog
public class Alerts {

    public static void showAlert(String msg,Context context){
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle("Kindertag");
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
