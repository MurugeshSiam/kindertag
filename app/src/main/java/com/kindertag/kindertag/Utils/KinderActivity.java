package com.kindertag.kindertag.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by bhupen on 21/5/17.
 */


// this activity is used to base activity to all activity

public class KinderActivity extends AppCompatActivity {

    public SharedPreferences pref = null;
    public static String uid;
    public Typeface tf_awsome, tf_material;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getSharedPreferences("Kindertag", Context.MODE_PRIVATE);
        uid = pref.getString("user_id", "");
        Log.i("uid", uid);
        tf_awsome = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(getAssets(), "MaterialIcons-Regular.ttf");
    }

    public void onSuccess(String response, String tag) {
    }

}
