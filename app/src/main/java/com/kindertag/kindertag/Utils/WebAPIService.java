package com.kindertag.kindertag.Utils;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Murugesh on 30-07-2018.
 */

public interface WebAPIService {

    @Multipart
    @POST("/webapi/changePassword")
    Call<ResponseBody> postFile_p(@Part("user_id") RequestBody user_id, @Part("password") RequestBody password);

    @Multipart
     @POST("/webapi/addPhoto")
    Call<ResponseBody> postFile_A(@Part MultipartBody.Part image1,@Part MultipartBody.Part image2,@Part MultipartBody.Part image3,@Part MultipartBody.Part image4,
                                  @Part("activity_date") RequestBody activity_date, @Part("activity_text") RequestBody activity_text, @Part("isstaff") RequestBody isstaff, @Part("room_id") RequestBody room_id,
                                  @Part("student_ids") RequestBody student_ids,@Part MultipartBody.Part video);

    @Multipart
    @POST("/webapi/updateProfile")
    Call<ResponseBody> postFile(@Part MultipartBody.Part image, @Part("user_id") RequestBody user_id, @Part("first_name") RequestBody first_name, @Part("last_name") RequestBody last_name, @Part("email") RequestBody email);
}