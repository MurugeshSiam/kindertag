package com.kindertag.kindertag.module;

/**
 * Created by Nysa on 14-Jun-18.
 */

// used for food list
public class Item_Food {
    String item_id;
    String item_name;
    boolean item_isCheck = false;

    public String getItem_id() {
        return item_id;
    }

    public Item_Food(String item_id, String item_name, boolean item_isCheck) {
        this.item_id = item_id;
        this.item_name = item_name;
        this.item_isCheck = item_isCheck;
    }

    public String getItem_name() {
        return item_name;
    }

    public boolean isItem_isCheck() {
        return item_isCheck;
    }

    public void setItem_name(String item_name) {

        this.item_name = item_name;
    }

    public void setItem_isCheck(boolean item_isCheck) {
        this.item_isCheck = item_isCheck;
    }
}
