package com.kindertag.kindertag.module;

/**
 * Created by abc on 22/4/18.
 */

// used for student list
public class Item_Students
{
    String std_id;
    String std_name;
    String std_image;

    public String getStd_id() {
        return std_id;
    }

    public String getStd_name() {
        return std_name;
    }

    public String getStd_image() {
        return std_image;
    }

    public Item_Students(String std_id, String std_name, String std_image) {

        this.std_id = std_id;
        this.std_name = std_name;
        this.std_image = std_image;
    }
}
