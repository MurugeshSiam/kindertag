package com.kindertag.kindertag.Activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.location.LocationRequest;
import com.kindertag.kindertag.Adapter.HomeActivityListAdapter;
import com.kindertag.kindertag.Adapter.HomeAttendanceGridAdapter;
import com.kindertag.kindertag.Adapter.HomeAttendanceGridAdapterTeacher;
import com.kindertag.kindertag.Adapter.HomeEventListAdapter;
import com.kindertag.kindertag.Adapter.HomeLearningListAdapter;
import com.kindertag.kindertag.Adapter.HomeMessageListAdapter;
import com.kindertag.kindertag.Adapter.HomeRoomListAdapter;
import com.kindertag.kindertag.Adapter.HomeStudentListAdapter;
import com.kindertag.kindertag.Fragment.AddActivityFragment;
import com.kindertag.kindertag.Fragment.AddChildFragment;
import com.kindertag.kindertag.Fragment.AddEventFragment;
import com.kindertag.kindertag.Fragment.AddFoodFragment;
import com.kindertag.kindertag.Fragment.AddLearningFragment;
import com.kindertag.kindertag.Fragment.AddMultiMessageFragment;
import com.kindertag.kindertag.Fragment.AddNapFragment;
import com.kindertag.kindertag.Fragment.AddPottyFragment;
import com.kindertag.kindertag.Fragment.AddStudentFragment;
import com.kindertag.kindertag.Fragment.ManageRoomsFragment;
import com.kindertag.kindertag.Fragment.NavMenuHelpFragment;
import com.kindertag.kindertag.Fragment.NavMenuParentCornerFragment;
import com.kindertag.kindertag.Fragment.NavMenuSettingsFragment;
import com.kindertag.kindertag.Fragment.NavMenuToolsFragment;
import com.kindertag.kindertag.Fragment.NavMenuToolsRoomCheckFragment;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.FloatingActionsMenu;
import com.kindertag.kindertag.Utils.KinderActivity;
import com.kindertag.kindertag.Utils.RoundedImageView;
import com.kindertag.kindertag.Utils.SharePref;
import com.kindertag.kindertag.Utils.WebAPIService;
import com.loopj.android.http.RequestParams;
import com.ogaclejapan.arclayout.ArcLayout;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeActivity extends KinderActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ProgressBar progressBar;
    private final String LOG_TAG = "BNK";
     com.kindertag.kindertag.Utils.FloatingActionButton action_photos,action_kudos,action_notes,action_learning
             ,action_food,action_nap,action_potty,action_med;
    public static String user_type;
    public  String title_option="Select All";
    public static String sel_room = "";
    public static ArrayList<String> messages = new ArrayList<String>();
    public static long img_name = Calendar.getInstance().getTimeInMillis() + 1500;
    public static Uri imguri;
    public static long newimgpos = Calendar.getInstance().getTimeInMillis();
 public  String filePath;
    long totalSize = 0;
    Uri imageUri;
    private final String API_URL_BASE = "http://kindertag.underdev.in";
    private View stickyViewSpacer;
    private int mLastFirstVisibleItem;
    private boolean mIsScrollingUp;
    // BroadCast receiver for Get Notification
    // Here received notification set into Notification bar
    // When click on notification redirect to Home activity
    final BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Common.PUSH_NOTIFICATION)) {
                // new push notification is received
                String title = intent.getStringExtra("title");
                String message = intent.getStringExtra("message");
                String type = intent.getStringExtra("type");
                String notidata = intent.getStringExtra("notidata");

                Intent ii = new Intent(getApplicationContext(), HomeActivity.class);
                ii.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                ii.putExtra("tab", type);
                PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), ii, PendingIntent.FLAG_CANCEL_CURRENT);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {

                    Notification noti = new Notification.Builder(getApplicationContext())
                            .setContentTitle(title)
                            .setContentText(message)
                            .setContentIntent(pIntent)
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                            .setAutoCancel(true)
                            .build();

                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                    notificationManager.notify(Integer.parseInt(notidata), noti);
                }

            }
        }
    };

    // Attendance student and teachers arraylists
    public ArrayList<String> att_selected_std_id = new ArrayList<String>();
    public ArrayList<String> attendance_students = new ArrayList<String>();
    public ArrayList<String> attendance_teachers = new ArrayList<String>();

    public ArrayList<String> categories = new ArrayList<String>();

    // Add food arraylists.
    public ArrayList<String> foods = new ArrayList<String>();
    public ArrayList<String> bottols = new ArrayList<String>();

    public DialogInterface dialog = null;
    public FloatingActionsMenu menuMultipleActions;
    public String seprator = "!~!";
    public LinearLayout ll_update_room;
    public TextView txt_home_attendance_chk_absent, txt_home_attendance_chk_move, txt_attendance_chk_in, txt_attendance_chk_out;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    Spinner spin_activity_types;
    TextView empty;
    GridView grid_home;
    LinearLayout ll_tabs, ll_spin, ll_header, ll_attendance_header, ll_attendance_btns,overview_layout,header_layout;
    FrameLayout fl_arc;
    Toolbar toolbar;
    FloatingActionButton fab;
    int sel_tab_pos = 0, exp_pos = -1;
    String sel_tab = "Student";
    String sel_att_tab = "Students";
    ArcLayout arc;
    ImageButton img_scanner;
    ImageView img_header_student;
    RoundedImageView user_profile;
    TextView txt_header_student_name, txt_header_student_edit, txt_parent_pin, txt_attendance_save,txt_img,txt_img_p;
    ArrayList<String> rooms = new ArrayList<String>();
    ArrayList<String> students = new ArrayList<String>();
    ArrayList<String> teachers = new ArrayList<String>();
    ArrayList<String> events = new ArrayList<String>();
    ArrayList<String> activities = new ArrayList<String>();
    ArrayList<String> learnings = new ArrayList<String>();
    String sel_child = "";//sel_student="";
    TextView room;
    Menu menu1;
    SearchView searchView;
    String dial_num = "";
    DialogInterface d = null;
    boolean is1sttime = true;
    ArrayList<String> arr_oid = new ArrayList<String>();
    ArrayList<String> arr_name = new ArrayList<String>();
    ArrayList<String> arr_stdid = new ArrayList<String>();
    String lat = "", lang = "";
    ReactiveLocationProvider locationProvider;
    String check_status = "";
    Context context = HomeActivity.this;
String Profile_Url;
    // Api for Teacher Check in.
    String URl = "http://kindertag.underdev.in/webapi/teacherCheckin";
    String ProfilePhoto ="http://kindertag.underdev.in/webapi/updateProfile";
    // Api for Change password
    String CNG_PASS_URl = "http://kindertag.underdev.in/webapi/changePassword";
    Calendar calendar = Calendar.getInstance();

    public static ArrayList<String> getReceivers() {
        return messages;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // finding toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set Navigation drawer and open close it
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.thene_blue_dark));
        }


        // Runtime Permission of phone calls
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }


        // check the role of user and set selected room and selected child
        user_type = pref.getString(uid + "role", "");
        Profile_Url=pref.getString(uid + "image_url","");
        Log.i("user_type", user_type);
        if (!user_type.equalsIgnoreCase("Owner") && !pref.getString(uid + "room_id", "").equals(""))
            sel_room = pref.getString(uid + "room_id", "") + ";" + pref.getString(uid + "room_name", "");
        if (user_type.equalsIgnoreCase("Parent") && !pref.getString(uid + "sel_child", "").equals("")) {
            sel_child = pref.getString(uid + "sel_child", "");
        }

        ll_update_room = (LinearLayout) findViewById(R.id.ll_home_list_room_update);


        // set navigation view items regarding user role
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().findItem(R.id.nav_tools).setVisible(user_type.equalsIgnoreCase("Owner"));
        navigationView.getMenu().findItem(R.id.nav_admin_corner).setVisible(user_type.equalsIgnoreCase("Owner"));
        navigationView.getMenu().findItem(R.id.nav_parent_corner).setVisible(user_type.equalsIgnoreCase("Parent"));
        navigationView.getMenu().findItem(R.id.nav_switch_student).setVisible(user_type.equalsIgnoreCase("Parent"));
        navigationView.getMenu().findItem(R.id.nav_teacher_attendance).setVisible(user_type.equalsIgnoreCase("Teacher"));
        navigationView.getMenu().findItem(R.id.nav_qr_code).setVisible(!user_type.equalsIgnoreCase("Parent"));

        navigationView.setNavigationItemSelectedListener(this);
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_nav_menu_header_user_name)).setText(pref.getString(uid + "name", ""));
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_nav_menu_header_user_email)).setText(pref.getString(uid + "email", ""));
        user_profile = (RoundedImageView) navigationView.getHeaderView(0).findViewById(R.id.img_nav_menu_header_user_profile);
        txt_img = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_img);
        txt_img_p = (TextView) findViewById(R.id.txt_img_p);
if(user_type.equalsIgnoreCase("Parent")){
    if(sel_child.split(";")[3].length()==0){
        String TextH=sel_child.split(";")[1];
        String s=TextH.substring(0,1);
        txt_img.setVisibility(View.VISIBLE);

        txt_img.setText(s);
    }else {
        txt_img.setVisibility(View.GONE);
        Picasso.with(context).load(sel_child.split(";")[3]).into(user_profile);


        // new DownLoadImageTask(user_profile).execute(Profile_Url);

        //    user_profile.setImageURI(Uri.parse(Profile_Url));
    }
}else {
    if(Profile_Url.length()==0){
        String TextH=pref.getString(uid + "name", "");
        String s=TextH.substring(0,1);
        txt_img.setText(s);
    }else {
        txt_img.setVisibility(View.GONE);
        Picasso.with(context).load(Profile_Url).into(user_profile);


        // new DownLoadImageTask(user_profile).execute(Profile_Url);

        //    user_profile.setImageURI(Uri.parse(Profile_Url));
    }
}





        /*Profile Photo Upload ---SiamComputing*/

        user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user_type.equalsIgnoreCase("Parent")){

                }else
                {


                final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Photo", R.drawable.ic_gallery + ";" + "  Choose Image"};
                ListAdapter adapter = new ArrayAdapter<String>(HomeActivity.this, android.R.layout.select_dialog_item, android.R.id.text1, items) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        //Use super class to create the View
                        View v = super.getView(position, convertView, parent);
                        TextView tv = (TextView) v.findViewById(android.R.id.text1);
                        //Put the image on the TextView
                        tv.setText(items[position].split(";")[1]);
                        tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                        return v;
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle("Choose Action");
                builder.setCancelable(true);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        newimgpos++;
                        img_name++;

                        switch (i) {
                            case 0:
                                // When click on Camera check camera runtime permissions and open camera

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                } else {
                                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    File photo = new File(Environment.getExternalStorageDirectory(), "temp" + newimgpos + ".png");
                                    camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                    startActivityForResult(camera, 4);
                                }
                                break;

                            case 1:
                                // When click on Gallery check External Storage runtime permissions and open camera

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                        ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                } else {
                                    Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    gallery.setType("image/*");
                                    startActivityForResult(gallery, 8);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                });
                builder.show();

                /*Profile Photo Upload ---SiamComputing*/
                }
            }
        });
        img_scanner = (ImageButton) navigationView.getHeaderView(0).findViewById(R.id.img_scanner);
        txt_parent_pin = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_parent_pin);

        // Find id's of views declared in xml
        ll_header = (LinearLayout) findViewById(R.id.ll_home_header);
        overview_layout = (LinearLayout) findViewById(R.id.overview_layout);
        img_header_student = (ImageView) findViewById(R.id.img_home_header_student);
        txt_header_student_edit = (TextView) findViewById(R.id.txt_home_header_student_edit);
        txt_header_student_name = (TextView) findViewById(R.id.txt_home_header_student_name);
        ll_attendance_header = (LinearLayout) findViewById(R.id.ll_home_attendance_tabs);
        final TextView txt_attendance_students = (TextView) findViewById(R.id.txt_home_attendance_tab_students);
        final TextView txt_attendance_teachers = (TextView) findViewById(R.id.txt_home_attendance_tab_teachers);
        txt_attendance_save = (TextView) findViewById(R.id.txt_home_attendance_save);
        ll_attendance_btns = (LinearLayout) findViewById(R.id.ll_home_attendance_btns);
        header_layout = (LinearLayout) findViewById(R.id.header_layout);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listHeader = inflater.inflate(R.layout.list_header, null);

        stickyViewSpacer = listHeader.findViewById(R.id.stickyViewPlaceholder);

        txt_attendance_chk_in = (TextView) findViewById(R.id.txt_home_attendance_chk_in);
        txt_attendance_chk_out = (TextView) findViewById(R.id.txt_home_attendance_chk_out);
        txt_home_attendance_chk_absent = (TextView) findViewById(R.id.txt_home_attendance_chk_absent);
        txt_home_attendance_chk_move = (TextView) findViewById(R.id.txt_home_attendance_chk_move);

        ll_spin = (LinearLayout) findViewById(R.id.ll_home_activity_spinner);
        spin_activity_types = (Spinner) findViewById(R.id.spin_home_activities);
        empty = (TextView) findViewById(R.id.txt_home_empty);
        grid_home = (GridView) findViewById(R.id.grid_home);
        fab = (FloatingActionButton) findViewById(R.id.fab_home);
        fl_arc = (FrameLayout) findViewById(R.id.fl_home_activity_arc);
        arc = (ArcLayout) findViewById(R.id.arc_layout);
        TextView close = (TextView) findViewById(R.id.txt_home_activity_arc_close);
        ll_tabs = (LinearLayout) findViewById(R.id.ll_home_tabs);
        RequestParams paramss = new RequestParams();

        Common.callWebService(HomeActivity.this, Common.wb_getSettings, paramss, "GetSettings");


        if(user_type.equalsIgnoreCase("Owner")||user_type.equalsIgnoreCase("Teacher")) {
    paramss.add("user_id", uid);
    if (user_type.equalsIgnoreCase("Owner")) {
        paramss.add("role_id", "2");

    } else if (user_type.equalsIgnoreCase("Teacher")) {
        paramss.add("role_id", "3");

    }

    Common.callWebService(HomeActivity.this, Common.wb_activityType, paramss, "GetActivityType");
}
        // if user type is parent and click student open new window - Add student screen.
        // if user type is parent then you can update student details.
        View.OnClickListener student_click = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_type.equalsIgnoreCase("Parent")) {
                    setActionDrawer(false, "Update");
                    AddStudentFragment asf = new AddStudentFragment();
                    Bundle b = new Bundle();
                    b.putString("vals", sel_child);
                    asf.setArguments(b);
                    getSupportFragmentManager().beginTransaction().add(R.id.fl_container, asf, "UpdateChild").addToBackStack("UpdateChild").commit();
                }
            }
        };

        // set click listener on student image

        img_header_student.setOnClickListener(student_click);
        txt_header_student_edit.setOnClickListener(student_click);
        txt_header_student_name.setOnClickListener(student_click);

        //============================================================
        // when click on Attendance tab you can check in, check out, absent and move-
        // teachers and students

        // This is for set absent student and teachers

        txt_home_attendance_chk_absent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorDrawable cd = (ColorDrawable) txt_home_attendance_chk_absent.getBackground();
                int colorCode = cd.getColor();
                if (colorCode == Color.parseColor("#28A9E0")) {
                    Log.i("sel_att_tab", sel_att_tab);
                    if (sel_att_tab.equalsIgnoreCase("Students")) {
                        RequestParams params = new RequestParams();
                        params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                        params.add("student_ids", getIds1(att_selected_std_id));
                        Common.callWebService(HomeActivity.this, Common.wb_absent, params, "AttendanceStudents" + "%" + "In");
                    } else if (sel_att_tab.equalsIgnoreCase("Teachers")) {
                        RequestParams params = new RequestParams();
                        params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                        params.add("teacher_ids", getIds1(att_selected_std_id));
                        Common.callWebService(HomeActivity.this, Common.wb_absent_teacher, params, "AttendanceTeachers" + "%" + "In");
                    }
                }
            }
        });

        // This is for set move student and teachers

        txt_home_attendance_chk_move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorDrawable cd = (ColorDrawable) txt_home_attendance_chk_move.getBackground();
                int colorCode = cd.getColor();
                if (colorCode == Color.parseColor("#28A9E0")) {

                    Intent intent = new Intent(context, MoveChildActivity.class);
                    intent.putExtra("student_ids", getIds1(att_selected_std_id));
                    startActivity(intent);
                }
            }
        });

        // This is for set check-in student and teachers

        txt_attendance_chk_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable cd = (ColorDrawable) txt_attendance_chk_in.getBackground();
                int colorCode = cd.getColor();
                if (colorCode == Color.parseColor("#28A9E0")) {
                    //call attendance wb here
                    Log.i("sel_att_tab", sel_att_tab);
                    if (sel_att_tab.equalsIgnoreCase("Students")) {
                        RequestParams params = new RequestParams();
                        params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                        params.add("student_ids", getIds1(att_selected_std_id));
                        Common.callWebService(HomeActivity.this, Common.wb_attendance, params, "AttendanceStudents" + "%" + "In");
                    } else if (sel_att_tab.equalsIgnoreCase("Teachers")) {
                        RequestParams params = new RequestParams();
                        params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                        params.add("teacher_ids", getIds1(att_selected_std_id));
                        Common.callWebService(HomeActivity.this, Common.wb_checkin_teacher, params, "AttendanceTeachers" + "%" + "In");
                    }
                }
            }
        });

        // This is for set check-out student and teachers

        txt_attendance_chk_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorDrawable cd = (ColorDrawable) txt_attendance_chk_out.getBackground();
                int colorCode = cd.getColor();
                if (colorCode == Color.parseColor("#28A9E0")) {
                    //call attendance wb here
                    if (sel_att_tab.equalsIgnoreCase("Students")) {
                        RequestParams params = new RequestParams();
                        params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                        params.add("student_ids", getIds1(att_selected_std_id));
                        Common.callWebService(HomeActivity.this, Common.wb_checkout, params, "AttendanceStudents" + "%" + "Out");
                    } else if (sel_att_tab.equalsIgnoreCase("Teachers")) {
                        RequestParams params = new RequestParams();
                        params.add("teacher_ids", getIds1(att_selected_std_id));
                        Common.callWebService(HomeActivity.this, Common.wb_checkout_teacher, params, "AttendanceTeachers" + "%" + "Out");
                    }
                }
            }
        });



        //============================================================
        if (user_type.equalsIgnoreCase("Owner")) {

            // if user type is owner then we pass owner id and get all rooms related owner.
            // call webservice roomlist and pass uid as Parameter.

            RequestParams params = new RequestParams();
            params.add("user_id", uid);
            Common.callWebService(HomeActivity.this, Common.wb_roomlist, params, "Rooms");

            // when user click on attendance tab there are two options Student and Teacher.
            // this is click listener perform on student and teacher both and set data regarding role.
            View.OnClickListener att_tab_click = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txt_attendance_teachers.setBackgroundResource(view.getId() == txt_attendance_teachers.getId() ? R.drawable.tab_bg_sel : R.drawable.tab_bg);
                    txt_attendance_teachers.setTextColor(view.getId() == txt_attendance_teachers.getId() ? getResources().getColor(R.color.tab_txt_sel) : getResources().getColor(R.color.tab_txt));
                    txt_attendance_students.setBackgroundResource(view.getId() == txt_attendance_students.getId() ? R.drawable.tab_bg_sel : R.drawable.tab_bg);
                    txt_attendance_students.setTextColor(view.getId() == txt_attendance_students.getId() ? getResources().getColor(R.color.tab_txt_sel) : getResources().getColor(R.color.tab_txt));
                    sel_att_tab = view.getId() == txt_attendance_teachers.getId() ? "Teachers" : "Students";
                    if (menu1 != null && menu1.findItem(R.id.action_search) != null && menu1.findItem(R.id.action_search).getActionView() != null)
                        menu1.findItem(R.id.action_search).collapseActionView();

                    if (sel_att_tab.equalsIgnoreCase("Teachers")) {
                        //Move here
                        title_option = "Select All";
                        grid_home.setAdapter(new HomeAttendanceGridAdapterTeacher(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));
                    }
                        else if(sel_att_tab.equalsIgnoreCase("Students")){
                        title_option = "Select All";

                        grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));
                    }
                    if (sel_att_tab.equalsIgnoreCase("Teachers"))
                        // Move Here //
                        txt_home_attendance_chk_move.setVisibility(View.GONE);
                    else
                        txt_home_attendance_chk_move.setVisibility(View.VISIBLE);
                }
            };

            // click listener on student tab and teacher tab
            txt_attendance_students.setOnClickListener(att_tab_click);
            txt_attendance_teachers.setOnClickListener(att_tab_click);

        } else if (user_type.equalsIgnoreCase("Parent") && !sel_child.equals("")) {

            // when user type is parent then visible Parent pin(Check in code)
            // visible scanner button.
            txt_parent_pin.setVisibility(View.VISIBLE);
            img_scanner.setVisibility(View.VISIBLE);
            txt_parent_pin.setText("Check-In Code :- " + SharePref.getParentPin(context));

            img_scanner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(Gravity.LEFT);

                    // when click on scanner button open Scanner activity.

                    Intent intent1 = new Intent(context, ScannerActivity.class);
                    intent1.putExtra("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                    intent1.putExtra("user_id", uid);
                    startActivity(intent1);
                }
            });


            // Set student image to Circle Imageview.
if(!sel_child.split(";")[3].equals("")) {
    txt_img_p.setVisibility(View.GONE);

    Glide.with(HomeActivity.this).load(sel_child.split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_header_student) {
        @Override
        protected void setResource(Bitmap resource) {
            if (resource != null && img_header_student != null) {

                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                img_header_student.setImageDrawable(circularBitmapDrawable);
            }
        }
    });
}else {
    txt_img_p.setVisibility(View.VISIBLE);
    img_header_student.setVisibility(View.VISIBLE);

    String Name=sel_child.split(";")[1];
    String F=Name.substring(0,1);
      txt_img_p.setText(F);

}
            // set student name to text field

            txt_header_student_name.setText(sel_child.split(";")[1]);

        } else {
            if (sel_room != null) {
                RequestParams params = new RequestParams();
                params.add("room_id", sel_room.split(";")[0]);
                params.add("user_id", uid);
                params.add("user_type", user_type.toLowerCase());
                Common.callWebService(HomeActivity.this, Common.wb_getusers, params, "GetUsers");

                if (user_type.equalsIgnoreCase("Teacher")) {

                    // get all student here
                    // call studentlist webservice passing parameter room_id
                    rooms.add(sel_room.split(";")[0] + ";" + sel_room.split(";")[1]);
                    RequestParams params1 = new RequestParams();
                    params1.add("room_id", sel_room.split(";")[0]);
                    Common.callWebService(HomeActivity.this, Common.wb_studentlist, params1, "Students");
                }
            }
        }

        // click on edit student details.
        txt_header_student_edit.setTypeface(tf_material);
        txt_header_student_edit.setText(R.string.icon_photo);


        //Set Arc Elements
        String types[] = getResources().getStringArray(R.array.activity_types);
        for (int i = 0; i < arc.getChildCount(); i++) {
            ((TextView) ((LinearLayout) arc.getChildAt(i)).getChildAt(0)).setTypeface(tf_material);
            ((TextView) ((LinearLayout) arc.getChildAt(i)).getChildAt(0)).setText(getResources().getIdentifier("icon_" + types[i + 1].toLowerCase(), "string", getPackageName()) + "");
            ((TextView) ((LinearLayout) arc.getChildAt(i)).getChildAt(1)).setText(types[i + 1]);
            arc.getChildAt(i).setTag(types[i + 1]);
            arc.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.getTag().toString().equalsIgnoreCase("Photo")) {

                        // show dialog for photo
                        // option Take a photo and Choose Image
                        final String items[] = new String[]{R.drawable.ic_camera + ";" + "  Take a Photo", R.drawable.ic_gallery + ";" + "  Choose Image"};
                        ListAdapter adapter = new ArrayAdapter<String>(HomeActivity.this, android.R.layout.select_dialog_item, android.R.id.text1, items) {
                            public View getView(int position, View convertView, ViewGroup parent) {
                                //Use super class to create the View
                                View v = super.getView(position, convertView, parent);
                                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                                //Put the image on the TextView
                                tv.setText(items[position].split(";")[1]);
                                tv.setCompoundDrawablesWithIntrinsicBounds(Integer.parseInt(items[position].split(";")[0]), 0, 0, 0);
                                return v;
                            }
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                        builder.setTitle("Choose Action");
                        builder.setCancelable(true);
                        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                newimgpos++;
                                img_name++;

                                switch (i) {
                                    case 0:
                                        // When click on Camera check camera runtime permissions and open camera

                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                                        ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                        ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                                        } else {
                                            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                            File photo = new File(Environment.getExternalStorageDirectory(), "temp" + newimgpos + ".png");
                                            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                            startActivityForResult(camera, 3);
                                        }
                                        break;

                                    case 1:
                                        // When click on Gallery check External Storage runtime permissions and open camera

                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                                        ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                                        } else {
                                            Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                            gallery.setType("image/*");
                                            startActivityForResult(gallery, 7);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        });
                        builder.show();
                    } else {
                        toolbar.setVisibility(View.VISIBLE);
                        fl_arc.setVisibility(View.GONE);
                        menuMultipleActions.setVisibility(View.GONE);
                        ll_spin.setVisibility(View.VISIBLE);
                        spin_activity_types.setSelection(0);
                        fab.setVisibility(View.VISIBLE);


                        setActionDrawer(false, "Add " + view.getTag().toString());
                        Bundle b = new Bundle();
                        b.putString("type", view.getTag().toString());
                        if (view.getTag().toString().equalsIgnoreCase("Learning")) {
                            RequestParams params = new RequestParams();
                            params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                            Common.callWebService(HomeActivity.this, Common.wb_getlearningcategory, params, "GetCategories" + "%" + view.getTag().toString());
                            } else {
                            AddActivityFragment aaf = new AddActivityFragment();
                            aaf.setArguments(b);
                            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, aaf, "AddActivity").addToBackStack("AddActivity").commit();
                        }
                    }
                }
            });
        }

        close.setTypeface(tf_material);
        close.setText(R.string.icon_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        grid_home.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

                if (absListView.getId() == grid_home.getId()) {
                    final int currentFirstVisibleItem = grid_home.getFirstVisiblePosition();

                    if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                        mIsScrollingUp = false;
                      //  Toast.makeText(HomeActivity.this,"Scrolling Down",Toast.LENGTH_LONG).show();
                      //  TranslateAnimation animate = new TranslateAnimation(0,0,0,-header_layout.getHeight());
                      //  animate.setDuration(500);
                       // animate.setFillAfter(true);
                        //header_layout.startAnimation(animate);
                       header_layout.setVisibility(View.GONE);
                       // header_layout.setVisibility(View.GONE);
//                        TranslateAnimation animate = new TranslateAnimation(
//                                0,                 // fromXDelta
//                                0,                 // toXDelta
//                                0,                 // fromYDelta
//                                header_layout.getHeight()); // toYDelta
//                        animate.setDuration(500);
//                        animate.setFillAfter(true);
//                        header_layout.startAnimation(animate);

                     /*   header_layout.animate()
                                .translationY(header_layout.getHeight())
                                .alpha(0.0f)
                                .setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        header_layout.setVisibility(View.GONE);
                                    }
                                });*/

                    } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                        mIsScrollingUp = true;
                       // Toast.makeText(HomeActivity.this,"Scrolling Up",Toast.LENGTH_LONG).show();
                    //    TranslateAnimation animate = new TranslateAnimation(0,0,0,header_layout.getHeight());
                     //   animate.setDuration(500);
                      //  animate.setFillAfter(true);
                    //    header_layout.startAnimation(animate);
                        header_layout.setVisibility(View.VISIBLE);

//                        header_layout.animate()
//                                .translationY(header_layout.getHeight())
//                                .alpha(1.0f)
//                                .setDuration(300)
//                                .setListener(new AnimatorListenerAdapter() {
//                                    @Override
//                                    public void onAnimationEnd(Animator animation) {
//                                        super.onAnimationEnd(animation);
//                                        header_layout.setVisibility(View.VISIBLE);
//                                    }
//                                });

                    }

                    mLastFirstVisibleItem = currentFirstVisibleItem;
                }

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

              /*  if (grid_home.getFirstVisiblePosition() == 0) {
                    View firstChild = grid_home.getChildAt(0);
                    int topY = 0;
                    if (firstChild != null) {
                        topY = firstChild.getTop();
                    }
                    int heroTopY = stickyViewSpacer.getTop();
                 //   ll_spin.setY(Math.max(0, heroTopY + topY));
                    //int heroTopY = stickyViewSpacer.getTop();
                    //  stickyView.setY(Math.max(0, heroTopY + topY));


                    overview_layout.setY(topY * 0.5f);
                }*/
            }
        });

        //Set Adapters
        grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, "", grid_home, empty));
        spin_activity_types.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        //Set OnClick Listeners on + for all tabs
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (sel_tab.equalsIgnoreCase("Activities")) {
                        menuMultipleActions.setVisibility(View.VISIBLE);
                        fab.setVisibility(View.GONE);
                        hideActivity();
                    } else if (sel_tab.equalsIgnoreCase("Events")) {
                        setActionDrawer(false, "Create Event");
                        getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new AddEventFragment(), "AddEvent").addToBackStack("AddEvent").commit();

                    } else if (sel_tab.equalsIgnoreCase("Messages")) {
                         {
                            setActionDrawer(false, "Send Multi Message");
                            Intent intent = new Intent(HomeActivity.this, MultiMessageActivity.class);
                            startActivity(intent);
                        }

                    } else if (sel_tab.equalsIgnoreCase("Student")) {
                        if (rooms.size() == 0) {
                            Toast.makeText(context, "Please add Room First", Toast.LENGTH_SHORT).show();
                            setActionDrawer(false, "Rooms");
                            grid_home.setAdapter(new HomeRoomListAdapter(HomeActivity.this, rooms, "", grid_home, empty));
                            ll_tabs.setVisibility(View.GONE);
                            fab.setVisibility(View.GONE);

                            if (rooms.size() == 0) {
                                ll_update_room.setVisibility(View.VISIBLE);
                            }
                        }else {
                            setActionDrawer(false, "Add Student");
                            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new AddStudentFragment(), "AddStudent").addToBackStack("AddStudent").commit();
                        }
                    }
            }
        });

        setTabs();

        // set Click listener on tab showing horizontal in bottom of screen
        // when click on particular tab open that regarding screen
        grid_home.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long l) {
                switch (sel_tab) {
                    case "Student":

                        if (!getSupportActionBar().getTitle().toString().equalsIgnoreCase("Rooms") && ll_header.getVisibility() != View.VISIBLE) {
                            sel_child = grid_home.getAdapter().getItem(pos).toString();
                            dial_num = grid_home.getAdapter().getItem(pos).toString().split(";")[8];
                            getSupportActionBar().setTitle("");
                          // getSupportActionBar().setDisplayShowCustomEnabled(false);
                            invalidateOptionsMenu();
                            Glide.with(HomeActivity.this).load(grid_home.getAdapter().getItem(pos).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_header_student) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    if (resource != null && img_header_student != null) {
                                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        img_header_student.setImageDrawable(circularBitmapDrawable);
                                    }
                                }
                            });
                            txt_header_student_name.setText(grid_home.getAdapter().getItem(pos).toString().split(";")[1]);
                            ll_header.setVisibility(View.VISIBLE);
                            ll_spin.setVisibility(View.VISIBLE);
                            spin_activity_types.setSelection(0);
                            fab.setVisibility(View.GONE);
                            if (menu1 != null && menu1.findItem(R.id.action_search) != null && menu1.findItem(R.id.action_search).getActionView() != null)
                                menu1.findItem(R.id.action_search).collapseActionView();
                            //grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(),activities,!getSupportActionBar().getTitle().equals(""),grid_home,empty));
                            RequestParams params = new RequestParams();
                            params.add("student_id", grid_home.getAdapter().getItem(pos).toString().split(";")[0]);
                            Common.callWebService(HomeActivity.this, Common.wb_getactivity, params, "getActivity");
                        }
                        break;
                    case "Home":
                        //Call Student
                        break;
                    case "Messages":
                        navigationView.getMenu().getItem(0).setChecked(false);

                        //Call Messages Activity Details Details Activity
                        Intent intent = new Intent(HomeActivity.this, MessagesActivity.class);
                        intent.putExtra("pos", "");
                        intent.putExtra("vals", grid_home.getAdapter().getItem(pos).toString());
                        intent.putExtra("sid", sel_child);
                        startActivity(intent);
                        break;
                        // Open Event screen
                    case "Events":
                        if (!user_type.equalsIgnoreCase("Parent")) {
                            exp_pos = (exp_pos != pos) ? pos : -1;
                            grid_home.setAdapter(new HomeEventListAdapter(HomeActivity.this, events, user_type, exp_pos, (searchView != null ? searchView.getQuery().toString() : ""), grid_home, empty));
                        }
                        break;
                    case "Attendance":
                        // No Code Here
                        break;
                    case "Activities":
                        //Call Student Activity Details
                        break;
                    case "Learning":
                        //Call Learning
                        break;
                    default:
                        break;
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setHome();
            }
        }, 10);


        updateLocation();
        if(user_type.equalsIgnoreCase("Parent"))
        if (SharePref.getfirstlogin(HomeActivity.this).equalsIgnoreCase("0")) {
            changepassDialog();
        }

        // When click on Activities showing one + button
        // click on that + button open floating action Menu.

        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
         action_photos = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_a);
         action_photos.setVisibility(View.GONE);
         action_kudos = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_c);
        action_kudos.setVisibility(View.GONE);
         action_notes = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_d);
        action_notes.setVisibility(View.GONE);
         action_learning = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_e);
         action_learning.setVisibility(View.GONE);
         action_food = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_f);
         action_food.setVisibility(View.GONE);
         action_nap = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_g);
         action_nap.setVisibility(View.GONE);
         action_potty = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_h);
         action_potty.setVisibility(View.GONE);
         action_med = (com.kindertag.kindertag.Utils.FloatingActionButton) findViewById(R.id.action_m);
         action_med.setVisibility(View.GONE);


        // set Click listener on all submenu
        // open fragment regarding button

        action_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // showPhotoDialogforActivity();

                addActivity("Photo");

            }
        });
        action_kudos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity("Praise");
            }
        });
        action_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity("Note");
            }
        });
        action_med.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity("Med");
            }
        });
        action_learning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity("Learning");
            }
        });
        action_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity("Food");
            }
        });
        action_nap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity("Nap");
            }
        });
        action_potty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addActivity("Potty");
            }
        });

    }

    private void hideActivity() {
     //   ll_spin.setVisibility(View.GONE);
    }

    // when click on floating action submenu call this method
    // get tag which button called
    // call web regarding Activity type and get data of perticular activity
    public void addActivity(String tag) {
        menuMultipleActions.collapse();
        toolbar.setVisibility(View.VISIBLE);
        fl_arc.setVisibility(View.GONE);
        menuMultipleActions.setVisibility(View.GONE);
        ll_spin.setVisibility(View.VISIBLE);
        spin_activity_types.setSelection(0);
        fab.setVisibility(View.VISIBLE);
        setActionDrawer(false, "Add " + tag);
        Bundle b = new Bundle();
        b.putString("type", tag);
        if (tag.equalsIgnoreCase("Food")) {
            RequestParams params = new RequestParams();
            params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
            Common.callWebService(HomeActivity.this, Common.wb_getfooditems, params, "GetFooditem" + "%" + tag);
        } else if (tag.equalsIgnoreCase("Learning")) {
            RequestParams params = new RequestParams();
            params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
            Common.callWebService(HomeActivity.this, Common.wb_getlearningcategory, params, "GetCategories" + "%" + tag);
        } else if (tag.equalsIgnoreCase("Nap")) {
            AddNapFragment aaf = new AddNapFragment();
            aaf.setArguments(b);
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, aaf, "AddNap").addToBackStack("AddNap").commit();
        } else if (tag.equalsIgnoreCase("Potty")) {
            AddPottyFragment aaf = new AddPottyFragment();
            aaf.setArguments(b);
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, aaf, "AddPotty").addToBackStack("AddPotty").commit();
        } else {
            AddActivityFragment aaf = new AddActivityFragment();
            aaf.setArguments(b);
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, aaf, "AddActivity").addToBackStack("AddActivity").commit();
        }
    }

    // when click back <- on any activity screen then go back to activities screen.

    @Override
    public void onBackPressed() {

        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (menu1 != null && menu1.findItem(R.id.action_search) != null && menu1.findItem(R.id.action_search).getActionView() != null)
                menu1.findItem(R.id.action_search).collapseActionView();
            if (getSupportActionBar() != null && (getSupportActionBar().getTitle().toString().equalsIgnoreCase("Select Student") || getSupportActionBar().getTitle().toString().equalsIgnoreCase("All Students"))) {
                if (getSupportFragmentManager().findFragmentByTag("AddActivity") != null)
                    ((AddActivityFragment) getSupportFragmentManager().findFragmentByTag("AddActivity")).reset();
                else if (getSupportFragmentManager().findFragmentByTag("AddLearning") != null)
                    ((AddLearningFragment) getSupportFragmentManager().findFragmentByTag("AddLearning")).reset();
                else if (getSupportFragmentManager().findFragmentByTag("AddFood") != null)
                    ((AddFoodFragment) getSupportFragmentManager().findFragmentByTag("AddFood")).reset();
                else if (getSupportFragmentManager().findFragmentByTag("AddNap") != null)
                    ((AddNapFragment) getSupportFragmentManager().findFragmentByTag("AddNap")).reset();
                else if (getSupportFragmentManager().findFragmentByTag("AddPotty") != null)
                    ((AddPottyFragment) getSupportFragmentManager().findFragmentByTag("AddPotty")).reset();
                else if (getSupportFragmentManager().findFragmentByTag("ManageRooms") != null)
                    ((ManageRoomsFragment) getSupportFragmentManager().findFragmentByTag("ManageRooms")).reset();
            }

            else if (getSupportFragmentManager().getBackStackEntryCount() > 1)
                getSupportFragmentManager().popBackStack();
            else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                getSupportFragmentManager().popBackStack();
                Log.i("sel_tab", "" + sel_tab);
                if(user_type.equalsIgnoreCase("Teacher")){
                    setActionDrawer(true, sel_tab.equalsIgnoreCase("Student") ? sel_room.split(";")[1] : sel_tab);

                } else if(user_type.equalsIgnoreCase("Owner")){
                   // getSupportActionBar().setTitle("");
                    setHome();

                    getSupportFragmentManager().popBackStack();

                }

                else  if(user_type.equalsIgnoreCase("Parent")) {
                    setActionDrawer(true,"Home");


                }else
                    setActionDrawer(true, sel_tab.equalsIgnoreCase("Student") ? "" : sel_tab);

                if (sel_tab.equalsIgnoreCase("Activities")) {
                    fab.setVisibility(View.GONE);
                    menuMultipleActions.setVisibility(View.VISIBLE);
                }
            }
        } else if (fl_arc != null && fl_arc.getVisibility() == View.VISIBLE) {
            toolbar.setVisibility(View.VISIBLE);
            fl_arc.setVisibility(View.GONE);
            ll_spin.setVisibility(View.VISIBLE);
            spin_activity_types.setSelection(0);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else if ((getSupportActionBar() != null && !user_type.equalsIgnoreCase("Teacher") && !getSupportActionBar().getTitle().toString().equalsIgnoreCase(""))
                || (!user_type.equalsIgnoreCase("Parent") && (ll_header.getVisibility() == View.VISIBLE))
                || (user_type.equals("Teacher") && sel_room != null && !getSupportActionBar().getTitle().toString().equalsIgnoreCase(sel_room.split(";")[1]))) {
            setHome();
        } else {

            // Show alertdialog for exit from application
            // If click on exit then out from application

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Exit Application");
            builder.setMessage("Do you really want to Exit ?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    finish();
                }
            });
            builder.create().show();
        }
        navigationView.getMenu().getItem(0).setChecked(true);

    }

    // set up of Navigation Drawer
    // which open when click on top left icon on app
    public void setActionDrawer(boolean isDrawerEnable, String actionTitle) {
        if (menu1 != null && menu1.findItem(R.id.action_search) != null && menu1.findItem(R.id.action_search).getActionView() != null)
            menu1.findItem(R.id.action_search).collapseActionView();
        if (isDrawerEnable)
            getSupportActionBar().setDisplayHomeAsUpEnabled(!isDrawerEnable);
        drawer.setDrawerLockMode(isDrawerEnable ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.setDrawerIndicatorEnabled(isDrawerEnable);
        toggle.setToolbarNavigationClickListener(isDrawerEnable ? null : new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toggle.syncState();
        if (!isDrawerEnable)
            getSupportActionBar().setDisplayHomeAsUpEnabled(!isDrawerEnable);
      /*  if(user_type.length()==0){
            getSupportActionBar().setTitle("Home");

        }else*/
            getSupportActionBar().setTitle(actionTitle);

        getSupportActionBar().setDisplayShowCustomEnabled(user_type.equalsIgnoreCase("Owner") && actionTitle.equals("") && ll_header.getVisibility() != View.VISIBLE);
        invalidateOptionsMenu();
    }

    public String getActionTitle() {
        return getSupportActionBar().getTitle().toString();
    }
    public void setActionTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        //Toast.makeText(HomeActivity.this,"NavUp",Toast.LENGTH_SHORT);
        return true;
    }


    // when we set teacher and student attendance on Attendance tab-
    // and return back to home screen then we update student and teacher list with updated value
    public void getUpdatedAttendance() {

        if (sel_att_tab.equalsIgnoreCase("Teachers")) {
            if (sel_room != null) {
                RequestParams params1 = new RequestParams();
                params1.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                Common.callWebService(HomeActivity.this, Common.wb_getteacher, params1, "GetTeachers");
            }
        } else {
            if (sel_room != null) {
                RequestParams params = new RequestParams();
                params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                Common.callWebService(HomeActivity.this, Common.wb_studentlist, params, "Students");
            }
        }
    }


    // set Bottom tabs
    // and load data regarding tabs
    public void setTabs() {
        final ArrayList<String> tabs = new ArrayList<String>();
        tabs.add("Home");
        tabs.add("Messages");
        tabs.add("Events");
        if (!user_type.equalsIgnoreCase("Parent"))
            tabs.add("Attendance");
        tabs.add(user_type.equalsIgnoreCase("Parent") ? "Learning" : "Activities");
        ll_tabs.removeAllViews();
        for (int i = 0; i < tabs.size(); i++) {
            View root = getLayoutInflater().inflate(R.layout.view_home_tab, null);
            root.setTag(i);
            TextView tab_icon = (TextView) root.findViewById(R.id.txt_home_tab_icon);
            TextView tab_name = (TextView) root.findViewById(R.id.txt_home_tab_name);
            tab_icon.setTypeface(tf_material);
            tab_icon.setText(getResources().getIdentifier("icon_" + tabs.get(i).toLowerCase(), "string", getPackageName()));
            tab_name.setText(tabs.get(i));
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //if(sel_tab_pos!=(int)view.getTag()){
                    ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(0)).setTextColor(getResources().getColor(R.color.tab_txt));
                    ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(1)).setTextColor(getResources().getColor(R.color.tab_txt));
                    sel_tab_pos = (int) view.getTag();
                    sel_tab = tabs.get(sel_tab_pos);
                    ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(0)).setTextColor(getResources().getColor(R.color.tab_txt_sel));
                    ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(1)).setTextColor(getResources().getColor(R.color.tab_txt_sel));
                    if (menu1 != null && menu1.findItem(R.id.action_search) != null && menu1.findItem(R.id.action_search).getActionView() != null)
                        menu1.findItem(R.id.action_search).collapseActionView();
                    getSupportActionBar().setTitle(sel_tab);
                    //getSupportActionBar().setDisplayShowTitleEnabled(true);
                    getSupportActionBar().setDisplayShowCustomEnabled(false);
                    if (fl_arc.getVisibility() == View.VISIBLE)
                        onBackPressed();
                    ll_header.setVisibility(View.GONE);
                    menuMultipleActions.setVisibility(View.GONE);
                    fab.setVisibility(((user_type.matches("Owner|Teacher") && !sel_tab.matches("Attendance|Activity")) ||
                            //(user_type.equalsIgnoreCase("Teacher") && !sel_tab.equalsIgnoreCase("Attendance") && !sel_tab.equalsIgnoreCase("Events")) ||
                            (user_type.equalsIgnoreCase("Parent") && sel_tab.equalsIgnoreCase("Messages"))) ? View.VISIBLE : View.GONE);
                    grid_home.setNumColumns(sel_tab.equalsIgnoreCase("Attendance") ? 4 : 1);
                    //grid_home.setVisibility(sel_tab.equalsIgnoreCase("Attendance")?View.VISIBLE:View.GONE);
                    //list_home.setVisibility(!sel_tab.equalsIgnoreCase("Attendance")?View.VISIBLE:View.GONE);
                    ll_spin.setVisibility(sel_tab.equalsIgnoreCase("Activities") ? View.VISIBLE : View.GONE);
                    ll_attendance_header.setVisibility(sel_tab.equalsIgnoreCase("Attendance") && user_type.equalsIgnoreCase("Owner") ? View.VISIBLE : View.GONE);
                    //txt_attendance_save.setVisibility(sel_tab.equalsIgnoreCase("Attendance")?View.VISIBLE:View.GONE);
                    ll_attendance_btns.setVisibility(sel_tab.equalsIgnoreCase("Attendance") ? View.VISIBLE : View.GONE);
                    //arc.setVisibility(sel_tab_pos==3?View.VISIBLE:View.GONE);
                    invalidateOptionsMenu();
                    //Set Rest of Code here
                    switch (sel_tab) {
                        case "Home":

                            setHome();
                            break;
                        case "Messages":
                            navigationView.getMenu().getItem(0).setChecked(false);

                            grid_home.setAdapter(new HomeMessageListAdapter(HomeActivity.this, messages, "", grid_home, empty));
                            RequestParams params = new RequestParams();

                            if (SharePref.getParent(context).equalsIgnoreCase("Parent"))
                                params.add("room_id", SharePref.getRoomid(context));
                            else
                                params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));

                            params.add("user_type", user_type.toLowerCase());
                            params.add("user_id", uid);
                            if (user_type.equalsIgnoreCase("Parent")) {
                                params.add("student_id", uid);
                            }

                            Log.e("spe", params.toString());
                            Common.callWebService(HomeActivity.this, Common.wb_getusers, params, "GetUsers");
                            break;
                        case "Events":
                            navigationView.getMenu().getItem(0).setChecked(false);

                            exp_pos = -1;
                            grid_home.setAdapter(new HomeEventListAdapter(HomeActivity.this, events, user_type, exp_pos, "", grid_home, empty));
                            //if(user_type.equalsIgnoreCase("Parent") || (user_type.equalsIgnoreCase("Owner") && sel_room!=null)){
                            RequestParams params1 = new RequestParams();
                            params1.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                            params1.add("user_id", uid);
                            Common.callWebService(HomeActivity.this, Common.wb_eventlist, params1, "Events");
                            //}
                            break;
                        case "Attendance":
                            navigationView.getMenu().getItem(0).setChecked(false);

                            for (int i = 0; i < students.size(); i++) {
                                if (Boolean.parseBoolean(students.get(i).split(";")[4]) && !attendance_students.contains(students.get(i).split(";")[1]))
                                    attendance_students.add(students.get(i).split(";")[1]);
                            }
                            for (int i = 0; i < teachers.size(); i++) {
                                if (Boolean.parseBoolean(teachers.get(i).split(";")[4]) && !attendance_teachers.contains(teachers.get(i).split(";")[0]))
                                    attendance_teachers.add(teachers.get(i).split(";")[0]);
                            }

                            if (sel_att_tab.equalsIgnoreCase("Teachers"))
                                grid_home.setAdapter(new HomeAttendanceGridAdapterTeacher(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));
                            else
                                grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));

                            break;
                        case "Activities":
                            navigationView.getMenu().getItem(0).setChecked(false);

                            RequestParams params2 = new RequestParams();
                            params2.add("room_id", sel_room.split(";")[0]);
                            Common.callWebService(HomeActivity.this, Common.wb_getroomactivity, params2, "Activities");
                            grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                            fab.performClick();
                            break;
                        case "Learning":
                            navigationView.getMenu().getItem(0).setChecked(false);

                            RequestParams params3 = new RequestParams();
                            params3.add("room_id", sel_room.split(";")[0]);
                            params3.add("student_id", sel_child.split(";")[0]);
                            Common.callWebService(HomeActivity.this, Common.wb_getlearningactivity, params3, "Learnings");
                            grid_home.setAdapter(new HomeLearningListAdapter(HomeActivity.this, learnings, grid_home, empty));
                            break;
                        default:
                            break;
                    }
                }
            });
            ll_tabs.addView(root, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));
        }
    }

    public void resetTabs() {
        ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(0)).setTextColor(getResources().getColor(R.color.tab_txt));
        ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(1)).setTextColor(getResources().getColor(R.color.tab_txt));
    }

    // get current selected user type
    public String getUserType() {
        return user_type;
    }

    // get all room list calling this method
    public ArrayList<String> getRooms() {
        return rooms;
    }

    // get all students of selected room using this method
    public ArrayList<String> getStudents() {
        ArrayList<String> stus = new ArrayList<String>();
        for (int i = 0; i < students.size(); i++)
            if (Boolean.parseBoolean(students.get(i).split(";")[4]))
                stus.add(students.get(i));
        Collections.sort(students, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].toLowerCase().compareTo(t1.split(";")[1].toLowerCase());
            }
        });
        return stus;//students;
    }

    // get Students for activities.
    public ArrayList<String> getStudentsforActivities() {
        ArrayList<String> stus = new ArrayList<String>();
        for (int i = 0; i < students.size(); i++) {
            stus.add(students.get(i));
        }

        Collections.sort(students, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].toLowerCase().compareTo(t1.split(";")[1].toLowerCase());
            }
        });
        return stus;//students;
    }

    // Get categories of learning activity
    public ArrayList<String> getCategories() {
        Collections.sort(categories, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].toLowerCase().compareTo(t1.split(";")[1].toLowerCase());
            }
        });
        return categories;
    }

    // get food list for food activity
    public ArrayList<String> getFoods() {
        Collections.sort(foods, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].toLowerCase().compareTo(t1.split(";")[1].toLowerCase());
            }
        });
        return foods;
    }

    // get bottols for bottol activity
    public ArrayList<String> getBottols() {
        Collections.sort(bottols, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].toLowerCase().compareTo(t1.split(";")[1].toLowerCase());
            }
        });
        return bottols;
    }

    // all activities and fragment action menu set here
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        menu.findItem(R.id.action_search).setVisible((getSupportActionBar().getTitle().toString().matches("^(Rooms|Messages|Events|Attendance|Select Student|All Students)$")) || (user_type.equalsIgnoreCase("Owner") && getSupportActionBar().getTitle().equals("") && ll_header.getVisibility() != View.VISIBLE && sel_tab.equalsIgnoreCase("Student")));
        menu.findItem(R.id.action_update).setVisible(getSupportActionBar().getTitle().toString().matches("(Select Student)"));
        //menu.findItem(R.id.action_update).setTitle(getSupportActionBar().getTitle().toString().equalsIgnoreCase("Select Student")?"Done":"Update");
        menu.findItem(R.id.action_select_all).setVisible(getSupportActionBar().getTitle().toString().matches("(Attendance|Select Student)"));//|Send Multi Message
        menu.findItem(R.id.action_select_check_in_option).setVisible(getSupportActionBar().getTitle().toString().matches("(Attendance|Select Student)"));//|Send Multi Message
        menu.findItem(R.id.action_add).setVisible(user_type.equalsIgnoreCase("Parent") && getSupportActionBar().getTitle().toString().equalsIgnoreCase("My Children"));
        menu.findItem(R.id.action_call).setVisible(!user_type.equalsIgnoreCase("Parent") && getSupportActionBar().getTitle().toString().equalsIgnoreCase("") && ll_header.getVisibility() == View.VISIBLE);
        menu.findItem(R.id.action_edit_student).setVisible(user_type.equalsIgnoreCase("Owner") && getSupportActionBar().getTitle().toString().equalsIgnoreCase("") && ll_header.getVisibility() == View.VISIBLE);
        menu.findItem(R.id.action_delete_student).setVisible(user_type.equalsIgnoreCase("Owner") && getSupportActionBar().getTitle().toString().equalsIgnoreCase("") && ll_header.getVisibility() == View.VISIBLE);
        menu.findItem(R.id.action_view_student).setVisible(user_type.equalsIgnoreCase("Teacher") && getSupportActionBar().getTitle().toString().equalsIgnoreCase("") && ll_header.getVisibility() == View.VISIBLE);
        menu.findItem(R.id.action_manage_rooms).setVisible(user_type.equalsIgnoreCase("Owner") && getSupportActionBar().getTitle().toString().equalsIgnoreCase("Rooms"));

        menu.findItem(R.id.action_call).setVisible(user_type.equalsIgnoreCase("Parent"));
        menu1 = menu;
        return super.onCreateOptionsMenu(menu);
    }

    // click listener on Option menu.

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                drawer.closeDrawer(GravityCompat.START);

                SearchManager searchManager = (SearchManager) HomeActivity.this.getSystemService(Context.SEARCH_SERVICE);
                searchView = (SearchView) item.getActionView();

                searchView.setSearchableInfo(searchManager.getSearchableInfo(HomeActivity.this.getComponentName()));
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            if (getSupportFragmentManager().findFragmentByTag("AddActivity") != null)
                                ((AddActivityFragment) getSupportFragmentManager().findFragmentByTag("AddActivity")).refresh_students(newText);
                            else if (getSupportFragmentManager().findFragmentByTag("AddLearning") != null)
                                ((AddLearningFragment) getSupportFragmentManager().findFragmentByTag("AddLearning")).refresh_students(newText);
                            else if (getSupportFragmentManager().findFragmentByTag("ManageRooms") != null)
                                ((ManageRoomsFragment) getSupportFragmentManager().findFragmentByTag("ManageRooms")).refresh_students(newText);
                        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            switch (sel_tab) {
                                case "Student":
                                    if (user_type.equalsIgnoreCase("Owner")) {
                                        if (getSupportActionBar().getTitle().toString().equalsIgnoreCase("Rooms"))
                                            grid_home.setAdapter(new HomeRoomListAdapter(HomeActivity.this, rooms, newText, grid_home, empty));
                                        else if (getSupportActionBar().getTitle().toString().equalsIgnoreCase(""))
                                            grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, newText, grid_home, empty));
                                    }
                                    break;
                                case "Home":
                                    break;
                                case "Messages":
                                    grid_home.setAdapter(new HomeMessageListAdapter(HomeActivity.this, messages, newText, grid_home, empty));
                                    break;
                                case "Events":
                                    grid_home.setAdapter(new HomeEventListAdapter(HomeActivity.this, events, user_type, exp_pos, newText, grid_home, empty));
                                    break;
                                case "Attendance":

                                    if (sel_att_tab.equalsIgnoreCase("Teachers"))
                                        grid_home.setAdapter(new HomeAttendanceGridAdapterTeacher(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, newText, grid_home, empty, true));
                                    else
                                        grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, newText, grid_home, empty, true));
                                    break;
                                case "Activities":
                                    break;
                                case "Learning":
                                    //No Code Here
                                    break;
                                default:
                                    break;
                            }
                        }
                        return true;
                    }
                });
                return true;
            case R.id.action_update:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportActionBar().getTitle().toString().equalsIgnoreCase("Select Student")) {
                    Log.i("sel_frag", getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount() - 1).getClass().getName().toString());
                    if (getSupportFragmentManager().findFragmentByTag("AddActivity") != null)
                        ((AddActivityFragment) getSupportFragmentManager().findFragmentByTag("AddActivity")).set();
                    else if (getSupportFragmentManager().findFragmentByTag("AddLearning") != null)
                        ((AddLearningFragment) getSupportFragmentManager().findFragmentByTag("AddLearning")).set();
                    else if (getSupportFragmentManager().findFragmentByTag("AddFood") != null)
                        ((AddFoodFragment) getSupportFragmentManager().findFragmentByTag("AddFood")).set();
                    else if (getSupportFragmentManager().findFragmentByTag("AddNap") != null)
                        ((AddNapFragment) getSupportFragmentManager().findFragmentByTag("AddNap")).set();
                    else if (getSupportFragmentManager().findFragmentByTag("AddPotty") != null)
                        ((AddPottyFragment) getSupportFragmentManager().findFragmentByTag("AddPotty")).set();
                }
                return true;

                // select all student of gridview
            case R.id.action_select_all:
                if (menu1 != null && menu1.findItem(R.id.action_search) != null && menu1.findItem(R.id.action_search).getActionView() != null)
                    menu1.findItem(R.id.action_search).collapseActionView();
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    if (getSupportFragmentManager().findFragmentByTag("AddActivity") != null)
                        ((AddActivityFragment) getSupportFragmentManager().findFragmentByTag("AddActivity")).setSelectionAll(item.getTitle().toString().equalsIgnoreCase("Select All"));
                    else if (getSupportFragmentManager().findFragmentByTag("AddLearning") != null)
                        ((AddLearningFragment) getSupportFragmentManager().findFragmentByTag("AddLearning")).setSelectionAll(item.getTitle().toString().equalsIgnoreCase("Select All"));
                    else if (getSupportFragmentManager().findFragmentByTag("AddFood") != null)
                        ((AddFoodFragment) getSupportFragmentManager().findFragmentByTag("AddFood")).setSelectionAll(item.getTitle().toString().equalsIgnoreCase("Select All"));
                    else if (getSupportFragmentManager().findFragmentByTag("AddNap") != null)
                        ((AddNapFragment) getSupportFragmentManager().findFragmentByTag("AddNap")).setSelectionAll(item.getTitle().toString().equalsIgnoreCase("Select All"));
                    else if (getSupportFragmentManager().findFragmentByTag("AddPotty") != null)
                        ((AddPottyFragment) getSupportFragmentManager().findFragmentByTag("AddPotty")).setSelectionAll(item.getTitle().toString().equalsIgnoreCase("Select All"));
                    else if (getSupportFragmentManager().findFragmentByTag("AddMultiMessage") != null)
                        ((AddMultiMessageFragment) getSupportFragmentManager().findFragmentByTag("AddMultiMessage")).setSelectionAll(item.getTitle().toString().equalsIgnoreCase("Select All"));
                } else if (sel_att_tab.equalsIgnoreCase("Students")) {

                    if (item.getTitle().toString().equalsIgnoreCase("Select All")) {
                        attendance_students.clear();
                        for (int i = 0; i < students.size(); i++)
                            attendance_students.add(students.get(i));
                    } else
                        attendance_students.clear();
                    if (sel_tab.equalsIgnoreCase("Attendance"))

                     //   grid_home.setAdapter(new HomeNewActivtyStudentsGridAdapterNew(HomeActivity.this, students, attendance_students, ""));

                    grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));
                } else if (sel_att_tab.equalsIgnoreCase("Teachers")) {
                    title_option="Select All";

                    if (item.getTitle().toString().equalsIgnoreCase("Select All")) {
                        attendance_teachers.clear();
                        for (int i = 0; i < teachers.size(); i++)
                            attendance_teachers.add(teachers.get(i).split(";")[0]);
                    } else
                        attendance_teachers.clear();
                    if (sel_tab.equalsIgnoreCase("Attendance"))
                        //grid_home.setAdapter(new HomeNewActivtyStudentsGridAdapterNew(HomeActivity.this, students, attendance_students, ""));

                     grid_home.setAdapter(new HomeAttendanceGridAdapterTeacher(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));
                }
                title_option="Select All";
                item.setTitle(item.getTitle().toString().equalsIgnoreCase(title_option) ? "Select None" : title_option);
                return true;
            case R.id.action_select_check_in_option:
                if (menu1 != null && menu1.findItem(R.id.action_select_check_in_option) != null && menu1.findItem(R.id.action_select_check_in_option).getActionView() != null)
                    menu1.findItem(R.id.action_select_check_in_option).collapseActionView();
                if (getSupportFragmentManager().findFragmentByTag("AddActivity") != null)
                    ((AddActivityFragment) getSupportFragmentManager().findFragmentByTag("AddActivity")).selectCheckIn(item.getTitle().toString().equalsIgnoreCase("Select Check-In"));
                else if (getSupportFragmentManager().findFragmentByTag("AddLearning") != null)
                    ((AddLearningFragment) getSupportFragmentManager().findFragmentByTag("AddLearning")).selectCheckIn(item.getTitle().toString().equalsIgnoreCase("Select Check-In"));
                else if (getSupportFragmentManager().findFragmentByTag("AddFood") != null)
                    ((AddFoodFragment) getSupportFragmentManager().findFragmentByTag("AddFood")).selectCheckIn(item.getTitle().toString().equalsIgnoreCase("Select Check-In"));
                else if (getSupportFragmentManager().findFragmentByTag("AddNap") != null)
                    ((AddNapFragment) getSupportFragmentManager().findFragmentByTag("AddNap")).selectCheckIn(item.getTitle().toString().equalsIgnoreCase("Select Check-In"));
                else if (getSupportFragmentManager().findFragmentByTag("AddPotty") != null)
                    ((AddPottyFragment) getSupportFragmentManager().findFragmentByTag("AddPotty")).selectCheckIn(item.getTitle().toString().equalsIgnoreCase("Select Check-In"));


                if(item.getTitle().toString().equalsIgnoreCase("Select Check-In")){
                    if (sel_att_tab.equalsIgnoreCase("Students")) {

                        if (item.getTitle().toString().equalsIgnoreCase("Select Check-In")) {
                            attendance_students.clear();
                            for (int i = 0; i < students.size(); i++)
                                attendance_students.add(students.get(i));
                        } else
                            attendance_students.clear();
                        if (sel_tab.equalsIgnoreCase("Attendance"))

                            //   grid_home.setAdapter(new HomeNewActivtyStudentsGridAdapterNew(HomeActivity.this, students, attendance_students, ""));

                            grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, false));
                    }else if (sel_att_tab.equalsIgnoreCase("Teachers")) {
                        if (item.getTitle().toString().equalsIgnoreCase("Select Check-In")) {

                            attendance_students.clear();
                            for (int i = 0; i < students.size(); i++)
                                attendance_students.add(students.get(i));
                        } else
                            attendance_students.clear();
                        if (sel_tab.equalsIgnoreCase("Attendance"))

                            //   grid_home.setAdapter(new HomeNewActivtyStudentsGridAdapterNew(HomeActivity.this, students, attendance_students, ""));

                            grid_home.setAdapter(new HomeAttendanceGridAdapterTeacher(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, false));
                    }
                }
                return true;
                // Add child
            case R.id.action_add:
                getSupportActionBar().setTitle("Add Child");
                invalidateOptionsMenu();
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new AddChildFragment(), "AddChild").addToBackStack("AddChild").commit();
                return true;
                // call to number
            case R.id.action_call:
                if (dial_num.length() > 0) {
                    Log.i("call", dial_num);
                    Intent call = new Intent(Intent.ACTION_DIAL);
                    call.setData(Uri.parse("tel:" + dial_num));
                    startActivity(call);
                }

                if (user_type.equalsIgnoreCase("Parent")) {
                    showProfileParent();
                    getSupportActionBar().setTitle("Home");

                }
                return true;

                // action edit studen
            case R.id.action_edit_student:
                setActionDrawer(false, "Edit Student Profile");
                AddStudentFragment asf = new AddStudentFragment();
                Bundle b = new Bundle();
                b.putString("vals", sel_child);
                asf.setArguments(b);
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, asf, "EditStudent").addToBackStack("EditStudent").commit();
                return true;

                // action view student
            case R.id.action_view_student:
                setActionDrawer(false, "View Student Profile");
                AddStudentFragment asf1 = new AddStudentFragment();
                Bundle b1 = new Bundle();
                b1.putString("vals", sel_child);
                asf1.setArguments(b1);
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, asf1, "ViewStudent").addToBackStack("ViewStudent").commit();
                return true;

                // action delete student
            case R.id.action_delete_student:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false);
                builder.setTitle("Delete Student Profile");
                builder.setMessage("Are you sure you want to Delete this Student Profile?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        d = dialogInterface;
                        Log.i("student_id", sel_child.split(";")[0]);
                        RequestParams params = new RequestParams();
                        params.add("student_id", sel_child.split(";")[0]);
                        Common.callWebService(HomeActivity.this, Common.wb_deletestudent, params, "DeleteStudent");
                    }
                });
                d = null;
                builder.create().show();
                return true;
                // action manage rooms.
            case R.id.action_manage_rooms:
                setActionDrawer(false, "Manage Rooms");
                ManageRoomsFragment mrf = new ManageRoomsFragment();
                Bundle b2 = new Bundle();
                b2.putStringArrayList("rooms", rooms);
                mrf.setArguments(b2);
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, mrf, "ManageRooms").addToBackStack("ManageRooms").commit();
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // set and select one room.
    public void setRoom() {

        room = new TextView(this);
        room.setBackgroundDrawable(new Spinner(this).getBackground());
        room.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        room.setTextColor(Color.WHITE);
        room.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        room.setGravity(Gravity.CENTER_VERTICAL);
        if (sel_room != null && sel_room.contains(";"))
            room.setText(sel_room.split(";")[1]);
        room.setSingleLine(true);
        room.setMaxLines(1);
        room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setActionDrawer(false, "Rooms");
                grid_home.setAdapter(new HomeRoomListAdapter(HomeActivity.this, rooms, "", grid_home, empty));
                ll_tabs.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);

                if (rooms.size() == 0) {
                    ll_update_room.setVisibility(View.VISIBLE);
                }
            }
        });
        getSupportActionBar().setCustomView(room, new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER));


        //===========================================================================================

        final EditText edit_room = (EditText) findViewById(R.id.edit_home_list_room_name);
        final TextView save = (TextView) findViewById(R.id.txt_home_list_room_save);
        final TextView clos = (TextView) findViewById(R.id.txt_home_list_room_close);

        ll_update_room.setVisibility(View.GONE);

        // when enter room name then click on save button
        // call addroom webservice passing parameter user_id, roomName, roomAddress
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.validate(edit_room, 1, true)) {
                    RequestParams params = new RequestParams();
                    params.add("user_id", uid);
                    params.add("roomName", edit_room.getText().toString());
                    params.add("roomAddress", "");
                    Common.callWebService((KinderActivity) context, Common.wb_addroom, params, "AddRoom" + "%" + edit_room.getText().toString());
                } else {
                    Toast.makeText(context, "Enter room name", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    //===========================================================================================
    // set home header and tab if user type is owner
    public void setHome() {
        try {


        navigationView.getMenu().getItem(0).setChecked(true);

        if (user_type.equalsIgnoreCase("Owner")) {
            if (room == null)
                setRoom();
            if (room != null && sel_room.contains(";"))
                room.setText(sel_room.split(";")[1]);
            ll_spin.setVisibility(View.GONE);
            ll_header.setVisibility(View.GONE);
            ll_attendance_header.setVisibility(View.GONE);
            //txt_attendance_save.setVisibility(View.GONE);
            ll_attendance_btns.setVisibility(View.GONE);
           /* if (user_type.equalsIgnoreCase("Parent")) {
                showProfileParent();
                getSupportActionBar().setTitle("Home");

            }*/
            setActionDrawer(true, "");
            resetTabs();
            sel_tab_pos = 0;
            sel_tab = "Student";
            ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(0)).setTextColor(getResources().getColor(R.color.tab_txt_sel));
            ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(1)).setTextColor(getResources().getColor(R.color.tab_txt_sel));

            if (sel_room != null) {
                RequestParams params = new RequestParams();
                params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                Common.callWebService(HomeActivity.this, Common.wb_studentlist, params, "Students");
            }
            if (sel_room != null) {
                RequestParams params1 = new RequestParams();
                params1.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                Common.callWebService(HomeActivity.this, Common.wb_getteacher, params1, "GetTeachers");
            }


            grid_home.setNumColumns(1);
            grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, "", grid_home, empty));
            ll_tabs.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        } else if (user_type.equalsIgnoreCase("Parent")) {
            // set home header and tab if user type is Parent
            setActionDrawer(true,"Home");
       //     getSupportActionBar().setTitle("Home");
            invalidateOptionsMenu();
            ll_header.setVisibility(View.VISIBLE);
            ll_spin.setVisibility(View.VISIBLE);
            spin_activity_types.setSelection(0);
            //list_home.setVisibility(VISIBLE);ll_header.setVisibility(VISIBLE);
            //grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(),activities,!getSupportActionBar().getTitle().equals(""),grid_home,empty));
            grid_home.setNumColumns(1);
            if (sel_child != null) {
                RequestParams params = new RequestParams();
                params.add("student_id", sel_child.split(";")[0]);
                Common.callWebService(HomeActivity.this, Common.wb_getactivity, params, "getActivity");
            } else
                grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
            fab.setVisibility(View.GONE);
            resetTabs();
            sel_tab_pos = 0;
            sel_tab = "Student";
            ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(0)).setTextColor(getResources().getColor(R.color.tab_txt_sel));
            ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(1)).setTextColor(getResources().getColor(R.color.tab_txt_sel));
        } else if (user_type.equalsIgnoreCase("Teacher")) {
            // set home header and tab if user type is Teacher

            ll_spin.setVisibility(View.GONE);
            ll_header.setVisibility(View.GONE);
            ll_attendance_header.setVisibility(View.GONE);
            ll_attendance_btns.setVisibility(View.GONE);
            setActionDrawer(true, sel_room.split(";")[1]);
            resetTabs();
            sel_tab_pos = 0;
            sel_tab = "Student";
            ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(0)).setTextColor(getResources().getColor(R.color.tab_txt_sel));
            ((TextView) ((LinearLayout) ll_tabs.getChildAt(sel_tab_pos)).getChildAt(1)).setTextColor(getResources().getColor(R.color.tab_txt_sel));

            if (sel_room != null) {
                RequestParams params = new RequestParams();
                params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                Common.callWebService(HomeActivity.this, Common.wb_studentlist, params, "Students");
            }
            grid_home.setNumColumns(1);
            grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, "", grid_home, empty));
            ll_tabs.setVisibility(View.VISIBLE);
            fab.setVisibility(View.GONE);
        }

        if (getIntent().hasExtra("tab") && is1sttime) {
            is1sttime = false;
            String tt = getIntent().getStringExtra("tab");
            int sel_tt = tt.equalsIgnoreCase("message") ? 1 : tt.equalsIgnoreCase("event") ? 2 : tt.equalsIgnoreCase("activity") && !user_type.equalsIgnoreCase("Parent") ? 4 : 0;
            if (ll_tabs != null)
                ll_tabs.getChildAt(sel_tt).performClick();
        }
        }catch (Exception e){
            Toast.makeText(this,"Something went wrong",Toast.LENGTH_LONG).show();
        }
    }

    // when click photo and get image from gallery then call onactivity result
    // here is it...
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 3) {

            File photo = new File(Environment.getExternalStorageDirectory(), "temp" + newimgpos + ".png");
            Uri imageUri = Uri.fromFile(photo);
            toolbar.setVisibility(View.VISIBLE);
            fl_arc.setVisibility(View.GONE);
            ll_spin.setVisibility(View.VISIBLE);
            spin_activity_types.setSelection(0);
            fab.setVisibility(View.VISIBLE);
            setActionDrawer(false, "Add Photo");

            imguri = imageUri;
            Bundle b = new Bundle();
            b.putString("type", "Photo");
            b.putString("img", imageUri.toString());
            AddActivityFragment aaf = new AddActivityFragment();
            aaf.setArguments(b);
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, aaf, "AddActivity").addToBackStack("AddActivity").commit();
        } else if (resultCode == Activity.RESULT_OK && requestCode == 7 && data != null) {
            Uri imageUri = data.getData();
            toolbar.setVisibility(View.VISIBLE);
            fl_arc.setVisibility(View.GONE);
            ll_spin.setVisibility(View.VISIBLE);
            spin_activity_types.setSelection(0);
            fab.setVisibility(View.VISIBLE);
            setActionDrawer(false, "Add Photo");

            imguri = imageUri;
            Bundle b = new Bundle();
            b.putString("type", "Photo");
            b.putString("img", imageUri.toString());
            AddActivityFragment aaf = new AddActivityFragment();
            aaf.setArguments(b);
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, aaf, "AddActivity").addToBackStack("AddActivity").commit();
        }
       else if (resultCode == Activity.RESULT_OK && requestCode == 4) {
            File photo = new File(Environment.getExternalStorageDirectory(), "temp" + newimgpos + ".png");
             imageUri = Uri.fromFile(photo);
            filePath=imageUri.toString();
        //    performCrop(imageUri);
         //   filePath = getPath(imageUri);
          //  new UploadFileToServer().execute();

            PhotoUpload(imageUri);
            imguri = imageUri;

          //  user_profile.setImageURI(imageUri);
                }

                else if (resultCode == Activity.RESULT_OK && requestCode == 8 && data != null) {
             imageUri = data.getData();
            /*String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(imageUri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();*/
           // user_profile.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            uploadFile(imageUri);


        }else if(resultCode == Activity.RESULT_OK && requestCode == 24 && data != null){
            uploadFile(imageUri);

        }
    }
//    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
//        @Override
//        protected void onPreExecute() {
//            // setting progress bar to zero
//            progressBar.setProgress(0);
//            super.onPreExecute();
//        }
//
//        @Override
//        protected void onProgressUpdate(Integer... progress) {
//            // Making progress bar visible
//            progressBar.setVisibility(View.VISIBLE);
//
//            // updating progress bar value
//            progressBar.setProgress(progress[0]);
//
//            // updating percentage value
//         //   txtPercentage.setText(String.valueOf(progress[0]) + "%");
//        }
//
//        @Override
//        protected String doInBackground(Void... params) {
//            return uploadFile();
//        }
//
//        @SuppressWarnings("deprecation")
//        private String uploadFile() {
//            String responseString = null;
//
//            HttpClient httpclient = new DefaultHttpClient();
//           HttpPost httppost = new HttpPost(ProfilePhoto);
//
//            try {
//                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
//                        new AndroidMultiPartEntity.ProgressListener() {
//
//                            @Override
//                            public void transferred(long num) {
//                                publishProgress((int) ((num / (float) totalSize) * 100));
//                            }
//                        });
//
//                File sourceFile = new File(filePath);
//
//                // Adding file data to http body
//                entity.addPart("image", new FileBody(sourceFile));
//
//                // Extra parameters if you want to pass to server
//                entity.addPart("user_id", new StringBody("2"));
//                entity.addPart("first_name", new StringBody("Nileshs"));
//                entity.addPart("last_name", new StringBody("Poman"));
//                entity.addPart("email", new StringBody("nileshpoman.969@gmail.com"));
//
//                totalSize = entity.getContentLength();
//                //httppost.setEntity(entity);
//
//                // Making server call
//                HttpResponse response = httpclient.execute(httppost);
//                HttpEntity r_entity = response.getEntity();
//
//                int statusCode = response.getStatusLine().getStatusCode();
//                if (statusCode == 200) {
//                    // Server response
//                    responseString = EntityUtils.toString(r_entity);
//                } else {
//                    responseString = "Error occurred! Http Status Code: "
//                            + statusCode;
//                }
//
//            } catch (ClientProtocolException e) {
//                responseString = e.toString();
//            } catch (IOException e) {
//                responseString = e.toString();
//            }
//
//            return responseString;
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//           // Log.e(, "Response from server: " + result);
//
//            // showing the server response in an alert dialog
//            showAlert(result);
//
//            super.onPostExecute(result);
//        }
//
//    }
private void performCrop(Uri uri){
    try {

        //call the standard crop action intent (the user device may not support it)
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        //indicate image type and Uri
        cropIntent.setDataAndType(uri, "image/*");
        //set crop properties
        cropIntent.putExtra("crop", "true");
        //indicate aspect of desired crop
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        //indicate output X and Y
        cropIntent.putExtra("outputX", 640);
        cropIntent.putExtra("outputY", 640);
        //retrieve data on return
        cropIntent.putExtra("return-data", true);
        //start the activity - we handle returning in onActivityResult
        startActivityForResult(cropIntent, 24);
    }
    catch(ActivityNotFoundException anfe){
        //display an error message
        String errorMessage = "Whoops - your device doesn't support the crop action!";
        Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
        toast.show();
    }
}
    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Response from Servers")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void PhotoUpload(Uri out) {

              }
        /*SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST,ProfilePhoto ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject jObj = new JSONObject(response);
                            String message = jObj.getString("message");

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            // JSON error
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        smr.addFile("user_id", "2");
        smr.addFile("first_name", "Nileshs");
        smr.addFile("last_name", "Poman");
        smr.addFile("email", "nileshpoman.969@gmail.com");
        smr.addFile("image", filePath);
        MyApplication.getInstance().addToRequestQueue(smr);*/

   // }
    private String getPath(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }
    // we check rutime permissions for camera and gallery
    // when permission allow or deny we reach here
    // if allow then camera and gallery action call from here
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photo = new File(Environment.getExternalStorageDirectory(), "temp" + newimgpos + ".png");
            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            startActivityForResult(camera, 3);
        } else if (requestCode == 3 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(HomeActivity.this, "Camera Permission Not Granted!", Toast.LENGTH_SHORT).show();
        if (requestCode == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photo = new File(Environment.getExternalStorageDirectory(), "temp" + newimgpos + ".png");
            camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
            startActivityForResult(camera, 4);
        } else if (requestCode == 4 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(HomeActivity.this, "Camera Permission Not Granted!", Toast.LENGTH_SHORT).show();
        if (requestCode == 7 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, 7);
        } else if (requestCode == 7 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(HomeActivity.this, "Gallery Permission Not Granted!", Toast.LENGTH_SHORT).show();
        if (requestCode == 8 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, 8);
        } else if (requestCode == 8 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(HomeActivity.this, "Gallery Permission Not Granted!", Toast.LENGTH_SHORT).show();
    }

    public String toString(ArrayList<String> list) {
        String str = "";
        for (int i = 0; i < list.size(); i++)
            str += seprator + list.get(i);
        return str.length() > 0 ? str.substring(seprator.length()).replaceAll(";", "#") : str;
    }

    public ArrayList<String> toList(String str) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < str.split(seprator).length; i++)
            list.add(str.split(seprator)[i]);
        return list;
    }

    public String getIds(ArrayList<String> sel_students) {
        String str = "";
        for (int i = 0; i < sel_students.size(); i++)
            str += "," + sel_students.get(i).split(";")[0];
        return str.length() > 0 ? str.substring(1) : str;
    }

    public String getIds1(ArrayList<String> sel_students) {
        String str = "";
        for (int i = 0; i < sel_students.size(); i++)
            str += "," + sel_students.get(i);
        return str.length() > 0 ? str.substring(1) : str;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        System.out.println(item);

        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_home:
                navigationView.getMenu().getItem(0).setChecked(true);


                setHome();
                break;
            case R.id.nav_tools:
                RequestParams params = new RequestParams();
                params.add("user_id", uid);
                Common.callWebService(HomeActivity.this, Common.wb_getadmindata, params, "Tools");
                //Common.callWebService(HomeActivity.this,Common.wb_getratio,params,"Ratio");
                break;
            case R.id.nav_admin_corner:
                setActionDrawer(false, "Admin Corner");
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new NavMenuParentCornerFragment(), "AdminCorner").addToBackStack("AdminCorner").commit();
                break;
            case R.id.nav_parent_corner:
                setActionDrawer(false, "Parent Corner");
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new NavMenuParentCornerFragment(), "ParentCorner").addToBackStack("ParentCorner").commit();
                break;
            case R.id.nav_switch_student:
                navigationView.getMenu().getItem(0).setChecked(false);
                Intent intent=new Intent(HomeActivity.this,SwitchChildActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_settings:
                navigationView.getMenu().getItem(0).setChecked(false);

                setActionDrawer(false, "Settings");
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new NavMenuSettingsFragment(), "Settings").addToBackStack("Settings").commit();
                break;
            case R.id.nav_help:
                navigationView.getMenu().getItem(0).setChecked(false);

                setActionDrawer(false, "Help");
                String hh[] = {"FAQs", "Report your issue", "About", "Video tutorials"};
                NavMenuHelpFragment nmhf = new NavMenuHelpFragment();
                Bundle b = new Bundle();
                b.putStringArray("helps", hh);
                nmhf.setArguments(b);
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, nmhf, "Help").addToBackStack("Help").commit();
                break;
            case R.id.nav_share:
                navigationView.getMenu().getItem(0).setChecked(false);

                Intent intents = new Intent(Intent.ACTION_SEND);
                intents.setType("text/plain");
                intents.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@kidertag.in"});
                startActivity(intents);
                break;
            case R.id.nav_signout:
                navigationView.getMenu().getItem(0).setChecked(false);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false);
                builder.setTitle("Sign Out");
                builder.setMessage("Do you really want to Sign Out ?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton("Sign Out", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        pref.edit().putString("user_id", "").commit();
                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("isSignout", true);
                        startActivity(intent);
                        finish();
                    }
                });
                builder.create().show();
                break;

            case R.id.nav_teacher_attendance:
                showdialog();
                break;

            case R.id.nav_qr_code:
                navigationView.getMenu().getItem(0).setChecked(false);

                intent = new Intent(context, QRCodeActivity.class);
                intent.putExtra("roomid", Common.chkNull(sel_room.split(";")[0], "0"));
                startActivity(intent);
                break;

            case R.id.nav_parent_profile:
                navigationView.getMenu().getItem(0).setChecked(false);

                showProfileParent();
                break;

            default:
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    // we call webservices from different different places
    // when call webservice and response reach here onSuccess.
    @Override
    public void onSuccess(String response, String tag) {
        super.onSuccess(response, tag);
        // Get all rooms respose
      /*  JSONObject jobjs = null;
        try {
            jobjs = new JSONObject(response);
            if(jobjs.getString("status").equalsIgnoreCase("2") ){

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
*/

        if (response != null && response.length() > 0 && tag.equalsIgnoreCase("Rooms")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    rooms.clear();
                    for (int i = 0; i < jarray.length(); i++)
                        rooms.add(Common.chkNull(jarray.getJSONObject(i), "room_id", "") + ";" + Common.chkNull(jarray.getJSONObject(i), "room_name", ""));

                    if (rooms.size() > 0)
                        sel_room = rooms.get(0).toString();

                    if (room != null && sel_room.length() > 0)
                        room.setText(sel_room.split(";")[1]);
                    if (sel_room != null) {// && pref.getString(uid+sel_room.split(";")[0]+"students","").equals("")){
                        RequestParams params = new RequestParams();
                        params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                        Common.callWebService(HomeActivity.this, Common.wb_studentlist, params, "Students");

                        RequestParams params1 = new RequestParams();
                        params1.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                        Common.callWebService(HomeActivity.this, Common.wb_getteacher, params1, "GetTeachers");
                    }
                    //else if(sel_room!=null && pref.getString(uid+sel_room.split(";")[0]+"students","").equals(""))
                    //students=Common.toList(pref.getString(uid+sel_room.split(";")[0]+"students",""));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Get all student response from perticular room
        } else if(response !=null && response.length()>0 && tag.equalsIgnoreCase("GetSettings")){
            Log.i("response", response);
            ArrayList<String> activity_list = new ArrayList<String>() ;
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONObject data = new JSONObject(jobj.getString("data"));
            String terms=data.getString("terms_conditions");
            String policy=data.getString("privacy_policy");
                    SharePref.setTerms(HomeActivity.this,terms);
                    SharePref.setPolicy(HomeActivity.this,policy);


                }
                } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        else if(response != null && response.length() > 0 && tag.equalsIgnoreCase("GetActivityType")){
            Log.i("response", response);
            try {
                ArrayList<String> activity_list = new ArrayList<String>() ;
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    for (int i = 0; i < jarray.length(); i++) {
                  activity_list.add(Common.chkNull(jarray.getJSONObject(i),"activity_type_id",""));
                    }
                    for (int i = 0; i < activity_list.size(); i++) {
                        if(activity_list.get(i).equalsIgnoreCase("1")){
                            action_photos.setVisibility(View.VISIBLE);

                        }else  if(activity_list.get(i).equalsIgnoreCase("2")){
                            action_kudos.setVisibility(View.VISIBLE);

                        }else  if(activity_list.get(i).equalsIgnoreCase("3")){
                            action_notes.setVisibility(View.VISIBLE);

                        }
                        else  if(activity_list.get(i).equalsIgnoreCase("4")){
                            action_learning.setVisibility(View.VISIBLE);

                        }
                        else  if(activity_list.get(i).equalsIgnoreCase("5")){
                            action_food.setVisibility(View.VISIBLE);

                        }
                        else  if(activity_list.get(i).equalsIgnoreCase("6")){
                            action_nap.setVisibility(View.VISIBLE);

                        }
                        else  if(activity_list.get(i).equalsIgnoreCase("7")){
                            action_potty.setVisibility(View.VISIBLE);

                        }
                        else  if(activity_list.get(i).equalsIgnoreCase("8")){
                            action_med.setVisibility(View.VISIBLE);

                        }
                    }

                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("Students")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    students.clear();
                    for (int i = 0; i < jarray.length(); i++) {
                        //String emergency_contact=Common.chkNull(new JSONObject(Common.chkNull(jarray.getJSONObject(i),"emergency_contact","")),"phone","");
                        students.add(Common.chkNull(jarray.getJSONObject(i), "student_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "studentName", "") + ";" +//Common.chkNull(jarray.getJSONObject(i),"first_name","")+" "+Common.chkNull(jarray.getJSONObject(i),"last_name","")+";"+
                                Common.chkNull(jarray.getJSONObject(i), "birthdate", "0000-00-00") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "photo", "") + ";" +
                                (jarray.getJSONObject(i).getInt("check_in") == 1 && jarray.getJSONObject(i).getInt("check_out") == 0) + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parent_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "fatherName", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "fatherEmail", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "fatherPhone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_email", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "gurdian_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "gurdian_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "emergency_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "emergency_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "allergy", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "medication", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "doctorName", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "doctorPhone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "address", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "address2", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "city", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "state", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "country", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "zip", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "notes", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "student_code", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "status", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "absent", "0"));

                        if (jarray.getJSONObject(i).getInt("check_in") == 1 && jarray.getJSONObject(i).getInt("check_out") == 0 && !attendance_students.contains(Common.chkNull(jarray.getJSONObject(i), "student_id", "")))
                            attendance_students.add(Common.chkNull(jarray.getJSONObject(i), "student_id", ""));
                    }

                    if (getSupportActionBar().getTitle().equals("") || (user_type.equalsIgnoreCase("Teacher") && sel_tab.equalsIgnoreCase("Student")))
                        grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, "", grid_home, empty));
                    if (sel_tab.equalsIgnoreCase("Attendance"))
                        grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));

                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Add student response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddStudent")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    students.add(tag.split("%")[1].replace("id", Common.chkNull(jobj, "student_id", "0")).replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (getSupportActionBar().getTitle().equals("") && ll_header.getVisibility() == View.GONE && Common.chkNull(sel_room.split(";")[0], "0").equals(tag.split("%")[2]))
                        grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, "", grid_home, empty));
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();

                    RequestParams params1 = new RequestParams();
                    params1.add("room_id", sel_room.split(";")[0]);
                    Common.callWebService(HomeActivity.this, Common.wb_studentlist, params1, "Students");

                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Edit student response

        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("EditStudent")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if(jobj.getString("status").equalsIgnoreCase("2")){
                    if(user_type.equalsIgnoreCase("Owner")){
                        sel_child = (tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                        Glide.with(HomeActivity.this).load(sel_child.split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_header_student) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                if (resource != null && img_header_student != null) {
                                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    img_header_student.setImageDrawable(circularBitmapDrawable);
                                }
                            }
                        });
                        txt_header_student_name.setText(sel_child.split(";")[1]);
                        pref.edit().putString(uid + "sel_child", sel_child).commit();
                        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                            onBackPressed();
                        //grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this,students,grid_home,empty));
                        //pref.edit().putString(uid+sel_room.split(";")[0]+"students",Common.toString(students)).commit();
                        if (!pref.getString(uid + "students", "").equals(""))
                            students = Common.toList(pref.getString(uid + "students", ""));
                        Toast.makeText(HomeActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                    }else {
                        Intent intent=new Intent(HomeActivity.this,profileotpactivity.class);
                        intent.putExtra("phone_no", jobj.getString("new_phone_no"));
                        intent.putExtra("user_id", jobj.getInt("user_id"));

                        startActivity(intent);
                    }

                }
                else if (jobj.getInt("status") > 0) {
                    sel_child = (tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    Glide.with(HomeActivity.this).load(sel_child.split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_header_student) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if (resource != null && img_header_student != null) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                img_header_student.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });
                    txt_header_student_name.setText(sel_child.split(";")[1]);
                    pref.edit().putString(uid + "sel_child", sel_child).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    //grid_home.setAdapter(new HomeAttendanceGridAdapter(HomeActivity.this,students,grid_home,empty));
                    //pref.edit().putString(uid+sel_room.split(";")[0]+"students",Common.toString(students)).commit();
                    if (!pref.getString(uid + "students", "").equals(""))
                        students = Common.toList(pref.getString(uid + "students", ""));
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Delete student response

        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("DeleteStudent")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    if (d != null) {
                        d.dismiss();
                        d = null;
                    }
                    onBackPressed();
                    if (students != null && !sel_child.equals("") && getSupportActionBar().getTitle().equals("") && ll_header.getVisibility() == View.GONE) {
                        students.remove(sel_child);
                        sel_child = "";
                        grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, "", grid_home, empty));
                    }
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Add room response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddRoom")) {
            //Log.i("tag",tag);
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    ll_update_room.setVisibility(View.GONE);
                    rooms.add(Common.chkNull(jobj, "room_id", "0") + ";" + tag.split("%")[1]);
                    grid_home.setAdapter(new HomeRoomListAdapter(HomeActivity.this, rooms, "", grid_home, empty));

                    if (rooms.size() > 0) {
                        if (rooms.size() > 0)
                            sel_room = rooms.get(0).toString();
                        room.setText(rooms.get(0).split(";")[1]);
                    }

                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Edit room response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("EditRoom")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    int pos = Integer.parseInt(tag.split("%")[2]);
                    Log.i("pos", "" + pos);
                    Log.i("pos", "" + pos);
                    rooms.remove(pos);
                    rooms.add(pos, tag.split("%")[1]);
                    grid_home.setAdapter(new HomeRoomListAdapter(HomeActivity.this, rooms, "", grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Delete room response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("DeleteRoom")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    if (dialog != null)
                        dialog.dismiss();
                    int pos = Integer.parseInt(tag.split("%")[1]);
                    rooms.remove(pos);
                    grid_home.setAdapter(new HomeRoomListAdapter(HomeActivity.this, rooms, (searchView != null) ? searchView.getQuery().toString() : "", grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Getusers response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("GetUsers")) {
            Log.i("GetUsers", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    Log.i("len", "" + jarray.length());
                    messages.clear();
                    arr_oid.clear();
                    arr_name.clear();
                    arr_stdid.clear();
                    for (int i = 0; i < jarray.length(); i++) {
                        messages.add(Common.chkNull(jarray.getJSONObject(i), "user_id", "") + ";" +
                                //Common.chkNull(jarray.getJSONObject(i),"name","")+";"+
                                (!Common.chkNull(jarray.getJSONObject(i), "student_name", "").equals("") ? Common.chkNull(jarray.getJSONObject(i), "student_name", "") : Common.chkNull(jarray.getJSONObject(i), "name", "")) + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "email", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "phone", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "photo", "null") + ";" +
                                (Common.chkNull(jarray.getJSONObject(i), "role_id", "").equals("4") ? "Parent" :
                                        Common.chkNull(jarray.getJSONObject(i), "role_id", "").equals("3") ? "Teacher" :
                                                Common.chkNull(jarray.getJSONObject(i), "role_id", "").equals("2") ? "Owner" : "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "msg_id", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "last_message", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "message_date", "0000-00-00 00:00:00") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "unread_count", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "student_id", ""));

                        JSONObject jsonObject = jarray.getJSONObject(i);
                        arr_oid.add(jsonObject.getString("user_id"));

                        if (!jsonObject.getString("student_name").equalsIgnoreCase("")) {
                            arr_name.add(jsonObject.getString("student_name"));
                            arr_stdid.add(jsonObject.getString("student_id"));
                        } else {
                            arr_name.add(jsonObject.getString("name"));
                            arr_stdid.add("");
                        }

                    }
                    if (sel_tab.equalsIgnoreCase("Messages")) {
                        grid_home.setAdapter(new HomeMessageListAdapter(HomeActivity.this, messages, "", grid_home, empty));

                        if (!SharePref.getReceiver(HomeActivity.this).equalsIgnoreCase("")) {

                            for (int i = 0; i < arr_oid.size(); i++) {
                                Log.e("oid receid", arr_oid.get(i) + " " + SharePref.getReceiver(HomeActivity.this));
                                Log.e("len", arr_oid.size() + "");

                                if (SharePref.getStudent(context).equalsIgnoreCase("0")) {
                                    if (arr_oid.get(i).equalsIgnoreCase(SharePref.getReceiver(HomeActivity.this))) {

                                        Intent intent = new Intent(HomeActivity.this, MessagesActivity.class);
                                        intent.putExtra("pos", SharePref.getReceiver(HomeActivity.this));
                                        intent.putExtra("vals", arr_name.get(i));

                                        if (SharePref.getStudent(context).equalsIgnoreCase("0"))
                                            intent.putExtra("sid", "0");
                                        else
                                            intent.putExtra("sid", arr_stdid.get(i));

                                        startActivity(intent);
                                        SharePref.setRecever(this, "", "", "", "");
                                        break;

                                    }
                                } else if (arr_stdid.get(i).equalsIgnoreCase(SharePref.getStudent(context))) {

                                    Intent intent = new Intent(HomeActivity.this, MessagesActivity.class);
                                    intent.putExtra("pos", SharePref.getReceiver(HomeActivity.this));
                                    intent.putExtra("vals", arr_name.get(i));
                                    intent.putExtra("sid", arr_stdid.get(i));
                                    startActivity(intent);

                                    SharePref.setRecever(this, "", "", "", "");
                                    break;
                                }
                            }
                        }
                    }
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Get teachers response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("GetTeachers")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    Log.i("len", "" + jarray.length());
                    teachers.clear();
                    for (int i = 0; i < jarray.length(); i++)
                        teachers.add(Common.chkNull(jarray.getJSONObject(i), "user_id", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "name", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "email", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "phone", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "photo", "null") + ";" +
                                (Common.chkNull(jarray.getJSONObject(i), "role_id", "").equals("4") ? "Parent" :
                                        Common.chkNull(jarray.getJSONObject(i), "role_id", "").equals("3") ? "Teacher" : Common.chkNull(jarray.getJSONObject(i), "role_id", "").equals("2") ? "Owner" : "") + ";" +
                                (jarray.getJSONObject(i).getInt("check_in") == 1 && jarray.getJSONObject(i).getInt("check_out") == 0) + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "status", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "absent", "0"));

                    if (sel_tab.equalsIgnoreCase("Attendance"))
                        grid_home.setAdapter(new HomeAttendanceGridAdapterTeacher(HomeActivity.this, sel_att_tab.equalsIgnoreCase("Teachers") ? teachers : students, sel_att_tab, sel_att_tab.equalsIgnoreCase("Teachers") ? attendance_teachers : attendance_students, "", grid_home, empty, true));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Get events response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("Events")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    events.clear();
                    for (int i = 0; i < jarray.length(); i++)
                        events.add(Common.chkNull(jarray.getJSONObject(i), "event_id", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "event_name", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "event_desc", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "event_date", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "accept_user", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "decline_user", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "event_status", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "status", ""));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (sel_tab.equalsIgnoreCase("Events"))
                        grid_home.setAdapter(new HomeEventListAdapter(HomeActivity.this, events, user_type, exp_pos, "", grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // add event response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddEvent")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    events.add(Common.chkNull(jobj, "event_id", "") + ";" + tag.split("%")[1] + ";" + Common.chkNull(jobj, "status", ""));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (sel_tab.equalsIgnoreCase("Events"))
                        grid_home.setAdapter(new HomeEventListAdapter(HomeActivity.this, events, user_type, exp_pos, "", grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Update event response

        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("UpdateEvent")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    RequestParams params = new RequestParams();
                    params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                    params.add("user_id", uid);
                    Common.callWebService(HomeActivity.this, Common.wb_eventlist, params, "Events");
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Attendance student response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AttendanceStudents")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < students.size(); i++)
                        if (attendance_students.contains(students.get(i).split(";")[0])) {
                            String s = students.get(i).split(";")[0] + ";" + students.get(i).split(";")[1] + ";" + students.get(i).split(";")[2] + ";" + students.get(i).split(";")[3] + ";" +
                                    tag.split("%")[1].equalsIgnoreCase("in") + ";" + students.get(i).split(";")[5] + ";" + students.get(i).split(";")[6] + ";" + students.get(i).split(";")[7] + ";" +
                                    students.get(i).split(";")[8] + ";" + students.get(i).split(";")[9] + ";" + students.get(i).split(";")[10] + ";" + students.get(i).split(";")[11] + ";" +
                                    students.get(i).split(";")[12] + ";" + students.get(i).split(";")[13] + ";" + students.get(i).split(";")[14] + ";" + students.get(i).split(";")[15] + ";" +
                                    students.get(i).split(";")[16] + ";" + students.get(i).split(";")[17] + ";" + students.get(i).split(";")[18] + ";" + students.get(i).split(";")[19] + ";" +
                                    students.get(i).split(";")[20] + ";" + students.get(i).split(";")[21] + ";" + students.get(i).split(";")[22] + ";" + students.get(i).split(";")[23] + ";" +
                                    students.get(i).split(";")[24] + ";" + students.get(i).split(";")[25] + ";" + students.get(i).split(";")[26] + ";" + students.get(i).split(";")[27] + ";" +
                                    students.get(i).split(";")[28];
                            students.remove(i);
                            students.add(i, s);
                            if (attendance_students.contains(s.split(";")[0]) && tag.split("%")[1].equalsIgnoreCase("out"))
                                attendance_students.remove(s.split(";")[0]);
                        }

                    getUpdatedAttendance();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // attendance teacher response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AttendanceTeachers")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < teachers.size(); i++)
                        if (attendance_teachers.contains(teachers.get(i).split(";")[0])) {
                            String s = teachers.get(i).split(";")[0] + ";" + teachers.get(i).split(";")[1] + ";" + teachers.get(i).split(";")[2] + ";" + teachers.get(i).split(";")[3] + ";" +
                                    teachers.get(i).split(";")[4] + ";" + teachers.get(i).split(";")[5] + ";" + tag.split("%")[1].equalsIgnoreCase("in") + ";" + teachers.get(i).split(";")[7];
                            teachers.remove(i);
                            teachers.add(i, s);
                            attendance_teachers.remove(s.split(";")[0]);
                        }

                    getUpdatedAttendance();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Get all activity response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("getActivity")) {
            Log.i("response", "" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    activities.clear();
                    for (int i = 0; i < jarray.length(); i++) {
                        ArrayList<String> stus = new ArrayList<String>();
                        stus.add(Common.chkNull(jarray.getJSONObject(i), "student_id", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "first_name", "") + " " + Common.chkNull(jarray.getJSONObject(i), "last_name", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "birthdate", "0000-00-00") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "photo", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "status", "0"));
                        activities.add(Common.chkNull(jarray.getJSONObject(i), "activityId", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "created", "0000-00-00 00:00:00") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "activity_key", "null") + ";" + toString(stus) + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "activity_text", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "activity_photo", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "is_staff", "0").equals("0"));
                    }

                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (sel_tab.equalsIgnoreCase("Student"))
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // get all activities
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("Activities")) {
            Log.i("response", "" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    activities.clear();
                    for (int i = 0; i < jarray.length(); i++) {
                        final String date = Common.chkNull(jarray.getJSONObject(i), "activity_date", "0000-00-00");
                        //Log.i("date",date);
                        JSONArray json_acts = new JSONArray(Common.chkNull(jarray.getJSONObject(i), "activity", ""));
                        for (int j = 0; j < json_acts.length(); j++) {
                            JSONArray json_stus = new JSONArray(Common.chkNull(json_acts.getJSONObject(j), "student", ""));
                            ArrayList<String> stus = new ArrayList<String>();
                            for (int k = 0; k < json_stus.length(); k++)
                                stus.add(Common.chkNull(json_stus.getJSONObject(k), "student_id", "") + ";" +
                                        Common.chkNull(json_stus.getJSONObject(k), "first_name", "") + " " + Common.chkNull(json_stus.getJSONObject(k), "last_name", "") + ";" +
                                        Common.chkNull(json_stus.getJSONObject(k), "birthdate", "0000-00-00") + ";" +
                                        Common.chkNull(json_stus.getJSONObject(k), "photo", "") + ";" +
                                        Common.chkNull(json_stus.getJSONObject(k), "status", "0"));
                            activities.add(Common.chkNull(json_acts.getJSONObject(j), "activityId", "0") + ";" + date + ";" +
                                    Common.chkNull(json_acts.getJSONObject(j), "name", "") + ";" + toString(stus) + ";" +
                                    Common.chkNull(json_acts.getJSONObject(j), "activity_text", "") + ";" +
                                    Common.chkNull(json_acts.getJSONObject(j), "activity_photo", "") + ";" +
                                    Common.chkNull(json_acts.getJSONObject(j), "activity_video", "") + ";" +
                                    Common.chkNull(json_acts.getJSONObject(j), "is_staff", "0").equals("0"));
                        }
                    }
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (sel_tab.equalsIgnoreCase("Activities"))
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // add activity response
        }else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddActivityMed")) {
            Log.i("response", response);//.substring(response.indexOf("{"),response.length()));
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, "Activity Added Successfully", Toast.LENGTH_SHORT).show();

                    activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (sel_tab.equalsIgnoreCase("Activities"))
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                if (tag.split("%")[0].equalsIgnoreCase("AddActivityMed")) {
                    //   activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    Toast.makeText(HomeActivity.this, "A medicine activity has been added", Toast.LENGTH_SHORT).show();

                    //  if (sel_tab.equalsIgnoreCase("Activities"))
                    //      grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                } else
                    //   Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

        else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddActivity")) {
            Log.i("response", response);//.substring(response.indexOf("{"),response.length()));
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, "Activity Added Successfully", Toast.LENGTH_SHORT).show();

                    activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (sel_tab.equalsIgnoreCase("Activities"))
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                if(tag.split("%")[0].equalsIgnoreCase("AddActivity")){
                 //   activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    Toast.makeText(HomeActivity.this, "A  activity has been added", Toast.LENGTH_SHORT).show();

                    //  if (sel_tab.equalsIgnoreCase("Activities"))
                  //      grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                }else
             //   Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // add learning response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddLearning")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
//                    activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (sel_tab.equalsIgnoreCase("Activities")) {
                        RequestParams params2 = new RequestParams();
                        params2.add("room_id", sel_room.split(";")[0]);
                        Common.callWebService(HomeActivity.this, Common.wb_getroomactivity, params2, "Activities");
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                    }
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                    onBackPressed();
                Toast.makeText(HomeActivity.this, "A Learning activity has been added", Toast.LENGTH_SHORT).show();
               // Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // add food response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddFood")) {
            Log.i("response", response);
            try {
                stripHtml(response);
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (sel_tab.equalsIgnoreCase("Activities")) {
                        RequestParams params2 = new RequestParams();
                        params2.add("room_id", sel_room.split(";")[0]);
                        Common.callWebService(HomeActivity.this, Common.wb_getroomactivity, params2, "Activities");
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                    }
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                    onBackPressed();
                Toast.makeText(HomeActivity.this, "A Food activity has been added", Toast.LENGTH_SHORT).show();
             //   Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // add nap response

        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddNap")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (sel_tab.equalsIgnoreCase("Activities"))
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                    onBackPressed();
                Toast.makeText(HomeActivity.this, "A medicine activity has been added", Toast.LENGTH_SHORT).show();
               // Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Add Pottoy response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddPotty")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    activities.add(Common.chkNull(jobj, "activity_id", "0") + ";" + tag.split("%")[1].replace("photos", Common.chkNull(jobj, "photo_url", "null")));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    if (sel_tab.equalsIgnoreCase("Activities"))
                        grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                    onBackPressed();
                Toast.makeText(HomeActivity.this, "A Potty activity has been added", Toast.LENGTH_SHORT).show();
            //    Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // get all categories response
            // we get categories for learning
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("GetCategories")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    categories.clear();
                    for (int i = 0; i < jarray.length(); i++)
                        categories.add(Common.chkNull(jarray.getJSONObject(i), "category_id", "0") + ";" + Common.chkNull(jarray.getJSONObject(i), "category_name", "null"));
                    Bundle b = new Bundle();
                    b.putString("type", tag.split("%")[1]);
                    AddLearningFragment alf = new AddLearningFragment();
                    alf.setArguments(b);
                    getSupportFragmentManager().beginTransaction().add(R.id.fl_container, alf, "AddLearning").addToBackStack("AddLearning").commit();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
               // Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            // get food items for add food activity.
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("GetFooditem")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    foods.clear();
                    bottols.clear();
                    for (int i = 0; i < jarray.length(); i++)
                        if (Common.chkNull(jarray.getJSONObject(i), "item_type", "0").equalsIgnoreCase("food"))
                            foods.add(Common.chkNull(jarray.getJSONObject(i), "item_id", "0") + ";" + Common.chkNull(jarray.getJSONObject(i), "item_name", "null"));
                        else
                            bottols.add(Common.chkNull(jarray.getJSONObject(i), "item_id", "0") + ";" + Common.chkNull(jarray.getJSONObject(i), "item_name", "null"));
                    Bundle b = new Bundle();
                    b.putString("type", tag.split("%")[1]);
                    AddFoodFragment alf = new AddFoodFragment();
                    alf.setArguments(b);
                    getSupportFragmentManager().beginTransaction().add(R.id.fl_container, alf, "AddFood").addToBackStack("AddFood").commit();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            // add new category from learning activity
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddCategory")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    categories.add(Common.chkNull(jobj, "category_id", "0") + ";" + tag.split("%")[1]);
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportFragmentManager().findFragmentByTag("AddLearning") != null) {
                        ((AddLearningFragment) getSupportFragmentManager().findFragmentByTag("AddLearning")).refresh();
                        Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    }


                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            // add food item from add food activity
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("AddFoodItem")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {

                    if (Common.chkNull(jobj, "item_type", "0").equalsIgnoreCase("food"))
                        foods.add(Common.chkNull(jobj, "item_id", "0") + ";" + tag.split("%")[1]);
                    else
                        bottols.add(Common.chkNull(jobj, "item_id", "0") + ";" + tag.split("%")[1]);

                    if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportFragmentManager().findFragmentByTag("AddFood") != null) {
                        ((AddFoodFragment) getSupportFragmentManager().findFragmentByTag("AddFood")).refresh();
                        Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // send message response
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("SendMessages")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        getSupportFragmentManager().popBackStack();
                    for (int i = 0; i < tag.split("%")[2].split(",").length; i++) {
                        ArrayList<String> msgs = new ArrayList<String>();
                        String oid = tag.split("%")[2].split(",")[i];
                        Log.i("oid", oid);
                        if (!pref.getString(uid + oid + "messages", "").equals(""))
                            msgs = Common.toList(pref.getString(uid + oid + "messages", ""));
                        msgs.add(Common.chkNull(jobj, "message_id", "0") + ";" + tag.split("%")[1] + ";" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())) + ";" + 1 + ";" + true);
                        pref.edit().putString(uid + oid + "messages", Common.toString(msgs)).commit();
                    }
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        onBackPressed();
                    RequestParams params = new RequestParams();
                    params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
                    params.add("user_id", uid);
                    params.add("user_type", user_type.toLowerCase());
                    Common.callWebService(HomeActivity.this, Common.wb_getusers, params, "GetUsers");
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Learning response

        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("Learnings")) {
            Log.i("response", "" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    learnings.clear();
                    for (int i = 0; i < jarray.length(); i++)
                        learnings.add(Common.chkNull(jarray.getJSONObject(i), "category_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "progress", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "scale", "null"));
                    //pref.edit().putString(uid+tag.split("%")[1]+"events",Common.toString(events)).commit();
                    if (sel_tab.equalsIgnoreCase("Learning"))
                        grid_home.setAdapter(new HomeLearningListAdapter(HomeActivity.this, learnings, grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this,"No Recent Learnings", Toast.LENGTH_SHORT).show();

               // Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("Tools")) {
            Log.i("response", "" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    NavMenuToolsFragment nmtf = new NavMenuToolsFragment();
                    JSONObject data = new JSONObject(jobj.getString("data"));
                    setActionDrawer(false, "Tools");
                    ArrayList<String> counts = new ArrayList<String>();
                    counts.add("Rooms;" + Common.chkNull(data, "room_count", "0"));
                    counts.add("Students;" + Common.chkNull(data, "student_count", "0"));
                    counts.add("Teachers;" + Common.chkNull(data, "teacher_count", "0"));
                    counts.add("Parent;" + Common.chkNull(data, "parent_count", "0"));
                    Bundle b = new Bundle();
                    b.putStringArrayList("tools", counts);
                    nmtf.setArguments(b);
                    getSupportFragmentManager().beginTransaction().add(R.id.fl_container, nmtf, "AdminTools").addToBackStack("AdminTools").commit();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("Ratio")) {
            Log.i("response", "" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    ArrayList<String> ratios = new ArrayList<String>();
                    ratios.add("-1" + ";" + "Rooms" + ";" + "Students" + ";" + "Teachers" + ";" + "Ratio");
                    for (int i = 0; i < jarray.length(); i++)
                        ratios.add(Common.chkNull(jarray.getJSONObject(i), "room_id", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "room_name", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "student", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "teacher", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "ratio", ""));
                    NavMenuToolsRoomCheckFragment nmtrcf = new NavMenuToolsRoomCheckFragment();
                    //setActionDrawer(false,"Tools");
                    getSupportActionBar().setTitle("Room Check");
                    Bundle b = new Bundle();
                    b.putStringArrayList("ratio", ratios);
                    nmtrcf.setArguments(b);
                    getSupportFragmentManager().beginTransaction().add(R.id.fl_container, nmtrcf, "RoomCheck").addToBackStack("RoomCheck").commit();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // All students response
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("AllStudents")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    ArrayList<String> all_students = new ArrayList<String>();
                    for (int i = 0; i < jarray.length(); i++)
                        all_students.add(Common.chkNull(jarray.getJSONObject(i), "student_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "studentName", "") + ";" +//Common.chkNull(jarray.getJSONObject(i),"first_name","")+" "+Common.chkNull(jarray.getJSONObject(i),"last_name","")+";"+
                                Common.chkNull(jarray.getJSONObject(i), "birthdate", "0000-00-00") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "photo", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "room_id", "0") + ";" +

                                Common.chkNull(jarray.getJSONObject(i), "status", "0"));
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportFragmentManager().findFragmentByTag("ManageRooms") != null)
                        ((ManageRoomsFragment) getSupportFragmentManager().findFragmentByTag("ManageRooms")).set(all_students);
                    if (getSupportActionBar().getTitle().equals(""))
                        grid_home.setAdapter(new HomeStudentListAdapter(HomeActivity.this, students, "", grid_home, empty));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Add student room response
        } else if (response != null && response.length() > 0 && tag.split("@")[0].equalsIgnoreCase("AddStudentRoom")) {
            Log.i("response", "" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    String stu_id = tag.split("@")[1];
                    String room_id = tag.split("@")[2];
                    int pos = -1;
                    ArrayList<String> all_students = Common.toList(tag.split("@")[3]);
                    for (int i = 0; i < all_students.size(); i++)
                        if (all_students.get(i).split(";")[0].equalsIgnoreCase(stu_id)) {
                            pos = i;
                            break;
                        }
                    if (pos >= 0) {
                        String ss = all_students.get(pos).split(";")[0] + ";" + all_students.get(pos).split(";")[1] + ";" +
                                all_students.get(pos).split(";")[2] + ";" + all_students.get(pos).split(";")[3] + ";"
                                + room_id + ";" + all_students.get(pos).split(";")[5];
                        all_students.remove(pos);
                        all_students.add(pos, ss);
                        if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportFragmentManager().findFragmentByTag("ManageRooms") != null)
                            ((ManageRoomsFragment) getSupportFragmentManager().findFragmentByTag("ManageRooms")).refresh_students(all_students, pos);
                        Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // remove student room
        } else if (response != null && response.length() > 0 && tag.split("@")[0].equalsIgnoreCase("RemoveStudentRoom")) {
            Log.i("response", "" + response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    String stu_id = tag.split("@")[1];
                    String room_id = tag.split("@")[2];
                    int pos = 0;
                    ArrayList<String> all_students = Common.toList(tag.split("@")[3]);
                    for (int i = 0; i < all_students.size(); i++)
                        if (all_students.get(i).split(";")[0].equalsIgnoreCase(stu_id)) {
                            pos = i;
                            break;
                        }
                    String ss = all_students.get(pos).split(";")[0] + ";" + all_students.get(pos).split(";")[1] + ";" +
                            all_students.get(pos).split(";")[2] + ";" + all_students.get(pos).split(";")[3] + ";" + "-1" + ";" + all_students.get(pos).split(";")[5];
                    all_students.remove(pos);
                    all_students.add(pos, ss);
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportFragmentManager().findFragmentByTag("ManageRooms") != null)
                        ((ManageRoomsFragment) getSupportFragmentManager().findFragmentByTag("ManageRooms")).refresh_students(all_students, pos);
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(HomeActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {

                Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    public void whenAddFood() {
        if (sel_tab.equalsIgnoreCase("Activities")) {
            RequestParams params2 = new RequestParams();
            params2.add("room_id", sel_room.split(";")[0]);
            Common.callWebService(HomeActivity.this, Common.wb_getroomactivity, params2, "Activities");
            grid_home.setAdapter(new HomeActivityListAdapter(HomeActivity.this, spin_activity_types.getSelectedItem().toString(), activities, !getSupportActionBar().getTitle().equals(""), grid_home, empty));
            fab.performClick();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Common.PUSH_NOTIFICATION));
        if (MultiMessageActivity.isyes || MessagesActivity.isyes) {

            MultiMessageActivity.isyes = false;
            MessagesActivity.isyes = false;
            RequestParams params = new RequestParams();

            params.add("room_id", Common.chkNull(sel_room.split(";")[0], "0"));
            params.add("user_type", user_type.toLowerCase());
            params.add("user_id", uid);
            Common.callWebService(HomeActivity.this, Common.wb_getusers, params, "GetUsers");
        }

        if (sel_tab.equalsIgnoreCase("Attendance")) {
            getUpdatedAttendance();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    // when click on call option menu then call this method
    // show dialog to do call and click on number call to set number
    public void showProfileParent() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.parent_profile_layout);

        TextView txt_call = (TextView) dialog.findViewById(R.id.txt_call);
        txt_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.hide();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:8796443695"));
                startActivity(callIntent);
            }
        });

        dialog.show();
    }

    // this method is called for dialog teacher check in check out
    public void showdialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.teacher_attendance_layout);

        final CheckBox checkbox_in = (CheckBox) dialog.findViewById(R.id.checkbox_in);
        final CheckBox checkbox_out = (CheckBox) dialog.findViewById(R.id.checkbox_out);
        Button btn_done = (Button) dialog.findViewById(R.id.btn_done);

        // when click on check in box
        checkbox_in.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkbox_out.setChecked(false);
                    check_status = "chekin";
                    URl = "https://kindertag.in/admin/webapi/teacherCheckin";
                }
            }
        });

        // click on checkout box
        checkbox_out.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkbox_in.setChecked(false);
                    check_status = "checkout";
                    URl = "https://kindertag.in/admin/webapi/teacherCheckout";
                }
            }
        });

        // click on btn_done
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!check_status.equalsIgnoreCase("")) {
                    dialog.hide();
                    teacher_attendance();
                } else
                    Toast.makeText(HomeActivity.this, "Please select Check In or Check Out", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();
    }
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    public String getRealPathFromUri(final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(HomeActivity.this, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(HomeActivity.this
                        , contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(HomeActivity.this, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(HomeActivity.this, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
    private void changepassword_form(String new_password){
        final ProgressDialog pd = new ProgressDialog(HomeActivity.this);//,R.style.MyTheme);
        //pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pd.setMessage("Please Wait...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        if (filePath != null && !filePath.isEmpty()) {
            File file = new File(filePath);
            if (file.exists()) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(API_URL_BASE)
                        .build();

                WebAPIService service = retrofit.create(WebAPIService.class);

                // MultipartBody.Part is used to send also the actual filename
                // adds another part within the multipart request
                RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), uid);
                RequestBody password = RequestBody.create(MediaType.parse("multipart/form-data"), new_password);
                // executes the request
                Call<ResponseBody> call = service.postFile_p(user_id,password);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           retrofit2.Response<ResponseBody> response) {
                        if (pd != null && pd.isShowing()) pd.dismiss();
                        String output =response.body().toString();
                        Log.i(LOG_TAG, output);

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (pd != null && pd.isShowing()) pd.dismiss();

                        Log.e(LOG_TAG, t.getMessage());
                    }
                });
            }
        }
    }
    private void uploadFile(Uri fileUri) {
            final ProgressDialog pd = new ProgressDialog(HomeActivity.this);//,R.style.MyTheme);
            //pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            pd.setMessage("Please Wait...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
        String filePath = getRealPathFromUri(fileUri);
        if (filePath != null && !filePath.isEmpty()) {
            File file = new File(filePath);
            if (file.exists()) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(API_URL_BASE)
                        .build();

                WebAPIService service = retrofit.create(WebAPIService.class);

                // creates RequestBody instance from file
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                // MultipartBody.Part is used to send also the actual filename
                MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
                // adds another part within the multipart request
                String descriptionString = "Sample description";
                RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), uid);
                RequestBody first_name = RequestBody.create(MediaType.parse("multipart/form-data"), pref.getString(uid + "name", ""));
                RequestBody last_name = RequestBody.create(MediaType.parse("multipart/form-data"), pref.getString(uid + "name", ""));
                RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"), pref.getString(uid + "email", ""));
                // executes the request
                Call<ResponseBody> call = service.postFile(image, user_id,first_name,last_name,email);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           retrofit2.Response<ResponseBody> response) {
                        if (pd != null && pd.isShowing()) pd.dismiss();
                        user_profile.setImageURI(imageUri);
                        txt_img.setVisibility(View.GONE);
                        String output =response.body().toString();
                        Log.i(LOG_TAG, output);

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (pd != null && pd.isShowing()) pd.dismiss();

                        Log.e(LOG_TAG, t.getMessage());
                    }
                });
            }
        }
    }
    // this method from get current location of user
    // we get location using mobile gps system
    public void updateLocation() {

        locationProvider = new ReactiveLocationProvider(this);

        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setNumUpdates(1)
                .setExpirationDuration(TimeUnit.SECONDS.toMillis(5))
                .setInterval(TimeUnit.SECONDS.toMillis(10))
                .setMaxWaitTime(TimeUnit.SECONDS.toMillis(5));
        locationProvider.getUpdatedLocation(locationRequest)    //does not get called on some devices but we still call it anyway
                .startWith(locationProvider.getLastKnownLocation()) //sure to get a value
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Location>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Location: ", e.toString());
                    }

                    @Override
                    public void onNext(Location location) {
                        String latitude = String.valueOf(location.getLatitude());
                        String longitude = String.valueOf(location.getLongitude());
                        Log.e("Location:", "Latitude: " + latitude + " Longitude: " + longitude);
                        //----------------------------------------------------
                        lat = String.valueOf(latitude);
                        lang = String.valueOf(longitude);

                        Log.e("lat", lat);
                        Log.e("lang", lang);
                    }

                });
    }

    // teacher attendance method
    private void teacher_attendance() {
        Log.e("url", URl);

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URl,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        Log.e("res", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            if (status.equalsIgnoreCase("1")) {
                                Toast.makeText(context, object.getString("msg") + "", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, object.getString("msg") + "", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("Error..", "Try Again");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();

                        Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();
                        //TODO
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                Log.e("teacher_id", uid);
                Log.e("lat", lat);
                Log.e("lan", lang);

                // passing parameter to teacher attendance
                map.put("teacher_id", uid);
                map.put("lat", lat);
                map.put("lan", lang);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

    // This dialog show to change password
    // when user login first time
    public void changepassDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.z_layoutchangepassword);

        final TextInputLayout edt_pass1 = (TextInputLayout) dialog.findViewById(R.id.edt_pass1);
        final TextInputLayout edt_newpass1 = (TextInputLayout) dialog.findViewById(R.id.edt_newpass1);

        final EditText edt_pass = (EditText) dialog.findViewById(R.id.edt_pass);
        final EditText edt_newpass = (EditText) dialog.findViewById(R.id.edt_newpass);
        Button btn_done = (Button) dialog.findViewById(R.id.btn_done);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String curre_password = edt_pass.getText().toString().trim();
                final String new_password = edt_newpass.getText().toString().trim();

                if (curre_password.equalsIgnoreCase("")) {
                    edt_pass1.setError("Enter new password.");

                } else if (new_password.equalsIgnoreCase("")) {
                    edt_newpass1.setError("Confirm new password.");
                } else if (!new_password.equalsIgnoreCase(curre_password)) {
                    edt_newpass1.setError("New password and confirm password must be same");
                } else {
                    changepassword_form(new_password);
                   // changepassword(new_password);
                    dialog.dismiss();
                }
            }
        });

        edt_pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edt_pass1.setErrorEnabled(false);
                edt_newpass1.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edt_newpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edt_pass1.setErrorEnabled(false);
                edt_newpass1.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dialog.show();
    }


    // call change password webservice
    private void changepassword(final String newpass) {
        Log.e("url", URl);

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CNG_PASS_URl,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        Log.e("res", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            if (status.equalsIgnoreCase("1")) {
                                SharePref.setfirstlogin(HomeActivity.this, "1");
                                Toast.makeText(context, object.getString("msg") + "", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, object.getString("msg") + "", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("Error..", "Try Again");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();

                        Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();
                        //TODO
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                Log.e("uid", uid);

                map.put("user_id", uid);
                map.put("password", newpass);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

    void showPhotoDialogforActivity() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom);
        dialog.setTitle("Photo");

        // set the custom dialog components - text, image and button
        TextView camera = (TextView) dialog.findViewById(R.id.camera);
        TextView gallery = (TextView) dialog.findViewById(R.id.gallery);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // take a photo from camera check camera permission and open camera.
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                        ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                } else {
                    Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File photo = new File(Environment.getExternalStorageDirectory(), "temp" + newimgpos + ".png");
                    camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                    startActivityForResult(camera, 3);
                }
            }
        });

        // choose photo from gallery, check runtime permission of gallery and open gallery to choose image.
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (
                        ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 7);
                } else {
                    Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    gallery.setType("image/*");
                    startActivityForResult(gallery, 7);
                }
            }
        });

        dialog.show();
    }
    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return String.valueOf(Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY));
        } else {
            return String.valueOf(Html.fromHtml(html));
        }
    }
    private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap>{
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView){
            this.imageView = imageView;
        }

        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();
                /*
                    decodeStream(InputStream is)
                        Decode an input stream into a bitmap.
                 */
                logo = BitmapFactory.decodeStream(is);
            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        /*
            onPostExecute(Result result)
                Runs on the UI thread after doInBackground(Params...).
         */

        protected void onPostExecute(Bitmap result){
            imageView.setImageBitmap(result);
        }
    }
}

