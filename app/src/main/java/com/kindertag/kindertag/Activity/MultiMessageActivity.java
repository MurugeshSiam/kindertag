package com.kindertag.kindertag.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kindertag.kindertag.Adapter.HomeNewActivtyStudentsGridAdapter;
import com.kindertag.kindertag.Adapter.HomeNewActivtyStudentsMessageGridAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.KinderActivity;
import com.kindertag.kindertag.Utils.SharePref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MultiMessageActivity extends AppCompatActivity {

    // Declare elements.
    Toolbar toolbar;
    EditText message;
    TextView send;
    GridView grid_receivers;
    ArrayList<String> receivers=new ArrayList<String>();
    ArrayList<String> sel_receivers=new ArrayList<String>();

    public Context context=MultiMessageActivity.this;
    public static boolean isyes=false;
    boolean setSele=true;

    String std_ids="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_message);

        isyes=true;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // visible back button
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.thene_blue_dark));
        }

        // find views declared in xml
        grid_receivers = (GridView) findViewById(R.id.grid_multi_message_receivers);
        message = (EditText) findViewById(R.id.edit_multi_message);
        send = (TextView) findViewById(R.id.btn_multi_message_send);

        receivers=HomeActivity.getReceivers();

        // get all student list and set to grid receivers
        grid_receivers.setAdapter(new HomeNewActivtyStudentsMessageGridAdapter(context,receivers,sel_receivers,"",true));

        // select those student whome send a message
        grid_receivers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(sel_receivers.contains(((HomeActivity)context).getStudents().get(i)))
                    sel_receivers.remove(((HomeActivity)context).getStudents().get(i));
                else
                    sel_receivers.add(((HomeActivity)context).getStudents().get(i));
                    Log.e("what is",((HomeActivity)context).getStudents().get(i));
            }});

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Enter messsage in message box and click on send button
                // call send message method passing parameter sender id, receiver id, student id and text message.
                if(Common.validate(message,1,true)) {
                    String ids=getIds(sel_receivers);
                    if (std_ids.length() > 1){std_ids=std_ids.substring(1,std_ids.length());}

                    if (HomeActivity.user_type.equalsIgnoreCase("Parent"))
                        std_ids=SharePref.getstdid(context);
                    RequestParams params = new RequestParams();
                    params.add("sender_id",HomeActivity.uid);
                    params.add("receiver_id",ids);
                    params.add("student_id",std_ids);
                    params.add("messageText",message.getText().toString());
                    callWebService(MultiMessageActivity.this,Common.wb_sendmessage,params,"SendMessages"+"%"+message.getText().toString()+"%"+ids);
                }
            }});

    }

    // find selected students id from grid view
    public String getIds(ArrayList<String> sel_students){
        String str="";
        std_ids="";
        for(int i=0;i<sel_students.size();i++) {
            str += "," + sel_students.get(i).split(";")[0];

            String [] arrstdid=sel_students.get(i).split(";");
            std_ids += "," + arrstdid[arrstdid.length-1];
        }
        return str.length()>0?str.substring(1):str;
    }


    // perform action on back button
    // perform action on select all option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        if (item.getItemId() == R.id.action_select_all)
        {
            setSelectionAll(setSele);

            if (setSele){setSele=false;}else {setSele=true;}
        }

        return super.onOptionsItemSelected(item);
    }

    // action select all student
    public void setSelectionAll(boolean isSelectAll){
        if(isSelectAll) {
            sel_receivers.clear();
            for(int i=0;i<receivers.size();i++)
                sel_receivers.add(receivers.get(i));
        }
        else
            sel_receivers.clear();
        grid_receivers.setAdapter(new HomeNewActivtyStudentsGridAdapter(context,receivers,sel_receivers,"",true));
    }

    // call webservice method to send message
    public void callWebService(final MultiMessageActivity activity, final String url, final RequestParams params, final String tag){
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo()!= null && connectivityManager.getActiveNetworkInfo().isConnected()){
            final ProgressDialog pd = new ProgressDialog(activity);//,R.style.MyTheme);
            //pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            pd.setMessage("Please Wait...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            new AsyncHttpClient().post(activity,url,params,new AsyncHttpResponseHandler() {
                public void onStart() {
                    super.onStart();
                    pd.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    if(pd!=null && pd.isShowing()) pd.dismiss();
                    Toast.makeText(activity, "Message Sent Successfully", Toast.LENGTH_SHORT).show();
                    message.setText("");
                    finish();
                }

                @Override
                public void onFailure(int statusCode,Header[] headers, byte[] responseBody,Throwable error) {
                    if(pd!=null && pd.isShowing()) pd.dismiss();
                    String  msg=statusCode==400?"Bad Request":statusCode==401?"Unauthorized":statusCode==403?"Forbidden":statusCode==404?"Not Found":
                            statusCode==500?"Internal Server Error":statusCode==550?"Permission denied":"Something went wrong at server side";
                    Toast.makeText(activity,msg,Toast.LENGTH_SHORT).show();
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(pd!=null && pd.isShowing()) {
                        pd.dismiss();
                        Toast.makeText(activity, "Connection timed out!\nData may not be updated!", Toast.LENGTH_SHORT).show();
                    }
                }},60000);
        }
        else
            Toast.makeText(activity,"Please check the Internet Connection!",Toast.LENGTH_SHORT).show();
    }

    // option menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menumultimessage, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
