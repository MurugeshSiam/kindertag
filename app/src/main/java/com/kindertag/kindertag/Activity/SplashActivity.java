package com.kindertag.kindertag.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.KinderActivity;

public class SplashActivity extends KinderActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.thene_blue_dark));
        }

        // this is splash activity
        // use handler to set delay, here 3.5 sec delay
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
         @Override
         public void run() {
           startActivity(new Intent(SplashActivity.this,LoginActivity.class));
           finish();
        }},3500);

    }

}
