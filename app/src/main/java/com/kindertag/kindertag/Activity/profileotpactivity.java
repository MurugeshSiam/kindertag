
package com.kindertag.kindertag.Activity;

        import android.Manifest;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.content.pm.PackageManager;
        import android.os.Build;
        import android.os.Bundle;
        import android.support.annotation.RequiresApi;
        import android.support.design.widget.TextInputEditText;
        import android.support.design.widget.TextInputLayout;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.content.ContextCompat;
        import android.support.v4.content.LocalBroadcastManager;
        import android.support.v7.app.AlertDialog;
        import android.text.Editable;
        import android.text.Html;
        import android.text.TextWatcher;
        import android.util.Log;
        import android.view.View;
        import android.view.Window;
        import android.view.WindowManager;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.LinearLayout;
        import android.widget.RelativeLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.google.firebase.iid.FirebaseInstanceId;
        import com.kindertag.kindertag.R;
        import com.kindertag.kindertag.Utils.Common;
        import com.kindertag.kindertag.Utils.KinderActivity;
        import com.kindertag.kindertag.Utils.SharePref;
        import com.loopj.android.http.RequestParams;
        import android.provider.Settings.Secure;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.List;

public class profileotpactivity extends KinderActivity {
    RelativeLayout hide_keyboard;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    TextView txt_logo, txt_mid_title, txt_botom_link, txt_type_welcome;
    LinearLayout ll_choice, ll_login, ll_type, ll_start, ll_start1, ll_start2, ll_start3, ll_fp, ll_fp1, ll_fp2, ll_fp3;
    TextInputLayout til_login_email, til_login_pass, til_start_fname, til_start_lname, til_start_email, til_start_mobile, til_start_pass, til_start_confirm, til_fp_mobile, til_fp_otp, til_fp_new, til_fp_confirm;
    EditText input_email;
    String phone_no;
    Integer user_id;
    final TextWatcher clear = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            clearErrors();
        }
    };

    Button btn_create, btn_signin, btn_login, btn_teacher, btn_parent, btn_start, btn_submit;
    String sel_type = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_otp);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.thene_blue_dark));
        }
        phone_no = getIntent().getStringExtra("phone_no");
        user_id = getIntent().getIntExtra("user_id",-1);

        //Set Views
//        hide_keyboard = (RelativeLayout) findViewById(R.id.hide_keyboard);
//        hide_keyboard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//            }
//        });
        txt_logo = (TextView) findViewById(R.id.txt_company_logo);
        txt_mid_title = (TextView) findViewById(R.id.txt_mid_title);
        ll_choice = (LinearLayout) findViewById(R.id.ll_choice);
        btn_create = (Button) findViewById(R.id.btn_choice_create);
        btn_signin = (Button) findViewById(R.id.btn_choice_signin);
        ll_login = (LinearLayout) findViewById(R.id.ll_login);
        til_login_email = (TextInputLayout) findViewById(R.id.til_login_email);

        til_login_pass = (TextInputLayout) findViewById(R.id.til_login_password);
        txt_botom_link = (TextView) findViewById(R.id.txt_bottom_link);
        btn_login = (Button) findViewById(R.id.btn_login);
        ll_type = (LinearLayout) findViewById(R.id.ll_type);
        txt_type_welcome = (TextView) findViewById(R.id.txt_type_welcome);
        btn_teacher = (Button) findViewById(R.id.btn_type_teacher);
        btn_parent = (Button) findViewById(R.id.btn_type_parent);
        ll_start = (LinearLayout) findViewById(R.id.ll_start);
        ll_start1 = (LinearLayout) findViewById(R.id.ll_start_1);
        ll_start2 = (LinearLayout) findViewById(R.id.ll_start_2);
        ll_start3 = (LinearLayout) findViewById(R.id.ll_start_3);
        til_start_fname = (TextInputLayout) findViewById(R.id.til_start_firstname);
        til_start_lname = (TextInputLayout) findViewById(R.id.til_start_lastname);
        til_start_email = (TextInputLayout) findViewById(R.id.til_start_email);
        til_start_mobile = (TextInputLayout) findViewById(R.id.til_start_mobile);
        til_start_pass = (TextInputLayout) findViewById(R.id.til_start_password);
        til_start_confirm = (TextInputLayout) findViewById(R.id.til_start_confirm_password);
        btn_start = (Button) findViewById(R.id.btn_start);
        ll_fp = (LinearLayout) findViewById(R.id.ll_forgot_password);
        ll_fp1 = (LinearLayout) findViewById(R.id.ll_forgot_password_1);
        ll_fp2 = (LinearLayout) findViewById(R.id.ll_forgot_password_2);
        ll_fp3 = (LinearLayout) findViewById(R.id.ll_forgot_password_3);
        til_fp_mobile = (TextInputLayout) findViewById(R.id.til_forgot_password_mobile);
        til_fp_otp = (TextInputLayout) findViewById(R.id.til_forgot_password_otp);
        til_fp_new = (TextInputLayout) findViewById(R.id.til_forgot_password_new);
        til_fp_confirm = (TextInputLayout) findViewById(R.id.til_forgot_password_confirm);
        btn_submit = (Button) findViewById(R.id.btn_forgot_password_submit);

        //txt_logo.setText(Html.fromHtml("<b><i>Kinder Tag</i></b>"));
        txt_type_welcome.setText(Html.fromHtml("<big><b>Welcome!</b></big><br/>Which describes you?"));

        til_login_email.getEditText().addTextChangedListener(clear);
        til_login_pass.getEditText().addTextChangedListener(clear);
        til_start_fname.getEditText().addTextChangedListener(clear);
        til_start_lname.getEditText().addTextChangedListener(clear);
        til_start_email.getEditText().addTextChangedListener(clear);
        til_start_mobile.getEditText().addTextChangedListener(clear);
        til_start_pass.getEditText().addTextChangedListener(clear);
        til_start_confirm.getEditText().addTextChangedListener(clear);
        til_fp_mobile.getEditText().addTextChangedListener(clear);
        til_fp_otp.getEditText().addTextChangedListener(clear);
        til_fp_new.getEditText().addTextChangedListener(clear);
        til_fp_confirm.getEditText().addTextChangedListener(clear);

        //Set OnClick Listeners
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllText();
                ll_choice.setVisibility(View.GONE);
                txt_mid_title.setText("Lets get started!");
                txt_botom_link.setText("by creating an account, I agree to terms of service.");
                txt_mid_title.setVisibility(View.INVISIBLE);
                txt_botom_link.setVisibility(View.VISIBLE);
                ll_type.setVisibility(View.VISIBLE);
            }
        });

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllText();
                ll_choice.setVisibility(View.GONE);
                txt_mid_title.setText("Sign in to Continue");
                txt_botom_link.setText("Forgot Password?");
                txt_mid_title.setVisibility(View.VISIBLE);
                txt_botom_link.setVisibility(View.VISIBLE);
                ll_login.setVisibility(View.VISIBLE);
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearErrors();
                otpservice();
            }
        });


    }

    public void otpservice() {
        RequestParams params = new RequestParams();
        params.add("otp_code", til_fp_otp.getEditText().getText().toString());
        params.add("phone_no",phone_no);
        params.add("user_id", user_id.toString());
        Common.callWebService(profileotpactivity.this, Common.wb_parentCheckOtp, params, "parentCheckOtp");
    }

    @Override
    public void onBackPressed() {
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Exit Verification");
            builder.setMessage("Do you really want to go back without entering otp ?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    finish();
                }
            });
            builder.create().show();
        }
    }

    public void clearErrors() {

        //til_fp_confirm.setErrorEnabled(false);
    }

    public boolean validatePass(TextInputLayout til_pass, TextInputLayout til_confirm) {
        if (Common.validate(til_pass, 6, true) && Common.validate(til_confirm, 1, true)) {
            if (!til_pass.getEditText().getText().toString().equals(til_confirm.getEditText().getText().toString())) {
                //til_start_confirm.setErrorEnabled(true);
                til_confirm.getEditText().setError("Password & Confirm Password does not match");
                til_confirm.getEditText().requestFocus();
            } else
                return true;
        }
        return false;
    }

    public boolean validateOTP(TextInputLayout til_otp, String otp) {
        if (Common.validate(til_otp, 4, true)) {
            if (!til_otp.getEditText().getText().toString().equals(otp)) {
                //til_start_confirm.setErrorEnabled(true);
                til_otp.getEditText().setError("Invalid OTP");
                til_otp.getEditText().requestFocus();
            } else
                return true;
        }
        return false;
    }

    public void clearAllText() {
        til_login_email.getEditText().setText("");
        til_login_pass.getEditText().setText("");
        til_start_fname.getEditText().setText("");
        til_start_lname.getEditText().setText("");
        til_start_email.getEditText().setText("");
        til_start_mobile.getEditText().setText("");
        til_start_pass.getEditText().setText("");
        til_start_confirm.getEditText().setText("");
        til_fp_mobile.getEditText().setText("");
        til_fp_otp.getEditText().setText("");
        til_fp_new.getEditText().setText("");
        til_fp_confirm.getEditText().setText("");
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String otp = intent.getStringExtra("otp");

                final String message = intent.getStringExtra("message");
                String j = "";

                Number(message);
                j = Number(message);

                til_fp_otp.getEditText().setText(j);

            }
        }
    };

    private String Number(String messages) {


        String otp = "";
        try {
            for (int i = 0; i < messages.length(); i++) {

                String name = messages;
                Character character = name.charAt(i);

                int ascii = (int) character;
                if (ascii >= 48 && ascii <= 57) {
                    otp = otp + character;
                }
            }
        } catch (Exception e) {

        }
        return otp;

    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onSuccess(String response, String tag) {
        super.onSuccess(response, tag);
        if (response != null && response.length() > 0 && tag.equalsIgnoreCase("parentCheckOtp")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
Intent intent = new Intent(profileotpactivity.this,HomeActivity.class);
startActivity(intent);
                } else if (jobj.getInt("status") == 0) {
                    Toast.makeText(profileotpactivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(profileotpactivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}
