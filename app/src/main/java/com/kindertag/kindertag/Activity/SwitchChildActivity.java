package com.kindertag.kindertag.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kindertag.kindertag.Adapter.ChildernListAdapter;
import com.kindertag.kindertag.Fragment.AddChildFragment;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.KinderActivity;
import com.kindertag.kindertag.Utils.SharePref;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SwitchChildActivity extends KinderActivity {
    Dialog myDialog = null;

    // Declare elements.
    public String userType = "";
    ListView list_children;
    Toolbar toolbar;
    boolean isInvite = false;
    ArrayList<String> childern = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_children);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.thene_blue_dark));
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Children");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Children");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // visible back button
        if(userType.equalsIgnoreCase("Teacher")){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        userType = pref.getString(uid + "role", "");
        getSupportActionBar().setTitle(userType.equalsIgnoreCase("Teacher") ? "Add Room Code" : "My Children");
        isInvite = pref.getString(uid + "isInvite", "0").equals("1");
        list_children = (ListView) findViewById(R.id.list_children);

        //Set ListAdapter
        list_children.setAdapter(new ChildernListAdapter(SwitchChildActivity.this, childern));
        list_children.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pref.edit().putString(uid + "sel_child", list_children.getAdapter().getItem(i).toString()).commit();

                String[] student_id = list_children.getAdapter().getItem(i).toString().split(";");
                Log.e("selechild", student_id[0]);
                SharePref.setstdid(getBaseContext(), student_id[0]);
                startActivity(new Intent(SwitchChildActivity.this, HomeActivity.class));
                finish();
            }
        });

        // if parent have already child then get his child by passing parent id and show child details.
        // if parent not added child then he goes to AddChildFragment
        if (userType.equalsIgnoreCase("Parent")) {
            if (isInvite) {
                RequestParams params = new RequestParams();
                params.add("parent_id", uid);
                Common.callWebService(SwitchChildActivity.this, Common.wb_getstudent, params, "GetChildren");
            } else if (!isInvite) {
                getSupportActionBar().setTitle("Add Child");
                invalidateOptionsMenu();
                getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new AddChildFragment(), "AddChild").addToBackStack("AddChild").commit();
            }
        } else if (userType.equalsIgnoreCase("Teacher")) {
            getSupportActionBar().setTitle("Add Room Code");
            invalidateOptionsMenu();
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new AddChildFragment(), "RoomCode").addToBackStack("RoomCode").commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    // action perform when click phone back button

    private void showDialog(String title, String message) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(SwitchChildActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                finish();                    }
                        }


                );


        // Dismiss any old dialog.
        if (myDialog != null) {
            myDialog.dismiss();
        }

        // Show the new dialog.
        myDialog = dialogBuilder.show();
    }
    // show action add button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_children, menu);
        menu.findItem(R.id.action_add).setVisible(getSupportActionBar().getTitle().toString().equalsIgnoreCase("My Children"));
        return super.onCreateOptionsMenu(menu);
    }

    // perform click on option menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            getSupportActionBar().setTitle("Add Child");
            invalidateOptionsMenu();
            getSupportFragmentManager().beginTransaction().add(R.id.fl_container, new AddChildFragment(), "AddChild").addToBackStack("AddChild").commit();
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    // get response of get child passing parentid
    @Override
    public void onSuccess(String response, String tag) {
        super.onSuccess(response, tag);
        if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("GetChildren")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    childern.clear();
                    for (int i = 0; i < jarray.length(); i++)
                        childern.add(Common.chkNull(jarray.getJSONObject(i), "student_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "studentName", "") + ";" +//jarray.getJSONObject(i),"first_name","")+" "+Common.chkNull(jarray.getJSONObject(i),"last_name","")+";"+
                                Common.chkNull(jarray.getJSONObject(i), "birthdate", "0000-00-00") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "photo", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "room_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parent_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parentname", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parentemail", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parentphone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_email", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "gurdian_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "gurdian_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "emergency_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "emergency_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "allergy", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "medication", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "doctorName", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "doctorPhone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "address", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "address2", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "city", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "state", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "country", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "zip", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "notes", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "student_code", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "status", "0")+ ";" +
                                Common.chkNull(jarray.getJSONObject(i), "user_id", "null")+ ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parent_type", "null")

                        );
                    list_children.setAdapter(new ChildernListAdapter(SwitchChildActivity.this, childern));
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(SwitchChildActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(SwitchChildActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("JoinCode")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    childern.clear();

                    for (int i = 0; i < jarray.length(); i++)
                        childern.add(Common.chkNull(jarray.getJSONObject(i), "student_id", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "studentName", "") + ";" +//jarray.getJSONObject(i),"first_name","")+" "+Common.chkNull(jarray.getJSONObject(i),"last_name","")+";"+
                                Common.chkNull(jarray.getJSONObject(i), "birthdate", "0000-00-00") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "photo", "") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "room_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parent_id", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parentname", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parentemail", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parentphone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_email", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "mother_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "gurdian_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "gurdian_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "emergency_name", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "emergency_phone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "allergy", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "medication", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "doctorName", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "doctorPhone", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "address", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "address2", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "city", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "state", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "country", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "zip", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "notes", "null") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "student_code", "0") + ";" +
                                Common.chkNull(jarray.getJSONObject(i), "status", "0")+ ";" +
                                Common.chkNull(jarray.getJSONObject(i), "user_id", "null")+ ";" +
                                Common.chkNull(jarray.getJSONObject(i), "parent_type", "null")
                        );
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        getSupportFragmentManager().popBackStack();
                    if (list_children != null)
                        list_children.setAdapter(new ChildernListAdapter(SwitchChildActivity.this, childern));
                    pref.edit().putString(uid + "isInvite", "1").commit();
                    isInvite = pref.getString(uid + "isInvite", "0").equals("1");
                    //Toast.makeText(MyChildrenActivity.this,"Child Added Successfully!",Toast.LENGTH_SHORT).show();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(SwitchChildActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(SwitchChildActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("JoinCodeTeacher")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONObject jarray = new JSONObject(Common.chkNull(jobj, "data", ""));
                    pref.edit().putString(uid + "isInvite", "1").commit();
                    isInvite = pref.getString(uid + "isInvite", "0").equals("1");
                    pref.edit().putString(uid + "room_id", Common.chkNull(jarray, "room_id", "")).commit();
                    startActivity(new Intent(SwitchChildActivity.this, HomeActivity.class));
                    finish();
                    //Toast.makeText(MyChildrenActivity.this,"Child Added Successfully!",Toast.LENGTH_SHORT).show();
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(SwitchChildActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(SwitchChildActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}
