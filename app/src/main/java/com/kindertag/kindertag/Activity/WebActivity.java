package com.kindertag.kindertag.Activity;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.kindertag.kindertag.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//nileshpoman.969@gmail.com
//admin@123
public class WebActivity extends AppCompatActivity {

    String filepath;
    String img_url;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        ImageButton img_download = (ImageButton) findViewById(R.id.img_download);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Photo");
        // set title and visible back button
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Photo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // set image url to imageview using picasso
        ImageView web_image = (ImageView) findViewById(R.id.web_image);
        img_url = getIntent().getStringExtra("url").replace("\\", "");
        Log.e("url", img_url);
        Picasso.with(WebActivity.this).load(img_url).resize(400, 400).into(web_image);

        // set click listener on download button
        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(WebActivity.this, "Downloading...", Toast.LENGTH_SHORT).show();
                downloadFile(img_url);
            }
        });
    }

    // perform acton on back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // create folder in External storage name as KinderTag
    // download image from url and save in external storage
    public void downloadFile(String uRl) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Kindertag");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        Toast.makeText(this, "Image Saved to "+direct.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        // use date to set saved image name
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        DownloadManager mgr = (DownloadManager)this.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        // save image in Kindertag folder
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Demo")
                .setDescription("Something useful. No, really.")
                .setDestinationInExternalPublicDir("/Kindertag", formattedDate+".jpg");

        mgr.enqueue(request);
    }
}


