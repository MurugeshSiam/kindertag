package com.kindertag.kindertag.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kindertag.kindertag.Adapter.StudentHorizAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.module.Item_Students;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class QRCodeActivity extends AppCompatActivity {

    public static ArrayList<String> arrayList_ids = new ArrayList<>();
    Context context = QRCodeActivity.this;
    ImageView img_qr;
    EditText edt_pin;
    Button btn_done;
    String QR_URL = "https://kindertag.in/admin/webapi/qrcode";
    String URL_PINCHECKIN = "https://kindertag.in/admin/webapi/pincheckin";
    String QR_Code = "";
    String roomid = "";
    ArrayList<Item_Students> arrayList = new ArrayList<>();
    Toolbar toolbar;
    String URL_CHECKOUT = "https://kindertag.in/admin/webapi/checkOut";
    String URL_CHECKIN = "https://kindertag.in/admin/webapi/attendance";
    String URL_ABSENT = "https://kindertag.in/admin/webapi/absent";
    String ids = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        // get roomid came from Home activity
        roomid = getIntent().getStringExtra("roomid");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Qr Code");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Qr Code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        img_qr = (ImageView) findViewById(R.id.img_qr);
        edt_pin = (EditText) findViewById(R.id.edt_pin);
        btn_done = (Button) findViewById(R.id.btn_done);

        // click on done button when enter pin
        // and call
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String pin = edt_pin.getText().toString().trim();

                if (pin.equalsIgnoreCase("")) {
                    edt_pin.setError("Enter PIN");
                } else {
                    getdata(edt_pin.getText().toString().trim());
                }
            }
        });

        getQrDetails();
    }

    // get QR details calling qrcode api

    private void getQrDetails() {
        Log.e("url", QR_URL);

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, QR_URL,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        Log.e("res", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            if (status.equalsIgnoreCase("1")) {
                                QR_Code = object.getString("qrcode");

                                // get Qr code url from response
                                // set url to image view using picasso
                                Picasso.with(context).load(QR_Code).into(img_qr);
                            } else {
                                Toast.makeText(context, "Error on getting qr code details", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("Error..", "Try Again");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        if (error instanceof NetworkError) {
                            Toast.makeText(context, "Please check the Internet Connection!", Toast.LENGTH_SHORT).show();

                        }else {
                            Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();

                        }
                        //TODO
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                // pass room id as parameter
                Log.e("room_id", roomid);

                map.put("room_id", roomid);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    // passing pin do check in of student
    private void getdata(final String pin) {
        Log.e("url", URL_PINCHECKIN);

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PINCHECKIN,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        arrayList.clear();
                        Log.e("res", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");

                            if (status.equalsIgnoreCase("1")) {
                                JSONArray jsonArray = object.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object1 = jsonArray.getJSONObject(i);
                                    Item_Students item_students = new Item_Students(
                                            object1.getString("student_id"),
                                            object1.getString("first_name"),
                                            object1.getString("photo"));

                                    arrayList.add(item_students);
                                }

                                scan_detailsDialog();
                            } else {
                                Toast.makeText(context, object.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e("Error..", "Try Again");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();

                        Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();
                        //TODO
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                map.put("pin", pin);
                map.put("room_id", roomid);

                Log.e("map", map.toString());
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

    // show dialog for student attendance
    // check in, check out and absent
    public void scan_detailsDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.z_scan_details);

        Button btn_checkin = (Button) dialog.findViewById(R.id.btn_checkin);
        Button btn_checkout = (Button) dialog.findViewById(R.id.btn_checkout);
        Button btn_absent = (Button) dialog.findViewById(R.id.btn_absent);
        RecyclerView recycler_students = (RecyclerView) dialog.findViewById(R.id.recycler_students);
        recycler_students.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        recycler_students.setHasFixedSize(true);
        recycler_students.setAdapter(new StudentHorizAdapter(context, arrayList));

        // btn check in click listener
        btn_checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arrayList_ids.size() > 0) {
                    dialog.dismiss();
                    sendAttendance(URL_CHECKIN);
                } else {
                    Toast.makeText(context, "Please select student", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // btn check out click listener

        btn_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arrayList_ids.size() > 0) {
                    dialog.dismiss();
                    sendAttendance(URL_CHECKOUT);
                } else {
                    Toast.makeText(context, "Please select student", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // btn absent click listener

        btn_absent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arrayList_ids.size() > 0) {
                    dialog.dismiss();
                    sendAttendance(URL_ABSENT);
                } else {
                    Toast.makeText(context, "Please select student", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }

    // CAll set attendance
    // Passing parameter room id and student id
    private void sendAttendance(String url) {
        Log.e("url", url);

            ids = "";
            for (int i = 0; i < arrayList_ids.size(); i++) {
                ids = ids + "," + arrayList_ids.get(i);
            }

            final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
            progressDialog.setCancelable(false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onResponse(String response) {
                            progressDialog.hide();
                            arrayList.clear();
                            Log.e("att res", response);
                            try {

                                JSONObject object = new JSONObject(response);
                                String status = object.getString("status");

                                if (status.equalsIgnoreCase("1")) {

                                    Toast.makeText(context,object.getString("msg")+ "", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                Log.e("Error..", "Try Again");
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.hide();

                            Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();
                            //TODO
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<String, String>();

                    map.put("student_ids", ids.substring(1));
                    map.put("room_id", roomid);

                    Log.e("map", map.toString());
                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

    }


    // action for back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
