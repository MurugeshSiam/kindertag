package com.kindertag.kindertag.Activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.KinderActivity;
import com.kindertag.kindertag.Utils.SharePref;
import com.loopj.android.http.RequestParams;
import android.provider.Settings.Secure;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends KinderActivity {
RelativeLayout hide_keyboard;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    TextView txt_logo, txt_mid_title, txt_botom_link, txt_type_welcome;
    LinearLayout ll_choice, ll_login, ll_type, ll_start, ll_start1, ll_start2, ll_start3, ll_fp, ll_fp1, ll_fp2, ll_fp3;
    TextInputLayout til_login_email, til_login_pass, til_start_fname, til_start_lname, til_start_email, til_start_mobile, til_start_pass, til_start_confirm, til_fp_mobile, til_fp_otp, til_fp_new, til_fp_confirm;
EditText input_email;
    final TextWatcher clear = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            clearErrors();
        }
    };

    Button btn_create, btn_signin, btn_login, btn_teacher, btn_parent, btn_start, btn_submit;
    String sel_type = "";
    String user_id = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.thene_blue_dark));
        }
        //Set Views
//        hide_keyboard = (RelativeLayout) findViewById(R.id.hide_keyboard);
//        hide_keyboard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//            }
//        });
        txt_logo = (TextView) findViewById(R.id.txt_company_logo);
        txt_mid_title = (TextView) findViewById(R.id.txt_mid_title);
        ll_choice = (LinearLayout) findViewById(R.id.ll_choice);
        btn_create = (Button) findViewById(R.id.btn_choice_create);
        btn_signin = (Button) findViewById(R.id.btn_choice_signin);
        ll_login = (LinearLayout) findViewById(R.id.ll_login);
        til_login_email = (TextInputLayout) findViewById(R.id.til_login_email);

        til_login_pass = (TextInputLayout) findViewById(R.id.til_login_password);
        txt_botom_link = (TextView) findViewById(R.id.txt_bottom_link);
        btn_login = (Button) findViewById(R.id.btn_login);
        ll_type = (LinearLayout) findViewById(R.id.ll_type);
        txt_type_welcome = (TextView) findViewById(R.id.txt_type_welcome);
        btn_teacher = (Button) findViewById(R.id.btn_type_teacher);
        btn_parent = (Button) findViewById(R.id.btn_type_parent);
        ll_start = (LinearLayout) findViewById(R.id.ll_start);
        ll_start1 = (LinearLayout) findViewById(R.id.ll_start_1);
        ll_start2 = (LinearLayout) findViewById(R.id.ll_start_2);
        ll_start3 = (LinearLayout) findViewById(R.id.ll_start_3);
        til_start_fname = (TextInputLayout) findViewById(R.id.til_start_firstname);
        til_start_lname = (TextInputLayout) findViewById(R.id.til_start_lastname);
        til_start_email = (TextInputLayout) findViewById(R.id.til_start_email);
        til_start_mobile = (TextInputLayout) findViewById(R.id.til_start_mobile);
        til_start_pass = (TextInputLayout) findViewById(R.id.til_start_password);
        til_start_confirm = (TextInputLayout) findViewById(R.id.til_start_confirm_password);
        btn_start = (Button) findViewById(R.id.btn_start);
        ll_fp = (LinearLayout) findViewById(R.id.ll_forgot_password);
        ll_fp1 = (LinearLayout) findViewById(R.id.ll_forgot_password_1);
        ll_fp2 = (LinearLayout) findViewById(R.id.ll_forgot_password_2);
        ll_fp3 = (LinearLayout) findViewById(R.id.ll_forgot_password_3);
        til_fp_mobile = (TextInputLayout) findViewById(R.id.til_forgot_password_mobile);
        til_fp_otp = (TextInputLayout) findViewById(R.id.til_forgot_password_otp);
        til_fp_new = (TextInputLayout) findViewById(R.id.til_forgot_password_new);
        til_fp_confirm = (TextInputLayout) findViewById(R.id.til_forgot_password_confirm);
        btn_submit = (Button) findViewById(R.id.btn_forgot_password_submit);

        //txt_logo.setText(Html.fromHtml("<b><i>Kinder Tag</i></b>"));
        txt_type_welcome.setText(Html.fromHtml("<big><b>Welcome!</b></big><br/>Which describes you?"));

        til_login_email.getEditText().addTextChangedListener(clear);
        til_login_pass.getEditText().addTextChangedListener(clear);
        til_start_fname.getEditText().addTextChangedListener(clear);
        til_start_lname.getEditText().addTextChangedListener(clear);
        til_start_email.getEditText().addTextChangedListener(clear);
        til_start_mobile.getEditText().addTextChangedListener(clear);
        til_start_pass.getEditText().addTextChangedListener(clear);
        til_start_confirm.getEditText().addTextChangedListener(clear);
        til_fp_mobile.getEditText().addTextChangedListener(clear);
        til_fp_otp.getEditText().addTextChangedListener(clear);
        til_fp_new.getEditText().addTextChangedListener(clear);
        til_fp_confirm.getEditText().addTextChangedListener(clear);

        //Set OnClick Listeners
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllText();
                ll_choice.setVisibility(View.GONE);
                txt_mid_title.setText("Lets get started!");
                txt_botom_link.setText("by creating an account, I agree to terms of service.");
                txt_mid_title.setVisibility(View.INVISIBLE);
                txt_botom_link.setVisibility(View.VISIBLE);
                ll_type.setVisibility(View.VISIBLE);
            }
        });

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllText();
                ll_choice.setVisibility(View.GONE);
                txt_mid_title.setText("Sign in to Continue");
                txt_botom_link.setText("Forgot Password?");
                txt_mid_title.setVisibility(View.VISIBLE);
                txt_botom_link.setVisibility(View.VISIBLE);
                ll_login.setVisibility(View.VISIBLE);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearErrors();
                if (Common.validate(til_login_email, 1, true) && Common.validate(til_login_pass, 1, true)) {
                    RequestParams params = new RequestParams();
                     String android_id = Secure.getString(getApplication().getContentResolver(),
                            Secure.ANDROID_ID);
/*
                    params.add("device_id", FirebaseInstanceId.getInstance().getToken());
*/
                    params.add("device_id", android_id);
                    params.add("uname", til_login_email.getEditText().getText().toString().toLowerCase());
                    params.add("pass", til_login_pass.getEditText().getText().toString());
                    params.add("device_type", "android");
//     Log.i("token",FirebaseInstanceId.getInstance().getToken());
                    Common.callWebService(LoginActivity.this, Common.wb_login, params, "Login");
                }
   /*sel_type=!sel_type.equals("")?sel_type:(til_login_email.getEditText().getText().toString().contains("p"))?"Parent":"Teacher";
   Intent intent =new Intent(LoginActivity.this,HomeActivity.class);
   intent.putExtra("type",sel_type);
   startActivity(intent);
   finish();*/
            }
        });

        txt_botom_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getVisibility() == View.VISIBLE && ((TextView) view).getText().toString().equalsIgnoreCase("Forgot Password?")) {
                    ll_login.setVisibility(View.GONE);
                    ll_fp.setVisibility(View.VISIBLE);
                    ll_fp1.setVisibility(View.VISIBLE);
                    txt_mid_title.setText("Forgot Password?");
                    txt_botom_link.setVisibility(View.INVISIBLE);
                }
            }
        });

        btn_teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllText();
                ll_type.setVisibility(View.GONE);
                sel_type = "Teacher";
                txt_mid_title.setVisibility(View.VISIBLE);
                ll_start.setVisibility(View.VISIBLE);
                ll_start1.setVisibility(View.VISIBLE);
            }
        });

        btn_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllText();
                ll_type.setVisibility(View.GONE);
                sel_type = "Parent";
                txt_mid_title.setVisibility(View.VISIBLE);
                ll_start.setVisibility(View.VISIBLE);
                ll_start1.setVisibility(View.VISIBLE);
            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearErrors();
                if (ll_start1.getVisibility() == View.VISIBLE) {
                    if (Common.validate(til_start_fname, 2, true) && Common.validate(til_start_lname, 2, true)) {
                        ll_start1.setVisibility(View.GONE);
                        ll_start3.setVisibility(View.VISIBLE);
                    }
                } else if (ll_start3.getVisibility() == View.VISIBLE) {
                    if (validatePass(til_start_pass, til_start_confirm)) {

                        ll_start3.setVisibility(View.GONE);
                        ll_start2.setVisibility(View.VISIBLE);
                    }
                } else if (ll_start2.getVisibility() == View.VISIBLE) {
                if (Common.validate(til_start_email, 6, true) && Common.validate(til_start_mobile, 10, true)) {

                        RequestParams params = new RequestParams();
                        params.add("namesurname", til_start_fname.getEditText().getText().toString() + " " + til_start_lname.getEditText().getText().toString());
                        params.add("email", til_start_email.getEditText().getText().toString().toLowerCase());
                        params.add("phone", til_start_mobile.getEditText().getText().toString());
                        params.add("pass", til_start_confirm.getEditText().getText().toString());
                        params.add("userType", sel_type.toLowerCase());
                        Log.i("sel_type", sel_type);
                        Common.callWebService(LoginActivity.this, Common.wb_register, params, "Register");
                    }
     /*Toast.makeText(LoginActivity.this,"Registration Successful!\nProcced to Login",Toast.LENGTH_SHORT).show();
     ll_start3.setVisibility(View.GONE);
     ll_start.setVisibility(View.GONE);
     btn_signin.performClick();*/
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearErrors();
                if (ll_fp1.getVisibility() == View.VISIBLE) {
                    if (Common.validate(til_fp_mobile, 10, true)) {
                        RequestParams params = new RequestParams();
                        params.add("mobile_no", til_fp_mobile.getEditText().getText().toString());
                        Common.callWebService(LoginActivity.this, Common.wb_sendotp, params, "SendOtp");
                        //ll_fp1.setVisibility(View.GONE);
                        //ll_fp2.setVisibility(View.VISIBLE);
                    }
                } else if (ll_fp2.getVisibility() == View.VISIBLE) {
                    if (Common.validate(til_fp_otp, 6, true)) {
                        //ll_fp2.setVisibility(View.GONE);
                        //ll_fp3.setVisibility(View.VISIBLE);
                        //}
                        RequestParams params = new RequestParams();
                        params.add("otp_code", til_fp_otp.getEditText().getText().toString());
                        Common.callWebService(LoginActivity.this, Common.wb_checkotp, params, "CheckOtp");
                    }
                } else if (ll_fp3.getVisibility() == View.VISIBLE) {
                    if (validatePass(til_fp_new, til_fp_confirm)) {
                        RequestParams params = new RequestParams();
                        params.add("user_id", user_id);
                        params.add("pass", til_fp_confirm.getEditText().getText().toString());
                        Common.callWebService(LoginActivity.this, Common.wb_resetPass, params, "ResetPass");
     /* Toast.makeText(LoginActivity.this,"Password Updated Successfully!",Toast.LENGTH_SHORT).show();
      clearAllText();
      ll_fp3.setVisibility(View.GONE);
      ll_fp.setVisibility(View.GONE);
      txt_mid_title.setText("Sign in to Continue");
      txt_mid_title.setVisibility(View.VISIBLE);
      txt_botom_link.setVisibility(View.VISIBLE);
      ll_login.setVisibility(View.VISIBLE);*/
                    }
     /*Toast.makeText(LoginActivity.this,"Registration Successful!\nProcced to Login",Toast.LENGTH_SHORT).show();
     ll_start3.setVisibility(View.GONE);
     ll_start.setVisibility(View.GONE);
     btn_signin.performClick();*/
                }
            }
        });

        if (!uid.equals("")) {
            startActivity(new Intent(LoginActivity.this, pref.getString(uid + "role", "").equalsIgnoreCase("Parent") ||
                    (pref.getString(uid + "role", "").equalsIgnoreCase("Teacher") && pref.getString(uid + "isInvite", "0").equals("0")) ? MyChildrenActivity.class : HomeActivity.class));
            finish();
        } else if (uid.equals("")) {
            ll_choice.setVisibility(View.VISIBLE);
            txt_mid_title.setVisibility(View.INVISIBLE);
            txt_botom_link.setVisibility(View.INVISIBLE);
            if (getIntent().hasExtra("isSignout") && getIntent().getBooleanExtra("isSignout", false))
                btn_signin.performClick();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
            }, 0);

    }

    @Override
    public void onBackPressed() {
        if (ll_fp.getVisibility() == View.VISIBLE) {
            if (ll_fp3.getVisibility() == View.VISIBLE) {
                clearErrors();
                ll_fp3.setVisibility(View.GONE);
                ll_fp2.setVisibility(View.VISIBLE);
            } else if (ll_fp2.getVisibility() == View.VISIBLE) {
                clearErrors();
                ll_fp2.setVisibility(View.GONE);
                ll_fp1.setVisibility(View.VISIBLE);
            } else if (ll_fp1.getVisibility() == View.VISIBLE) {
                clearAllText();
                ll_fp.setVisibility(View.GONE);
                txt_mid_title.setText("Sign in to Continue");
                txt_botom_link.setVisibility(View.VISIBLE);
                ll_login.setVisibility(View.VISIBLE);
            }
        } else if (ll_start.getVisibility() == View.VISIBLE) {
            if (ll_start3.getVisibility() == View.VISIBLE) {
                clearErrors();
                ll_start3.setVisibility(View.GONE);
                ll_start2.setVisibility(View.VISIBLE);
            } else if (ll_start2.getVisibility() == View.VISIBLE) {
                clearErrors();
                ll_start2.setVisibility(View.GONE);
                ll_start1.setVisibility(View.VISIBLE);
            } else if (ll_start1.getVisibility() == View.VISIBLE) {
                clearErrors();
                ll_start.setVisibility(View.GONE);
                txt_mid_title.setVisibility(View.INVISIBLE);
                ll_type.setVisibility(View.VISIBLE);
            }
        } else if (ll_type.getVisibility() == View.VISIBLE || ll_login.getVisibility() == View.VISIBLE) {
            clearErrors();
            ll_type.setVisibility(View.GONE);
            ll_login.setVisibility(View.GONE);
            txt_mid_title.setVisibility(View.INVISIBLE);
            txt_botom_link.setVisibility(View.INVISIBLE);
            ll_choice.setVisibility(View.VISIBLE);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Exit Application");
            builder.setMessage("Do you really want to Exit ?");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    finish();
                }
            });
            builder.create().show();
        }
    }

    public void clearErrors() {
        til_login_email.getEditText().setError(null);
        //til_login_email.setErrorEnabled(false);
        til_login_pass.getEditText().setError(null);
        //til_login_pass.setErrorEnabled(false);
        til_start_fname.getEditText().setError(null);
        //til_start_fname.setErrorEnabled(false);
        til_start_lname.getEditText().setError(null);
        //til_start_lname.setErrorEnabled(false);
        til_start_email.getEditText().setError(null);
        //til_start_email.setErrorEnabled(false);
        til_start_mobile.getEditText().setError(null);
        //til_start_mobile.setErrorEnabled(false);
        til_start_pass.getEditText().setError(null);
        //til_start_pass.setErrorEnabled(false);
        til_start_confirm.getEditText().setError(null);
        //til_start_confirm.setErrorEnabled(false);
        til_fp_mobile.getEditText().setError(null);
        //til_fp_mobile.setErrorEnabled(false);
        til_fp_otp.getEditText().setError(null);
        //til_fp_otp.setErrorEnabled(false);
        til_fp_new.getEditText().setError(null);
        //til_fp_new.setErrorEnabled(false);
        til_fp_confirm.getEditText().setError(null);
        //til_fp_confirm.setErrorEnabled(false);
    }

    public boolean validatePass(TextInputLayout til_pass, TextInputLayout til_confirm) {
        if (Common.validate(til_pass, 6, true) && Common.validate(til_confirm, 1, true)) {
            if (!til_pass.getEditText().getText().toString().equals(til_confirm.getEditText().getText().toString())) {
                //til_start_confirm.setErrorEnabled(true);
                til_confirm.getEditText().setError("Password & Confirm Password does not match");
                til_confirm.getEditText().requestFocus();
            } else
                return true;
        }
        return false;
    }

    public boolean validateOTP(TextInputLayout til_otp, String otp) {
        if (Common.validate(til_otp, 4, true)) {
            if (!til_otp.getEditText().getText().toString().equals(otp)) {
                //til_start_confirm.setErrorEnabled(true);
                til_otp.getEditText().setError("Invalid OTP");
                til_otp.getEditText().requestFocus();
            } else
                return true;
        }
        return false;
    }

    public void clearAllText() {
        til_login_email.getEditText().setText("");
        til_login_pass.getEditText().setText("");
        til_start_fname.getEditText().setText("");
        til_start_lname.getEditText().setText("");
        til_start_email.getEditText().setText("");
        til_start_mobile.getEditText().setText("");
        til_start_pass.getEditText().setText("");
        til_start_confirm.getEditText().setText("");
        til_fp_mobile.getEditText().setText("");
        til_fp_otp.getEditText().setText("");
        til_fp_new.getEditText().setText("");
        til_fp_confirm.getEditText().setText("");
    }
    private  boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String otp = intent.getStringExtra("otp");

                final String message = intent.getStringExtra("message");
                String j = "" ;

                Number(message);
                j =  Number(message);

                til_fp_otp.getEditText().setText(j);

            }
        }
    };
    private String Number(String messages) {


        String otp = "";
        try{
            for (int i = 0; i < messages.length(); i++) {

                String name = messages;
                Character character = name.charAt(i);

                int ascii = (int) character;
                if (ascii >= 48 && ascii <= 57) {
                    otp = otp + character;
                }
            }
        }
        catch (Exception e)
        {

        }
        return otp;

    }
    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
    @Override
    public void onSuccess(String response, String tag) {
        super.onSuccess(response, tag);
        if (response != null && response.length() > 0 && tag.equalsIgnoreCase("Register")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);


                     if (jobj.getInt("status") > 0) {
                    clearAllText();
                    //Toast.makeText(LoginActivity.this, "Registration Successful!\nProcced to Login", Toast.LENGTH_SHORT).show();
                    Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    ll_start3.setVisibility(View.GONE);
                    ll_start.setVisibility(View.GONE);
                    btn_signin.performClick();
                } else if (jobj.getInt("status") == 0) {
                  //  Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    if (jobj.getString("msg").contains("already register")) {
                       // onBackPressed();
                        til_start_email.getEditText().setError("Email already Registered");
                        til_start_email.getEditText().requestFocus();
                    }
                }
            } catch (JSONException e) {
                Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("Login")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
if(jobj.getInt("status")==3){
    Toast.makeText(LoginActivity.this,jobj.getString("msg"), Toast.LENGTH_SHORT).show();

}
              else   if (jobj.getInt("status")==4) {
                    //  Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    Toast.makeText(LoginActivity.this,"Super Admin only for Web", Toast.LENGTH_SHORT).show();

                }
              else  if (jobj.getInt("status") > 0) {
    JSONObject data = new JSONObject(jobj.getString("data"));

    Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    pref.edit().putString("user_id", Common.chkNull(data, "id", "")).commit();
                    uid = pref.getString("user_id", "");
                    pref.edit().putString(uid + "image_url", Common.chkNull(data, "image_url", "")).commit();
                    pref.edit().putString(uid + "name", Common.chkNull(data, "name", "")).commit();
                    pref.edit().putString(uid + "email", Common.chkNull(data, "email", "")).commit();
                    pref.edit().putString(uid + "mobile", Common.chkNull(data, "phone", "")).commit();

                    SharePref.setfirstlogin(LoginActivity.this, Common.chkNull(data, "signined", ""));
                    //String room_id=+";name";
                    sel_type = Common.chkNull(data, "role_id", "").equals("4") ? "Parent" : Common.chkNull(data, "role_id", "").equals("3") ? "Teacher" : Common.chkNull(data, "role_id", "").equals("2") ? "Owner" : "";
                    pref.edit().putString(uid + "role", sel_type).commit();
                    //sel_type=!sel_type.equals("")?sel_type:(til_login_email.getEditText().getText().toString().contains("p"))?"Parent":"Teacher";
                    if (!sel_type.equalsIgnoreCase("Owner")) {
                        pref.edit().putString(uid + "room_id", Common.chkNull(data, "room_id", "")).commit();
                        pref.edit().putString(uid + "room_name", Common.chkNull(data, "room_name", "")).commit();
                        pref.edit().putString(uid + "isInvite", Common.chkNull(data, "invite_code", "0")).commit();
                    }

                    if (sel_type.equalsIgnoreCase("Parent")) {
                        SharePref.setParentPin(LoginActivity.this, Common.chkNull(data, "pin", ""));
                    }

                    pref.edit().putString(uid + "newLogin", Common.chkNull(data, "signined", "")).commit();
                    startActivity(new Intent(LoginActivity.this, (sel_type.equalsIgnoreCase("Parent") || (sel_type.equalsIgnoreCase("Teacher") && Common.chkNull(data, "invite_code", "0").equalsIgnoreCase("0"))) ? MyChildrenActivity.class : HomeActivity.class));
                    finish();
                } else if (jobj.getInt("status") == 0) {
                    Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                    //til_login_email.setErrorEnabled(true);til_login_pass.setErrorEnabled(true);
                    til_login_email.getEditText().setError("Invalid username or password!");
                    til_login_pass.getEditText().setError("Invalid username or password!");
                }
            } catch (JSONException e) {
                til_login_email.getEditText().setError("Invalid username or password!");
                til_login_pass.getEditText().setError("Invalid username or password!");
                //Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("SendOtp")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    user_id = Common.chkNull(jobj, "user_id", "");
                    ll_fp1.setVisibility(View.GONE);
                    ll_fp2.setVisibility(View.VISIBLE);
                    /*Auto Detect OTP-SIAM ---START*/
                    if (checkAndRequestPermissions()) {
                        // carry on the normal flow, as the case of  permissions  granted.
                    }
                    /*Auto Detect OTP-SIAM ---END*/

                } else if (jobj.getInt("status") == 0) {
                    Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("CheckOtp")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    ll_fp2.setVisibility(View.GONE);
                    ll_fp3.setVisibility(View.VISIBLE);
                } else if (jobj.getInt("status") == 0) {
                    Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("ResetPass")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    Toast.makeText(LoginActivity.this, "Password Updated Successfully!", Toast.LENGTH_SHORT).show();
                    clearAllText();
                    ll_fp3.setVisibility(View.GONE);
                    ll_fp.setVisibility(View.GONE);
                    txt_mid_title.setText("Sign in to Continue");
                    txt_mid_title.setVisibility(View.VISIBLE);
                    txt_botom_link.setVisibility(View.VISIBLE);
                    ll_login.setVisibility(View.VISIBLE);
                } else if (jobj.getInt("status") == 0) {
                    Toast.makeText(LoginActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}
