package com.kindertag.kindertag.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.kindertag.kindertag.Adapter.StudentHorizAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.module.Item_Students;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    public static String systemCode;
    public static ArrayList<String> arrayList_ids = new ArrayList<>();
    Context context = ScannerActivity.this;
    String URL = "https://kindertag.in/admin/webapi/qrcheckin";
    String URL_CHECKOUT = "https://kindertag.in/admin/webapi/checkOut";
    String URL_CHECKIN = "https://kindertag.in/admin/webapi/attendance";
    String URL_ABSENT = "https://kindertag.in/admin/webapi/absent";
    String room_id, user_id;
    ArrayList<Item_Students> arrayList = new ArrayList<>();
    String ids = "";
    private ZXingScannerView scannerView;

    @Override
    protected void onStop() {
        super.onStop();
        // stop camera preview
        // stop camera
        scannerView.stopCamera();
        scannerView.stopCameraPreview();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        // find views
        scannerView = (ZXingScannerView) findViewById(R.id.scanner);

        // receive roomid and userid came from Home activity.
        room_id = getIntent().getStringExtra("room_id");
        user_id = getIntent().getStringExtra("user_id");
    }

    @Override
    public void onResume() {
        super.onResume();

        // initilize camera for scan qr code
        scannerView.startCamera();
        scannerView.setFocusable(true);
        scannerView.setFormats(getBarcodeFormats());
        scannerView.setResultHandler(ScannerActivity.this);
    }


    @Override
    public void handleResult(Result result) {

        // when scan barcode result got here
        // set result to systemCode
        Log.d("########", "Scan Result : " + result.getText());
        Toast.makeText(this, "" + result.getText(), Toast.LENGTH_SHORT).show();
        systemCode = result.getText();

        scannerView.setVisibility(View.GONE);
        getdata();

    }

    // set barcode reading formats
    private List<BarcodeFormat> getBarcodeFormats() {
        List<BarcodeFormat> barcodeFormatList = new ArrayList<>();
        barcodeFormatList.add(BarcodeFormat.UPC_A);
        barcodeFormatList.add(BarcodeFormat.CODABAR);
        barcodeFormatList.add(BarcodeFormat.QR_CODE);
        barcodeFormatList.add(BarcodeFormat.EAN_13);
        barcodeFormatList.add(BarcodeFormat.PDF_417);
        barcodeFormatList.add(BarcodeFormat.CODE_93);
        barcodeFormatList.add(BarcodeFormat.CODE_128);
        barcodeFormatList.add(BarcodeFormat.RSS_14);
        barcodeFormatList.add(BarcodeFormat.UPC_E);
        barcodeFormatList.add(BarcodeFormat.DATA_MATRIX);
        barcodeFormatList.add(BarcodeFormat.ITF);
        barcodeFormatList.add(BarcodeFormat.CODE_39);
        barcodeFormatList.add(BarcodeFormat.EAN_8);
        return barcodeFormatList;
    }

    // back button of mobile action
    @Override
    public void onBackPressed() {
        finish();
    }

    // using scan result get all student
    private void getdata() {
        Log.e("url", URL);

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        arrayList.clear();
                        arrayList_ids.clear();
                        Log.e("res", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");

                            if (status.equalsIgnoreCase("1")) {
                                JSONArray jsonArray = object.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object1 = jsonArray.getJSONObject(i);
                                    Item_Students item_students = new Item_Students(
                                            object1.getString("student_id"),
                                            object1.getString("first_name"),
                                            object1.getString("photo"));

                                    // add all students in arraylist
                                    arrayList.add(item_students);
                                }

                                scan_detailsDialog();
                            } else {
                                Toast.makeText(context, "Data not found.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e("Error..", "Try Again");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();

                        Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();
                        //TODO
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                map.put("user_id", user_id);
                map.put("room_id", room_id);

                Log.e("map", map.toString());
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }


    // show student dialog
    // can select student from list and
    // perform attendance like chekc in, check out and absent
    public void scan_detailsDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.z_scan_details);

        Button btn_checkin = (Button) dialog.findViewById(R.id.btn_checkin);
        Button btn_checkout = (Button) dialog.findViewById(R.id.btn_checkout);
        Button btn_absent = (Button) dialog.findViewById(R.id.btn_absent);
        RecyclerView recycler_students = (RecyclerView) dialog.findViewById(R.id.recycler_students);
        recycler_students.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        recycler_students.setHasFixedSize(true);
        recycler_students.setAdapter(new StudentHorizAdapter(context, arrayList));

        // check in students
        btn_checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arrayList_ids.size() > 0) {
                    dialog.dismiss();
                    sendAttendance(URL_CHECKIN);
                } else {
                    Toast.makeText(context, "Please select student", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // check out students

        btn_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arrayList_ids.size() > 0) {
                    dialog.dismiss();
                    sendAttendance(URL_CHECKOUT);
                } else {
                    Toast.makeText(context, "Please select student", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // absent students

        btn_absent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arrayList_ids.size() > 0) {
                    dialog.dismiss();
                    sendAttendance(URL_ABSENT);
                } else {
                    Toast.makeText(context, "Please select student", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }


    // call attendance url checkin, check out or absent
    // by passing parameter student ids and room id
    private void sendAttendance(String url) {
        Log.e("url", url);

        ids = "";
        for (int i = 0; i < arrayList_ids.size(); i++) {
            ids = ids + "," + arrayList_ids.get(i);
        }

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        arrayList.clear();
                        Log.e("att res", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");

                            if (status.equalsIgnoreCase("1")) {

                                Toast.makeText(context, object.getString("msg") + "", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e("Error..", "Try Again");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();

                        Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();
                        //TODO
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                map.put("student_ids", ids.substring(1));
                map.put("room_id", room_id);

                Log.e("map", map.toString());
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

}
