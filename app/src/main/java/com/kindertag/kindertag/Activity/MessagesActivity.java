package com.kindertag.kindertag.Activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kindertag.kindertag.Adapter.MessagesListAdapter;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.KinderActivity;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

public class MessagesActivity extends KinderActivity {
    ListView list_messages;
    EditText message;
    TextView send;
    Toolbar toolbar;
    String vals;
    String oid;
    String std_id = "";
    String sid;
    String pos="";

    ArrayList<String> messages = new ArrayList<String>();
    public static boolean isyes = false;
    Bitmap b;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        // hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        isyes = true;
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.thene_blue_dark));
        }

        // receives values comes from home activity.
        // name, student id and position.
        vals = getIntent().getExtras().getString("vals", "");
        sid = getIntent().getExtras().getString("sid", "");
        pos = getIntent().getExtras().getString("pos", "");

        Log.e("vals", vals);
        Log.e("std",sid);
        oid = vals.split(";")[0];
        String[] arrid = vals.split(";");
        if(sid.equalsIgnoreCase("")) {
            std_id = arrid[arrid.length - 1];
        }else{
            String[] selectStd = sid.split(";");
            std_id = selectStd[0];
        }
        Log.i("oid", oid);
        Log.i("std_id", std_id);

        if (!vals.contains(";"))
        {
            oid= pos;
        }

        Log.i("oid", oid);

        if (!pref.getString(uid + oid + "messages", "").equals(""))
            messages = Common.toList(pref.getString(uid + oid + "messages", ""));

        // if image url contains http then get image from that and
        // set image to toolbar back button.

        if (vals.contains("https")) {
            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        URL url = null;
                        try {
                            url = new URL(vals.split(";")[4]);
                            Log.e("url", url + "");
                            try {
                                b = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        RoundedBitmapDrawable rbd = RoundedBitmapDrawableFactory.create(getResources(), Bitmap.createScaledBitmap(b, 90, 90, true));
                                        rbd.setCircular(true);
                                        getSupportActionBar().setIcon(rbd);
                                    }
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.nav_menu_header_bg);
            RoundedBitmapDrawable rbd = RoundedBitmapDrawableFactory.create(getResources(), Bitmap.createScaledBitmap(b, 90, 90, true));
            rbd.setCircular(true);
            getSupportActionBar().setIcon(rbd);
        }

        // visible back buttons
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        if (vals.contains(";"))
        getSupportActionBar().setTitle("   " + vals.split(";")[1]);
        else
        getSupportActionBar().setTitle("   "+vals);

        // find views
        list_messages = (ListView) findViewById(R.id.list_messages);
        message = (EditText) findViewById(R.id.edit_message);
        send = (TextView) findViewById(R.id.btn_message_send);
        send.setTypeface(tf_material);
        send.setText(R.string.icon_send);

        //Set ListAdapter
        Collections.reverse(messages);
        list_messages.setAdapter(new MessagesListAdapter(MessagesActivity.this, messages));
        list_messages.setSelection(list_messages.getAdapter().getCount() - 1);
        //Set OnClick Listeners on send button
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // call send message web service
                // with passing parameters sender id, receiver id, message and student_id.
                message.setSelection(message.getText().length());
                if (Common.validate(message, 1, true)) {
                    RequestParams params = new RequestParams();
                    params.add("sender_id", uid);
                    params.add("receiver_id", oid);
                    params.add("messageText", message.getText().toString());
                    params.add("student_id", std_id);
                    Common.callWebService(MessagesActivity.this, Common.wb_sendmessage, params, "SendMessage" + "%" + message.getText().toString());
                }
                message.setSelection(message.getText().length());
            }
        });

        // get all message of perticular student
        // by sending student id
        RequestParams params = new RequestParams();
        params.add("sender_id", uid);
        params.add("receiver_id", oid);
        params.add("student_id", std_id);
        Log.e("message param",params.toString());

        Common.callWebService(MessagesActivity.this, Common.wb_getmessage, params, "GetMessage");

    //   add TextChangedListener on message edittext
        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                list_messages.setSelection(messages.size());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                list_messages.setSelection(messages.size());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // when sending message and
    @Override
    public void onSuccess(String response, String tag) {
        super.onSuccess(response, tag);

        //here response when send message
        if (response != null && response.length() > 0 && tag.split("%")[0].equalsIgnoreCase("SendMessage")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    message.setText("");
                    messages.add(Common.chkNull(jobj, "message_id", "") + ";" + tag.split("%")[1] + ";" +
                            new SimpleDateFormat("dd MMMM").format(new Date(System.currentTimeMillis())) + ";" + 1 + ";" + true+ ";" +""+ ";"+ new SimpleDateFormat("HH:mm a").format(new Date(System.currentTimeMillis())) );
//                    pref.edit().putString(uid + oid + "messages", Common.toString(messages)).commit();
                    list_messages.setAdapter(new MessagesListAdapter(MessagesActivity.this, messages));
                    list_messages.setSelection(list_messages.getAdapter().getCount() - 1);
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(MessagesActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(MessagesActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // here response when get all messages of particular student
        } else if (response != null && response.length() > 0 && tag.equalsIgnoreCase("GetMessage")) {
            Log.i("response", response);
            try {
                JSONObject jobj = new JSONObject(response);
                if (jobj.getInt("status") > 0) {
                    JSONArray jarray = new JSONArray(Common.chkNull(jobj, "data", ""));
                    for (int i = 0; i < jarray.length(); i++) {
                        boolean isexist = false;
                        for (int j = 0; j < messages.size(); j++)
                            if (messages.get(j).split(";")[0].equalsIgnoreCase(Common.chkNull(jarray.getJSONObject(i), "message_id", ""))) {
                                isexist = true;
                                break;
                            }
                        if (!isexist) {
                            messages.add(Common.chkNull(jarray.getJSONObject(i), "message_id", "") + ";" + Common.chkNull(jarray.getJSONObject(i), "message_text", "") + ";" +
                                    Common.chkNull(jarray.getJSONObject(i), "message_date", "") + ";" + Common.chkNull(jarray.getJSONObject(i), "status", "") + ";" + Common.chkNull(jarray.getJSONObject(i), "sender_id", "").equals(uid)+ ";" + Common.chkNull(jarray.getJSONObject(i), "message_time", ""));

                        }
                    }
                    Collections.reverse(messages);
                    list_messages.setAdapter(new MessagesListAdapter(MessagesActivity.this, messages));
                    list_messages.setSelection(list_messages.getAdapter().getCount() - 1);
                } else if (jobj.getInt("status") == 0)
                    Toast.makeText(MessagesActivity.this, jobj.getString("msg"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(MessagesActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}
