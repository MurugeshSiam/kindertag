package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kindertag.kindertag.Activity.QRCodeActivity;
import com.kindertag.kindertag.Activity.ScannerActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.module.Item_Students;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class StudentHorizAdapter extends RecyclerView.Adapter<StudentHorizAdapter.MyViewHolder> {

    ArrayList<Item_Students> arrayList = new ArrayList<>();
    Context context;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter is used to show child in horizontally when scan qr code
    // check student and and set check in check out and absent
    public StudentHorizAdapter(Context context, ArrayList<Item_Students> arr_name) {

        this.arrayList = arr_name;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.z_items_hori_stdlist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final Item_Students item_students = arrayList.get(position);
        holder.txt_std_name.setText(item_students.getStd_name());
        Picasso.with(context).load(item_students.getStd_image()).into(holder.img_std);

        // student showing in horizontaly
        // when click on student image checked student set unchecked
        // and unchecked student set checked.

        holder.img_std.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.img_std_sele.getVisibility() == View.VISIBLE) {
                    ScannerActivity.arrayList_ids.remove(item_students.getStd_id());
                    QRCodeActivity.arrayList_ids.remove(item_students.getStd_id());
                    holder.img_std_sele.setVisibility(View.GONE);
                    holder.img_std_sele1.setVisibility(View.GONE);
                } else {

                    // add id into arrayList_ids when select student.

                    if (!ScannerActivity.arrayList_ids.contains(item_students.getStd_id()))
                        ScannerActivity.arrayList_ids.add(item_students.getStd_id());
                    if (!QRCodeActivity.arrayList_ids.contains(item_students.getStd_id()))
                        QRCodeActivity.arrayList_ids.add(item_students.getStd_id());

                    holder.img_std_sele.setVisibility(View.VISIBLE);
                    holder.img_std_sele1.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_std_name;
        ImageView img_std, img_std_sele, img_std_sele1;

        public MyViewHolder(View view) {
            super(view);

            // find out ids are declared in z_items_hori_stdlist
            img_std = (ImageView) view.findViewById(R.id.img_std);
            img_std_sele = (ImageView) view.findViewById(R.id.img_std_sele);
            img_std_sele1 = (ImageView) view.findViewById(R.id.img_std_sele1);
            txt_std_name = (TextView) view.findViewById(R.id.txt_std_name);
        }
    }
}
