package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bhupen on 1/5/17.
 */


public class HomeStudentListAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> vals = new ArrayList<String>();
    ArrayList<String> search_vals = new ArrayList<String>();
    String search = "";
    Typeface tf_awsome, tf_material;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show students in selected room.
    //
    public HomeStudentListAdapter(Context context, ArrayList<String> vals, String search, GridView list, TextView empty) {
        this.context = context;
        this.vals = vals;
        this.search = search;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        Collections.sort(this.vals, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].compareTo(t1.split(";")[1]);
            }
        });
        if (vals.size() > 0 && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                    search_vals.add(vals.get(i));
        }
        list.setVisibility(vals.size() > 0 && (search.length() == 0 || (search.length() > 0 && search_vals.size() > 0)) ? View.VISIBLE : View.GONE);
        empty.setText("No Student is Added in the Room" + ((vals.size() > 0 && search.length() > 0 && search_vals.size() == 0) ? "\nas per search criteria" : ""));
        empty.setVisibility((vals.size() == 0 || (search.length() > 0 && search_vals.size() == 0)) ? View.VISIBLE : View.GONE);


        ((HomeActivity) context).ll_update_room.setVisibility(View.GONE);
    }

    @Override
    public int getCount() {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.size() : vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.get(pos) : vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to views come from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_list_students, null);
        final ImageView img_student = (ImageView) convertView.findViewById(R.id.img_home_list_student);
        final ImageView img_status = (ImageView) convertView.findViewById(R.id.img_home_list_student_status);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_home_list_student_name);

        final TextView txt_img = (TextView) convertView.findViewById(R.id.txt_img);

        Log.e("data_list", getItem(pos).toString());
        if (!getItem(pos).toString().contains("http")) {
            txt_img.setVisibility(View.VISIBLE);
            String[] arr_name = getItem(pos).toString().split(";");
            String first_let = "" + arr_name[1].charAt(0);
            txt_img.setText(first_let);
        }

        // set name to name textview
        name.setText(getItem(pos).toString().split(";")[1]);

        // set imageurl to imageview
        if (!getItem(pos).toString().split(";")[3].equals("")) {
            Glide.with(context).load(getItem(pos).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_student) {
                @Override
                protected void setResource(Bitmap resource) {
                    if (resource != null && img_student != null) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img_student.setImageDrawable(circularBitmapDrawable);
                    }
                }
            });
        }
        img_status.setImageResource(Boolean.parseBoolean(getItem(pos).toString().split(";")[4]) ? android.R.drawable.presence_online : android.R.drawable.presence_invisible);
        return convertView;
    }
}
