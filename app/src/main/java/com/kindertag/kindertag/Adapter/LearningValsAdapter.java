package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindertag.kindertag.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bhupen on 1/5/17.
 */


public class LearningValsAdapter extends BaseAdapter {

    ArrayList<String> vals;
    Context context;
    String sel = "";
    String search = "";
    ArrayList<String> search_vals = new ArrayList<String>();
    Typeface tf_awesome, tf_material;
    boolean isCat = false;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show learning values it showing in learning fragment when add Learning activity.
    //
    public LearningValsAdapter(Context context, String search, boolean isCat, ArrayList<String> vals, String sel) {
        this.context = context;
        this.search = search;
        this.vals = vals;
        this.sel = sel;
        this.isCat = isCat;
        tf_awesome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        if (isCat) {
            Collections.sort(this.vals, new Comparator<String>() {
                @Override
                public int compare(String s, String t1) {
                    return s.split(";")[1].toLowerCase().compareTo(t1.split(";")[1].toLowerCase());
                }
            });
        }
        if (isCat && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                    search_vals.add(vals.get(i));
        }
    }

    @Override
    public int getCount() {
        return (isCat && search.length() > 0) ? search_vals.size() : vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return (isCat && search.length() > 0) ? (search_vals.get(pos).contains(";") ? search_vals.get(pos).split(";")[1] : search_vals.get(pos)) : vals.get(pos).contains(";") ? vals.get(pos).split(";")[1] : vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to views come from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_learning_vals, null);
        final TextView chk = (TextView) convertView.findViewById(R.id.txt_learning_vals_chk);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_learning_vals_name);
        chk.setTypeface(tf_material);
        chk.setText(R.string.icon_chk);
        name.setText(getItem(pos).toString());
        chk.setTextColor(context.getResources().getColor(sel.contains(getItem(pos).toString()) ? R.color.thene_blue : R.color.tab_txt));
        name.setTextColor(context.getResources().getColor(sel.contains(getItem(pos).toString()) ? R.color.black : R.color.tab_txt));
        return convertView;
    }
}
