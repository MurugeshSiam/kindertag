package com.kindertag.kindertag.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.kindertag.kindertag.Utils.SharePref;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class HomeEventListAdapter extends BaseAdapter {

    // Declaration of variables
    String URl = "https://kindertag.in/admin/webapi/deleteEvent";
    HomeActivity context;
    ArrayList<String> vals = new ArrayList<String>();
    String user_type = "Teacher";
    ArrayList<String> search_vals = new ArrayList<String>();
    String search = "";
    Typeface tf_awsome, tf_material;
    int exp_pos = -1;
    GridView list;

    //  write constructor to call from activity
    // and receive all data data came from activity
    // using this adapter show even list in events tab

    public HomeEventListAdapter(HomeActivity context, ArrayList<String> vals, String user_type, int exp_pos, String search, GridView list, TextView empty) {
        this.context = context;
        this.vals = vals;
        this.user_type = user_type;
        this.search = search;
        this.exp_pos = user_type.equalsIgnoreCase("Parent") ? -1 : exp_pos;
        this.list = list;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        Collections.sort(this.vals, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                try {
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    return sf.parse(s.split(";")[3]).compareTo(sf.parse(t1.split(";")[3]));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        if (vals.size() > 0 && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase())){
                    search_vals.add(vals.get(i));

                }else if(vals.get(i).split(";")[2].toLowerCase().contains(search.toLowerCase()))
                {
                    search_vals.add(vals.get(i));

                }
        }
        list.setVisibility(vals.size() > 0 ? View.VISIBLE : View.GONE);
        empty.setText("No Recent Events");
        empty.setVisibility(vals.size() == 0 ? View.VISIBLE : View.GONE);
        if (vals.size() > 0) {
            Collections.sort(vals, new Comparator<String>() {
                @Override
                public int compare(String s, String t1) {
                    try {
                        Date d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(s.split(";")[3]);
                        Date d2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(t1.split(";")[3]);
                        return d1.compareTo(d2);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
            });
        }
    }

    @Override
    public int getCount() {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.size() : vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.get(pos) : vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to textview from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_list_event, null);
        final View vv = convertView;
        final ImageView img = (ImageView) convertView.findViewById(R.id.img_home_list_event);
        final TextView title = (TextView) convertView.findViewById(R.id.txt_home_list_event_title);
        final TextView desc = (TextView) convertView.findViewById(R.id.txt_home_list_event_desc);
        final TextView date = (TextView) convertView.findViewById(R.id.txt_home_list_event_date);
        final RelativeLayout ll_expanded = (RelativeLayout) convertView.findViewById(R.id.ll_home_list_event_expanded);
        final LinearLayout ll_accpted = (LinearLayout) convertView.findViewById(R.id.ll_home_list_event_accepted);
        final LinearLayout line_main = (LinearLayout) convertView.findViewById(R.id.line_main);
        final LinearLayout ll_declined = (LinearLayout) convertView.findViewById(R.id.ll_home_list_event_declined);
        final TextView sym = (TextView) convertView.findViewById(R.id.txt_home_list_event_sym);
        final TextView status = (TextView) convertView.findViewById(R.id.txt_home_list_event_status);

        final ImageButton img_delelet_event = (ImageButton) convertView.findViewById(R.id.img_delelet_event);

        title.setText(getItem(pos).toString().split(";")[1]);
        desc.setText(getItem(pos).toString().split(";")[2]);
        date.setText(getItem(pos).toString().split(";")[3]);

        final TextView txt_img = (TextView) convertView.findViewById(R.id.txt_img);

        // can delete any event
        // click listener on delete button
      /*  ll_accpted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_expanded.setVisibility(View.GONE);
            }
        });*/
        img_delelet_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteEvent(getItem(pos).toString().split(";")[0], pos);
            }
        });

        // if image contains in event list then visible imageiew
        // and set image to imageview
        Log.e("data_list", getItem(pos).toString());
        if (!getItem(pos).toString().contains("http")) {
            txt_img.setVisibility(View.VISIBLE);
            String[] arr_name = getItem(pos).toString().split(";");
            String first_let = "" + arr_name[1].charAt(0);
            txt_img.setText(first_let);
        }

        ll_accpted.getChildAt(0).setBackgroundResource(user_type.equalsIgnoreCase("Parent") ? R.drawable.black_circle_green : R.drawable.black_circle_back);
        ((TextView) ll_accpted.getChildAt(0)).setTypeface(user_type.equalsIgnoreCase("Parent") ? tf_material : Typeface.DEFAULT);
        ((TextView) ll_accpted.getChildAt(0)).setText(user_type.equalsIgnoreCase("Parent") ? context.getResources().getString(R.string.icon_tick) : getItem(pos).toString().split(";")[4]);
        ((TextView) ll_accpted.getChildAt(1)).setText(user_type.equalsIgnoreCase("Parent") ? "Accept" : "Accepted");
        ll_declined.getChildAt(0).setBackgroundResource(user_type.equalsIgnoreCase("Parent") ? R.drawable.black_circle_red : R.drawable.black_circle_back);
        ((TextView) ll_declined.getChildAt(0)).setTypeface(user_type.equalsIgnoreCase("Parent") ? tf_material : Typeface.DEFAULT);
        ((TextView) ll_declined.getChildAt(0)).setText(user_type.equalsIgnoreCase("Parent") ? context.getResources().getString(R.string.icon_close) : getItem(pos).toString().split(";")[5]);
        ((TextView) ll_declined.getChildAt(1)).setText(user_type.equalsIgnoreCase("Parent") ? "Decline" : "Declined");

        // from here user can accept and decline the events
        // when user select accept or decline then call webservice update event
        View.OnClickListener event_click = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_expanded.setVisibility(View.GONE);

                if (user_type.equalsIgnoreCase("Parent")  && status.getVisibility() == View.GONE) {
                    String student_id = SharePref.getstdid(context);
                    Log.e("stdid", student_id);
                    RequestParams params = new RequestParams();
                    params.add("event_id", getItem(pos).toString().split(";")[0]);
                    params.add("user_id", context.uid);
                    params.add("student_id", student_id);
                    params.add("status", view.getTag().toString().toLowerCase().replace("accepted", "accept").replace("declined", "decline"));
                    Common.callWebService(context, Common.wb_updateevent, params, "UpdateEvent");

                } else if(ll_expanded.getVisibility()==View.VISIBLE)

                {
                    ll_expanded.setVisibility(View.GONE);

                    list.performItemClick(vv, pos, getItemId(pos));
                }

            }
        };

        // set click listener on accept and decline button
        ll_accpted.setOnClickListener(event_click);
        ll_declined.setOnClickListener(event_click);

        line_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!user_type.equalsIgnoreCase("Parent")) {
                    if (ll_expanded.getVisibility() == View.GONE) {
                        ll_expanded.setVisibility(View.VISIBLE);
                        sym.setText(R.string.icon_up);

                    } else {
                        ll_expanded.setVisibility(View.GONE);
                        sym.setText(R.string.icon_down);

                    }
                }
            }
        });

        sym.setTypeface(tf_material);
        sym.setText(exp_pos == pos ? R.string.icon_up : R.string.icon_down);
        sym.setVisibility(!user_type.equalsIgnoreCase("Parent") ? View.VISIBLE : View.GONE);
        ll_expanded.setVisibility((exp_pos == pos || user_type.equalsIgnoreCase("Parent")) ? View.VISIBLE : View.GONE);

        if (user_type.equalsIgnoreCase("Parent")) {
            img_delelet_event.setVisibility(View.GONE);
        }

        if (user_type.equalsIgnoreCase("Parent") && !getItem(pos).toString().split(";")[6].equalsIgnoreCase("null")) {
            Log.i("event_status", getItem(pos).toString().split(";")[6]);
            ll_expanded.setVisibility(View.GONE);
            status.setTextColor(context.getResources().getColor(getItem(pos).toString().split(";")[6].startsWith("a") ? android.R.color.holo_green_dark : android.R.color.holo_red_dark));
            status.setText("Event " + getItem(pos).toString().split(";")[6] + (getItem(pos).toString().split(";")[6].endsWith("e") ? "d" : "ed"));
            status.setVisibility(View.VISIBLE);
        }

        return convertView;
    }


    // call delete event webservice
    private void deleteEvent(final String event_id, final int position) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, null, "Please wait...", false, false);
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URl,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        Log.e("res", response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            if (status.equalsIgnoreCase("1")) {
                                Toast.makeText(context, object.getString("msg") + "", Toast.LENGTH_SHORT).show();
                                vals.remove(position);
                                notifyDataSetChanged();
                            } else {
                                Toast.makeText(context, object.getString("msg") + "", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("Error..", "Try Again");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();

                        Toast.makeText(context, "Check your Server Side ", Toast.LENGTH_SHORT).show();
                        //TODO
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                // pass event_id parameter to delete particular event
                map.put("event_id", event_id);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }
}
