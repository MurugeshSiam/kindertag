package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Activity.MessagesActivity;
import com.kindertag.kindertag.R;

import java.util.ArrayList;


public class HomeNewActivtyStudentsGridAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> students = new ArrayList<String>();
    ArrayList<String> sel_students = new ArrayList<String>();
    ArrayList<String> search_vals = new ArrayList<String>();
    String search = "";
    Typeface tf_awsome, tf_material;
    String Checkin;
    Boolean Status;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show all students of particular room.
    // on gridview

    public HomeNewActivtyStudentsGridAdapter(Context context, ArrayList<String> students, ArrayList<String> sel_students, String search,Boolean status) {
        this.context = context;
        this.students = students;
        this.sel_students = sel_students;
        this.search = search;
        this.Status = status;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        if (students.size() > 0 && search.length() > 0) {
            for (int i = 0; i < students.size(); i++)
                if (students.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                    search_vals.add(students.get(i));
        }
    }

    @Override
    public int getCount() {
        return (students.size() > 0 && search.length() > 0) ? search_vals.size() : students.size();
    }

    @Override
    public Object getItem(int pos) {
        return (students.size() > 0 && search.length() > 0) ? search_vals.get(pos) : students.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to textview from list
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_grid_attendance, null);
        final LinearLayout ll = (LinearLayout) convertView.findViewById(R.id.ll_home_grid_attendance);
        final ImageView img_student = (ImageView) convertView.findViewById(R.id.img_home_grid_attendance_student);
        final ImageView img_status = (ImageView) convertView.findViewById(R.id.img_home_grid_attendance_status);
        final ImageView img_home_list_student_status = (ImageView) convertView.findViewById(R.id.img_home_list_student_status);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_home_grid_attendance_name);

        final TextView txt_img = (TextView) convertView.findViewById(R.id.txt_img);

        // check image is present or not
        Log.e("data_list", getItem(pos).toString());
        if (!getItem(pos).toString().contains("http")) {
            txt_img.setVisibility(View.VISIBLE);
            String[] arr_name = getItem(pos).toString().split(";");
            String first_let = "" + arr_name[1].charAt(0);
            txt_img.setText(first_let);
        }

        // ste name to name textview
        name.setText(getItem(pos).toString().split(";")[1]);
        String sts = getItem(pos).toString().split(";")[4];
        img_home_list_student_status.setVisibility(sts.equals("true") ? View.VISIBLE : View.GONE);

        // if image of student present in list then set image url to imageview
        Glide.with(context).load(getItem(pos).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_student) {
            @Override
            protected void setResource(Bitmap resource) {
                if (resource != null && img_student != null) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img_student.setImageDrawable(circularBitmapDrawable);
                }
            }
        });


        Glide.with(context).load(R.drawable.chk1).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_status) {
            @Override
            protected void setResource(Bitmap resource) {
                if (resource != null && img_status != null) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img_status.setImageDrawable(circularBitmapDrawable);
                }
            }
        });

        // image student is selected then visible select


        if(Status==true){
            img_status.setVisibility(sel_students.contains(getItem(pos).toString()) ? View.VISIBLE : View.GONE);

        }else {
            String SelectStatus=getItem(pos).toString().split(";")[4];
            img_status.setVisibility(SelectStatus.equals("true") ? View.VISIBLE : View.GONE);
        }

        // this action when click on student
        // if student selected already then un select when click
        // if student not selected then select student on click

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_status.setVisibility(img_status.getVisibility() != View.VISIBLE ? View.VISIBLE : View.GONE);
                if (!sel_students.contains(getItem(pos).toString()))
                    sel_students.add(getItem(pos).toString());
                else if (sel_students.contains(getItem(pos).toString()))
                    sel_students.remove(getItem(pos).toString());
            }
        });

        return convertView;
    }
}
