package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;

import java.util.ArrayList;

/**
 * Created by bhupen on 1/5/17.
 */


public class ManageRoomsAdapter extends BaseAdapter {

    HomeActivity context;
    ArrayList<String> vals = new ArrayList<String>();
    ArrayList<String> search_vals = new ArrayList<String>();
    String search = "";
    Typeface tf_awsome, tf_material;


    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show manage rooms.
    //
    public ManageRoomsAdapter(HomeActivity context, ArrayList<String> vals) {
        this.context = context;
        this.vals = vals;
        this.search = search;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");

        if (vals.size() > 0 && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                    search_vals.add(vals.get(i));
        }
    }

    @Override
    public int getCount() {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.size() : vals.size() + 1;
    }

    @Override
    public Object getItem(int pos) {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.get(pos) : pos < vals.size() ? vals.get(pos) : "0;Room Name";
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to views come from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_list_manage_room, null);
        final String room_id = getItem(pos).toString().split(";")[0];
        final TextView name = (TextView) convertView.findViewById(R.id.txt_home_list_manage_room_name);
        name.setText(getItem(pos).toString().split(";")[1]);
        return convertView;
    }
}
