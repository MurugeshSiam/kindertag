package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindertag.kindertag.BuildConfig;
import com.kindertag.kindertag.R;

/**
 * Created by bhupen on 1/5/17.
 */


public class HelpListAdapter extends BaseAdapter {
    String[] vals;
    Context context;

    // write constructor to call from activity
    // and receive all data data came from activity
    public HelpListAdapter(Context context, String[] vals) {
        this.context = context;
        this.vals = vals;
    }

    @Override
    public int getCount() {
        return vals.length;
    }

    @Override
    public Object getItem(int pos) {
        return vals[pos];
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {
        // Inflat item layout find views by id
        // set data to textview from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_help_list, null);
        final TextView type = (TextView) convertView.findViewById(R.id.txt_nav_menu_help);
        type.setText(getItem(pos).toString());
        if (type.getText().toString().equalsIgnoreCase("App version"))
            type.setText(getItem(pos).toString() + (getItem(pos).toString().equalsIgnoreCase("App version") ? "\n" + BuildConfig.VERSION_NAME : ""));//context.getPackageManager().getPackageInfo(context.getPackageName(),0).versionName:""));
        return convertView;
    }
}
