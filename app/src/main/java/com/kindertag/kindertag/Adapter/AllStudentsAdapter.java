package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bhupen on 1/5/17.
 */


public class AllStudentsAdapter extends BaseAdapter {

    // Declaration
    Context context;
    ArrayList<String> vals = new ArrayList<String>();
    ArrayList<String> search_vals = new ArrayList<String>();
    Typeface tf_awsome, tf_material;
    String room_id;
    String search = "";

    // write constructor to call from activity
    // and receive all data data came from activity
    public AllStudentsAdapter(Context context, String room_id, ArrayList<String> vals, String search) {
        this.context = context;
        this.vals = vals;
        this.room_id = room_id;
        this.search = search;
        Collections.sort(this.vals, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].compareTo(t1.split(";")[1]);
            }
        });
        if (vals.size() > 0 && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                    search_vals.add(vals.get(i));
        }

        // set font to textviews
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
    }

    @Override
    public int getCount() {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.size() : vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.get(pos) : vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    //  write getView method to set
    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_all_student, null);
        final String student_id = getItem(pos).toString().split(";")[0];
        final ImageView img_student = (ImageView) convertView.findViewById(R.id.img_grid_all_student);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_home_grid_all_student_name);
        final TextView action = (TextView) convertView.findViewById(R.id.txt_home_grid_all_student_action);

        // set all to data to each row item
        name.setText(getItem(pos).toString().split(";")[1]);//.substring(0,getItem(pos).toString().split(";")[1].split(" ")[0].length()+2)+".");
        action.setText(getItem(pos).toString().split(";")[4].equals(room_id) ? "Remove" : "Add");
        action.setVisibility(!getItem(pos).toString().split(";")[4].equals(room_id) ? View.VISIBLE : View.INVISIBLE);

        // set image url to imageview
        Glide.with(context).load(getItem(pos).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_student) {
            @Override
            protected void setResource(Bitmap resource) {
                if (resource != null && img_student != null) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img_student.setImageDrawable(circularBitmapDrawable);
                }
            }
        });

        action.setVisibility(View.GONE);

        // set click listener on img_student
        img_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String txt = action.getText().toString();
                RequestParams params = new RequestParams();
                params.add("student_id", student_id);
                if (txt.equalsIgnoreCase("Add"))
                    params.add("room_id", room_id);
                Common.callWebService((HomeActivity) context, txt.equalsIgnoreCase("Remove") ? Common.wb_removeroom : Common.wb_addstudentroom, params, txt + "StudentRoom" + "@" + student_id + "@" + room_id + "@" + Common.toString(vals));

            }
        });

        return convertView;
    }
}
