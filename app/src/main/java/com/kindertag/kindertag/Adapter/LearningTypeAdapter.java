package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindertag.kindertag.R;

/**
 * Created by bhupen on 1/5/17.
 */


public class LearningTypeAdapter extends BaseAdapter {

    String[] vals;
    Context context;
    int sel_pos = 0;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show learning type it showing in learning fragment when add activity.
    //
    public LearningTypeAdapter(Context context, String[] vals, int sel_pos) {
        this.context = context;
        this.vals = vals;
        this.sel_pos = sel_pos;
    }

    @Override
    public int getCount() {
        return vals.length;
    }

    @Override
    public Object getItem(int pos) {
        return vals[pos];
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {


        // Inflate item layout find views by id
        // set data to views come from list
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_learning_type, null);
        convertView.setBackgroundResource(sel_pos == pos ? R.color.white : R.color.transparent);
        final TextView type = (TextView) convertView.findViewById(R.id.txt_learning_type);
        type.setText(getItem(pos).toString());
        return convertView;
    }
}
