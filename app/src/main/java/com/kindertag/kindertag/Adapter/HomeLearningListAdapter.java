package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;

import java.util.ArrayList;

/**
 * Created by bhupen on 1/5/17.
 */

public class HomeLearningListAdapter extends BaseAdapter {


    ArrayList<String> vals = new ArrayList<String>();
    Context context;
    Typeface tf_awsome, tf_material;


    // write constructor to call from activity
    // and receive all data data came from activity

    public HomeLearningListAdapter(Context context, ArrayList<String> vals, GridView list, TextView empty) {
        this.context = context;
        this.vals = vals;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        list.setVisibility(vals.size() > 0 ? View.VISIBLE : View.GONE);
        empty.setText("No Recent Leanrnings");
        empty.setVisibility(vals.size() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getCount() {
        return vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }


    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to textview from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_list_learning, null);
        final TextView category = (TextView) convertView.findViewById(R.id.txt_home_list_learning_category);
        final ProgressBar progress = (ProgressBar) convertView.findViewById(R.id.progress_home_list_learning);
        final TextView txt_progress = (TextView) convertView.findViewById(R.id.txt_home_list_learning_progress);
        final TextView txt_scale = (TextView) convertView.findViewById(R.id.txt_home_list_learning_scale);
        category.setText(getItem(pos).toString().split(";")[0] + " :");
        String prog = Common.chkNull(getItem(pos).toString().split(";")[1], "");
        String scale = Common.chkNull(getItem(pos).toString().split(";")[2], "");
        txt_progress.setText(prog);
        txt_scale.setText(scale);
        int pro = prog.equalsIgnoreCase("Introduced") ? 25 : prog.equalsIgnoreCase("Observing") ? 50 : prog.equalsIgnoreCase("Developing") ? 75 : prog.equalsIgnoreCase("Mastered") ? 100 : 0;
        progress.setProgress(pro);
        return convertView;
    }
}
