package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bhupen on 1/5/17.
 */


public class HomeMessageListAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> vals = new ArrayList<String>();
    ArrayList<String> search_vals = new ArrayList<String>();
    String search = "";
    Typeface tf_awsome, tf_material;


    // write constructor to call from activity
    // and receive all data data came from activity
   // this adapter use to show messages send messages and receive messages
    public HomeMessageListAdapter(Context context, ArrayList<String> vals, String search, GridView list, TextView empty) {
        this.context = context;
        this.vals = vals;
        this.search = search;
        Log.i("Vals size", "" + vals.size());
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        Collections.sort(this.vals, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                try {
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                    return sf.parse(t1.split(";")[8].split(" ")[0]).compareTo(sf.parse(s.split(";")[8].split(" ")[0]));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        if (vals.size() > 0 && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                {
                    search_vals.add(vals.get(i));

                }else if(vals.get(i).split(";")[7].toLowerCase().contains(search.toLowerCase())){
                    search_vals.add(vals.get(i));

                }
        }
        list.setVisibility(vals.size() > 0 && (search.length() == 0 || (search.length() > 0 && search_vals.size() > 0)) ? View.VISIBLE : View.GONE);
        empty.setText("No Recent Message" + ((vals.size() > 0 && search.length() > 0 && search_vals.size() == 0) ? "\nas per search criteria" : ""));
        empty.setVisibility((vals.size() == 0 || (search.length() > 0 && search_vals.size() == 0)) ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getCount() {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.size() : vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.get(pos) : vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to textview from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_list_message, null);
        final ImageView img = (ImageView) convertView.findViewById(R.id.img_home_list_message);
        final TextView user = (TextView) convertView.findViewById(R.id.txt_home_list_message_user);
        final TextView desc = (TextView) convertView.findViewById(R.id.txt_home_list_message_desc);
        final TextView date = (TextView) convertView.findViewById(R.id.txt_home_list_message_date);
        final TextView txt_img = (TextView) convertView.findViewById(R.id.txt_img);

        // check image is present or not in list
        Log.e("data_list", getItem(pos).toString());
        if (!getItem(pos).toString().contains("http")) {
            txt_img.setVisibility(View.VISIBLE);
            String[] arr_name = getItem(pos).toString().split(";");
            String first_let = "" + arr_name[1].charAt(0);
            txt_img.setText(first_let);
        }


        // if sender or receiver image is present in list or not
        if (getItem(pos).toString().split(";")[5].equalsIgnoreCase("Teacher"))
            img.setImageResource(R.drawable.ic_local_library_white_24dp);
        else if (!Common.chkNull(getItem(pos).toString().split(";")[4], "").equals("")) {
            Glide.with(context).load(getItem(pos).toString().split(";")[4].replace("null", "")).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
                @Override
                protected void setResource(Bitmap resource) {

                    // if image of sender or receiver is present in then set image url ot imageview
                    if (resource != null && img != null) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img.setImageDrawable(circularBitmapDrawable);
                    }
                }
            });
        }

        user.setText(getItem(pos).toString().split(";")[1]);
        desc.setText(getItem(pos).toString().split(";")[7].replace("null", ""));
        date.setText(getItem(pos).toString().split(";")[8].replace("0000-00-00 00:00:00", ""));
        return convertView;
    }
}
