package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.Activity.WebActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by bhupen on 1/5/17.
 */


public class HomeActivityListAdapter extends BaseAdapter {
    ArrayList<String> vals = new ArrayList<String>();
    ArrayList<String> types = new ArrayList<String>();

    String main_type = "";
    String type;
    Context context;
    Typeface tf_awsome, tf_material;
    //final String types[]={"Note","Learning","Kudos","Photo"};
    boolean showStudents = false;
    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public HomeActivityListAdapter(Context context, String type, ArrayList<String> vals, boolean showStudents, GridView list, TextView empty) {
        this.context = context;
        this.type = type;
        this.vals = vals;
        this.showStudents = showStudents;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        Collections.sort(this.vals, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                try {
                    return sf.parse(t1.split(";")[1]).compareTo(sf.parse(s.split(";")[1]));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        if (!type.equalsIgnoreCase("All Activities"))
            for (int i = 0; i < vals.size(); i++) {
                final String atype = (vals.get(i).split(";")[2].toLowerCase().endsWith("s") ? vals.get(i).split(";")[2].toLowerCase().substring(0, vals.get(i).split(";")[2].toLowerCase().length() - 1) : vals.get(i).split(";")[2].toLowerCase()).replace("kudo", "praise").replace("_", " ");
                if (atype.equalsIgnoreCase(type))
                    types.add(vals.get(i));
            }
        list.setVisibility(vals.size() > 0 && (type.equalsIgnoreCase("All Activities") || (!type.equalsIgnoreCase("All Activities") && types.size() > 0)) ? View.VISIBLE : View.GONE);
        empty.setText("No Recent Activities" + (!type.equalsIgnoreCase("All Activities") && types.size() == 0 ? " for " + type : ""));
        empty.setVisibility(vals.size() == 0 || (!type.equalsIgnoreCase("All Activities") && types.size() == 0) ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getCount() {
        return !type.equalsIgnoreCase("All Activities") ? types.size() : vals.size();
    }//type.equalsIgnoreCase("All Activities")?12:3;}//students.size();}

    @Override
    public Object getItem(int pos) {
        return !type.equalsIgnoreCase("All Activities") ? types.get(pos) : vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_list_activity, null);
        final TextView txt_date = (TextView) convertView.findViewById(R.id.txt_home_list_activity_date);
        final TextView txt_type = (TextView) convertView.findViewById(R.id.txt_home_list_activity_type);
        final TextView txt_sym = (TextView) convertView.findViewById(R.id.txt_home_list_activity_type_sym);
        final TextView txt_notes = (TextView) convertView.findViewById(R.id.txt_home_list_activity_notes);
        final LinearLayout ll_learning = (LinearLayout) convertView.findViewById(R.id.ll_home_list_activity_learning);
        final TextView txt_category = (TextView) convertView.findViewById(R.id.txt_home_list_activity_learning_category);
        final ProgressBar progress_learning = (ProgressBar) convertView.findViewById(R.id.progress_home_list_activity_learning);
        final TextView txt_progress = (TextView) convertView.findViewById(R.id.txt_home_list_activity_learning_progress);
        //final GridView grid_photos = (GridView) convertView.findViewById(R.id.grid_home_list_activity_photos);
        final ImageView img_photo = (ImageView) convertView.findViewById(R.id.img_home_list_activity_photo);
        final VideoView new_activity_video = (VideoView) convertView.findViewById(R.id.new_activity_video);

        final LinearLayout ll_photos = (LinearLayout) convertView.findViewById(R.id.ll_home_list_activity_photos);
        final LinearLayout ll_students = (LinearLayout) convertView.findViewById(R.id.ll_home_list_activity_students);
        final ArrayList<String> students = ((HomeActivity) context).toList(getItem(pos).toString().split(";")[3]);
        final String atype = (getItem(pos).toString().split(";")[2].toLowerCase().endsWith("s") ? getItem(pos).toString().split(";")[2].toLowerCase().substring(0, getItem(pos).toString().split(";")[2].toLowerCase().length() - 1) : getItem(pos).toString().split(";")[2].toLowerCase()).replace("kudo", "praise");//type.equalsIgnoreCase("All Activities")?types[pos%4]:type;
        Log.i("atype", atype);
        try {
            txt_date.setText(/*new SimpleDateFormat(new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis())).equals(getItem(pos).toString().split(";")[1])?"HH:mm":"d-MMM")*/
                    new SimpleDateFormat("d-MMM\nHH:mm").format(sf.parse(getItem(pos).toString().split(";")[1])));
        } catch (ParseException e) {
            e.printStackTrace();
            txt_date.setText(new SimpleDateFormat("d-MMM\nHH:mm").format(new Date(System.currentTimeMillis())));
        }

        txt_date.setMaxLines(atype.equalsIgnoreCase("absent") ? 1 : 2);
        txt_date.setMinLines(atype.equalsIgnoreCase("absent") ? 1 : 2);
        txt_date.setPadding(txt_date.getPaddingLeft(), context.getResources().getDimensionPixelSize(atype.equalsIgnoreCase("absent") ? R.dimen.dp_10 : R.dimen.dp_5), txt_date.getPaddingRight(), txt_date.getPaddingBottom());
        txt_sym.setTypeface(tf_material);
        txt_type.setText(getFirstCaps(atype.replace("_", " ")));
        txt_sym.setText(context.getResources().getIdentifier("icon_" + atype.toLowerCase(), "string", context.getPackageName()) + "");
        txt_notes.setVisibility((getItem(pos).toString().split(";")[4].length() > 0 && !atype.matches("(absent|check_out|check_in)")) ? View.VISIBLE : View.GONE);
        ll_learning.setVisibility(atype.equalsIgnoreCase("learning") ? View.VISIBLE : View.GONE);

        Log.e("Type : ", getItem(pos).toString().split(";")[2]);
        main_type = getItem(pos).toString().split(";")[2].toLowerCase();


        if (main_type.equalsIgnoreCase("learning")) {
            //Log.i("notes",getItem(pos).toString().split(";")[4]);
            try {
                JSONObject learn = new JSONObject(getItem(pos).toString().split(";")[4].replace("a:4:{s:11:\"category_id\"", "{}"));
                txt_notes.setVisibility(Common.chkNull(learn, "note", "").length() > 0 ? View.VISIBLE : View.GONE);
                txt_notes.setText(Common.chkNull(learn, "note", ""));
                String scal = Common.chkNull(learn, "scale", "");
                txt_category.setText(scal + " | " + Common.chkNull(learn, "category_id", ""));
                String prog = Common.chkNull(learn, "progress", "");
                txt_progress.setText(prog);
                int progress = prog.equalsIgnoreCase("Introduced") ? 25 : prog.equalsIgnoreCase("Observing") ? 50 : prog.equalsIgnoreCase("Developing") ? 75 : prog.equalsIgnoreCase("Mastered") ? 100 : 0;
                progress_learning.setProgress(progress);
                progress_learning.setVisibility(progress > 0 ? View.VISIBLE : View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (main_type.equalsIgnoreCase("food")) {
            try {
                JSONObject food = new JSONObject(getItem(pos).toString().split(";")[4]);
                String meal_type = "";
                String food_consumption = "";
                try {
                    meal_type = food.getString("food_meal_type");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    food_consumption = food.getString("food_consumption");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String food_item_name = "";
                try {
                    food_item_name = food.getString("food_item_name").replace(" and",",");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                txt_notes.setText(meal_type + " | " + food_consumption + " | " + food_item_name);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (main_type.equalsIgnoreCase("potty")) {
            try {
                JSONObject food = new JSONObject(getItem(pos).toString().split(";")[4]);
                Log.e("Potty : ", food.toString());
                String potty_type = food.getString("potty_type");
                String potty_value = food.getString("potty_value");
                String note = "";
                try {
                    note = food.getString("note");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                txt_notes.setText(Html.fromHtml(potty_type + " : " + potty_value + "<br>" + note));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (main_type.equalsIgnoreCase("nap")) {
            try {
                JSONObject nap = new JSONObject(getItem(pos).toString().split(";")[4]);
                Log.e("nap : ", nap.toString());
                String naptime = nap.getString("naptime");
                String note = "";
                try {
                    note = nap.getString("note");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                txt_notes.setText(Html.fromHtml("<b>" + naptime + "</b>" + "<br>" + note));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            txt_notes.setText(Common.chkNull(getItem(pos).toString().split(";")[4], ""));
        }
      //  MediaController mediaController = new MediaController(context);

          // new_activity_video.setVisibility(!atype.matches("(absent|check_out|check_in|photo)") && !Common.chkNull(getItem(pos).toString().split(";")[6], "").equals("") ? View.VISIBLE : View.GONE);
   //     new_activity_video.setVisibility(View.VISIBLE);
String path1 ="http://kindertag.underdev.in/uploads/activity/SampleVideo_1280x720_1mb1536042465.mp4";
        Uri uri=Uri.parse(path1);
      //  new_activity_video.setMediaController(mediaController);
String videoPath=getItem(pos).toString().split(";")[6];
        new_activity_video.setVideoPath(videoPath);
        new_activity_video.start();
//new_activity_video.setVisibility(View.VISIBLE);
        //grid_photos.setAdapter(new HomeActivityPhotosGridAdapter(context,new ArrayList<String>()));
        //grid_photos.setVisibility(atype.equalsIgnoreCase("photo")?View.VISIBLE:View.GONE);
        img_photo.setVisibility(!atype.matches("(absent|check_out|check_in|photo)") && !Common.chkNull(getItem(pos).toString().split(";")[5], "").equals("") ? View.VISIBLE : View.GONE);
       //new_activity_video.setVisibility(!Common.chkNull(getItem(pos).toString().split(";")[6], "").equals("") ? View.VISIBLE : View.GONE);
        //ll_photos.setVisibility(atype.equalsIgnoreCase("photo")?View.VISIBLE:View.GONE);
  /* if(atype.equalsIgnoreCase("praise")){
    Log.i("photo_url",Common.chkNull(getItem(pos).toString().split(";")[5],"null"));
   }*/
        if (atype.equalsIgnoreCase("photo") && !Common.chkNull(getItem(pos).toString().split(";")[5], "").equals(""))
            setPhotos(ll_photos, Common.chkNull(getItem(pos).toString().split(";")[5], "").replace("[\"", "").replace("\"]", "").split("\",\""));
        else if (!atype.equalsIgnoreCase("photo") && !Common.chkNull(getItem(pos).toString().split(";")[5], "").equals("")) {
   /*final String path=Common.getImageFilePath(getItem(pos).toString().split(";")[5].split("/")[getItem(pos).toString().split(";")[5].split("/").length-1]);
   if(!path.equals(""))
    Glide.with(context).load(new File(path)).into(img_photo);
   else if(path.equals("")) {
    Glide.with(context).load(getItem(pos).toString().split(";")[5]).asBitmap().into(new SimpleTarget<Bitmap>() {
     @Override
     public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
      img_photo.setImageBitmap(resource);
      Log.i("url",getItem(pos).toString().split(";")[5]);
      Log.i("name",getItem(pos).toString().split(";")[5].split("/")[getItem(pos).toString().split(";")[5].split("/").length - 1]);
      Common.saveImage(resource, getItem(pos).toString().split(";")[5].split("/")[getItem(pos).toString().split(";")[5].split("/").length - 1]);
    }});//img_photo);
   }*/

            if (img_photo.getVisibility() == View.VISIBLE)
                Glide.with(context).load(getItem(pos).toString().split(";")[5]).into(img_photo);

            img_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //String path=Common.getImageFilePath(getItem(pos).toString().split(";")[5].split("/")[getItem(pos).toString().split(";")[5].split("/").length-1]);
                    //if(!path.equals("")) {
                    //Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(getItem(pos).toString().split(";")[5]));
                    //intent.setAction(Intent.ACTION_VIEW);
                    //intent.setDataAndType(Uri.fromFile(new File(path)),"image/*");
                    //context.startActivity(intent);
                    Intent ii = new Intent(context, WebActivity.class);
                    ii.putExtra("url", getItem(pos).toString().split(";")[5]);
                    context.startActivity(ii);
                    //}
                }
            });
        }

        if (showStudents)
            setStudents(ll_students, students);
        return convertView;
    }

    public void setStudents(final LinearLayout ll, ArrayList<String> students) {
        ll.setVisibility(View.VISIBLE);
        ll.removeAllViews();
        int max = students.size() > 6 ? 6 : students.size();
        for (int i = 0; i < max; i++) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(context.getResources().getDimensionPixelOffset(R.dimen.dp_30), context.getResources().getDimensionPixelOffset(R.dimen.dp_30));
            llp.setMargins(i > 0 ? context.getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, context.getResources().getDimensionPixelOffset(R.dimen.dp_5), i < max - 1 ? context.getResources().getDimensionPixelOffset(R.dimen.dp_5) : 0, context.getResources().getDimensionPixelOffset(R.dimen.dp_5));
            if (students.size() > 6 && i == 5) {
                TextView txt = new TextView(context);
                txt.setLayoutParams(llp);
                txt.setBackgroundResource(R.drawable.black_circle_back2);
                txt.setGravity(Gravity.CENTER);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                txt.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                txt.setTextColor(Color.WHITE);
                txt.setText(students.size() - 5 + "+");
                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Show Dialog HERE
                    }
                });
                ll.addView(txt);
            } else {
                final ImageView img = new ImageView(context);
                img.setLayoutParams(llp);

                img.setBackgroundResource(R.drawable.black_circle_back2);
                Glide.with(context).load(students.get(i).toString().split("#")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        if (resource != null && img != null) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            img.setImageDrawable(circularBitmapDrawable);
                        }
                    }
                });
                ll.addView(img);
            }
        }
    }

    public void setPhotos(final LinearLayout ll, final String[] urls) {
        ll.removeAllViews();
        int max = urls.length % 2 != 0 ? urls.length + 1 : urls.length;
        for (int i = 0, j = 0; i < (max / 2); i++) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_home_activity_photos, null);
            ImageView img = (ImageView) view.findViewById(R.id.img_home_activity_photos_photo1);
            ImageView img1 = (ImageView) view.findViewById(R.id.img_home_activity_photos_photo2);
            Glide.with(context).load(urls[j > 0 ? ++j : j].replaceAll("\\\\", "")).into(img);
            img.setTag(urls[j]);
            img1.setVisibility((!(urls.length % 2 != 0 && j == urls.length - 1)) ? View.VISIBLE : View.INVISIBLE);
            if (img1.getVisibility() == View.VISIBLE) {
                Glide.with(context).load(urls[++j].replaceAll("\\\\", "")).into(img1);
                img1.setTag(urls[j]);
            }

            View.OnClickListener photo_click = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.getVisibility() == View.VISIBLE) {
                        Intent ii = new Intent(context, WebActivity.class);
                        ii.putExtra("url", view.getTag().toString());
                        context.startActivity(ii);
                    }
                }
            };

            img.setOnClickListener(photo_click);
            img1.setOnClickListener(photo_click);
            ll.addView(view);
        }
        ll.setVisibility(View.VISIBLE);
    }


    public String getFirstCaps(String sr) {

        try {
            String str = sr;
            String[] strArray = str.split(" ");
            StringBuilder builder = new StringBuilder();
            for (String s : strArray) {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                builder.append(cap + " ");
            }
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
