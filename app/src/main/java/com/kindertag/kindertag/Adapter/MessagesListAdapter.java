package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kindertag.kindertag.R;

import java.util.ArrayList;

/**
 * Created by bhupen on 1/5/17.
 */


public class MessagesListAdapter extends BaseAdapter {

    ArrayList<String> vals = new ArrayList<String>();
    Context context;
    Typeface tf_awsome, tf_material;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show message list.
    //
    public MessagesListAdapter(Context context, ArrayList<String> vals) {
        this.context = context;
        this.vals = vals;

        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
    }

    @Override
    public int getCount() {
        return vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to views come from list
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_messages_list_message, null);
        final Boolean isSenderMe = Boolean.parseBoolean(vals.get(pos).split(";")[4]);
        final TextView date = (TextView) convertView.findViewById(R.id.txt_messages_list_messsage_date);
        final ImageView pic = (ImageView) convertView.findViewById(R.id.img_messages_list_messsage_user);
        final TextView msg = (TextView) convertView.findViewById(R.id.txt_messages_list_messsage);
        final TextView txt_messages_time = (TextView) convertView.findViewById(R.id.txt_messages_time);

        // Adjust messages and date text regarding messages.

        Log.e("datapos", vals.get(pos));
        date.setVisibility(View.GONE);

        if (pos == 0) {
            date.setVisibility(View.VISIBLE);
            date.setText(getItem(pos).toString().split(";")[2]);
        }

        if (pos != 0 && !getItem(pos).toString().split(";")[2].equalsIgnoreCase(getItem(pos - 1).toString().split(";")[2])) {
            date.setVisibility(View.VISIBLE);
            date.setText(getItem(pos).toString().split(";")[2]);
        }


        String[] time = getItem(pos).toString().split(";");
        String ft = time[time.length - 1];
        txt_messages_time.setText(ft);

        pic.setVisibility(!isSenderMe ? View.VISIBLE : View.GONE);
        msg.setMinWidth(parent.getWidth() / 4);
        msg.setMaxWidth((parent.getWidth() * 2) / 3);

        txt_messages_time.setMinWidth(parent.getWidth() / 4);
        txt_messages_time.setMaxWidth((parent.getWidth() * 2) / 3);

        ((RelativeLayout) msg.getParent()).setGravity(!isSenderMe ? Gravity.LEFT : Gravity.RIGHT);
        ((RelativeLayout) txt_messages_time.getParent()).setGravity(!isSenderMe ? Gravity.LEFT : Gravity.RIGHT);
        msg.setBackgroundResource(!isSenderMe ? R.drawable.msg_back : R.drawable.msg_back_self);
        msg.setTextColor(!isSenderMe ? Color.BLACK : Color.WHITE);
        msg.setText(getItem(pos).toString().split(";")[1]);

        return convertView;
    }
}
