package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bhupen on 1/5/17.
 */


public class ChildernListAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> vals = new ArrayList<String>();
    Typeface tf_awsome, tf_material;

    // write constructor to call from activity
    // and receive all data data came from activity

    public ChildernListAdapter(Context context, ArrayList<String> vals) {
        this.context = context;
        this.vals = vals;
        Collections.sort(this.vals, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].toLowerCase().compareTo(t1.split(";")[1].toLowerCase());
            }
        });
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
    }

    @Override
    public int getCount() {
        return vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {

        // Inflat item layout find views by id
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_list_students, null);
        final ImageView img_student = (ImageView) convertView.findViewById(R.id.img_home_list_student);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_home_list_student_name);
        final TextView txt_img = (TextView) convertView.findViewById(R.id.txt_img);
        name.setText(getItem(pos).toString().split(";")[1]);

        // set image url to imageview

        if (!getItem(pos).toString().split(";")[3].equals("")) {
            Glide.with(context).load(getItem(pos).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_student) {
                @Override
                protected void setResource(Bitmap resource) {
                    if (resource != null && img_student != null) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img_student.setImageDrawable(circularBitmapDrawable);
                    }
                }
            });
        }else {
            txt_img.setVisibility(View.VISIBLE);

            String N =getItem(pos).toString().split(";")[1];
            String s=N.substring(0,1);

            txt_img.setText(s);
        }
        return convertView;
    }
}
