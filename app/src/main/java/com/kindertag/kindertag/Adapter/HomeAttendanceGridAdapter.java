package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bhupen on 1/5/17.
 */


public class HomeAttendanceGridAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> vals = new ArrayList<String>();
    ArrayList<String> vals1 = new ArrayList<String>();
    ArrayList<String> att = new ArrayList<String>();
    ArrayList<String> att1 = new ArrayList<String>();
    ArrayList<String> search_vals = new ArrayList<String>();
    String search,sel = "";
    Typeface tf_awsome, tf_material;
    String type;
    Boolean SelectAction=true;

    Boolean status;
    String abs;

    public HomeAttendanceGridAdapter(Context context, ArrayList<String> vals, String sel, ArrayList<String> att, String search, GridView grid, TextView empty, boolean status) {
        this.context = context;
        this.vals = vals;
        this.att = att;
        this.att1 = att;
        this.search = search;
        this.sel = sel;
        this.status=status;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");

        ((HomeActivity) context).att_selected_std_id.clear();
        Collections.sort(this.vals, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.split(";")[1].compareTo(t1.split(";")[1]);
            }
        });
        if (vals.size() > 0 && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                    search_vals.add(vals.get(i));
        }
        grid.setVisibility(vals.size() > 0 && (search.length() == 0 || (search.length() > 0 && search_vals.size() > 0)) ? View.VISIBLE : View.GONE);
        empty.setText("No " + sel.substring(0, sel.length() - 1) + " is Added in the Room" + ((vals.size() > 0 && search.length() > 0 && search_vals.size() == 0) ? "\nas per search criteria" : ""));
        empty.setVisibility((vals.size() == 0 || (search.length() > 0 && search_vals.size() == 0)) ? View.VISIBLE : View.GONE);


        ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));

    }

    @Override
    public int getCount() {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.size() : vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.get(pos) : vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_grid_attendance, null);
        final LinearLayout ll = (LinearLayout) convertView.findViewById(R.id.ll_home_grid_attendance);
        final ImageView img_student = (ImageView) convertView.findViewById(R.id.img_home_grid_attendance_student);
        final ImageView img_status = (ImageView) convertView.findViewById(R.id.img_home_grid_attendance_status);
        final ImageView img_home_list_student_status = (ImageView) convertView.findViewById(R.id.img_home_list_student_status);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_home_grid_attendance_name);

        final TextView txt_img = (TextView) convertView.findViewById(R.id.txt_img);
        final TextView txt_absent = (TextView) convertView.findViewById(R.id.txt_absent);

        //Toast.makeText(context,Common.chkNull(getItem(pos).toString().split(";")[5],"no value"),Toast.LENGTH_LONG).show();

        Log.e("data_list", getItem(pos).toString());
        if (!getItem(pos).toString().contains("http")) {
            txt_img.setVisibility(View.VISIBLE);
            String[] arr_name = getItem(pos).toString().split(";");
            String first_let = "" + arr_name[1].charAt(0);
            txt_img.setText(first_let);
        }
        name.setText(getItem(pos).toString().split(";")[1]);//.substring(0,getItem(pos).toString().split(";")[1].split(" ")[0].length()+2)+".");
        Glide.with(context).load(getItem(pos).toString().split(";")[3]).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_student) {
            @Override
            protected void setResource(Bitmap resource) {
                if (resource != null && img_student != null) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img_student.setImageDrawable(circularBitmapDrawable);
                }
            }
        });
        Glide.with(context).load(R.drawable.chk1).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_status) {
            @Override
            protected void setResource(Bitmap resource) {
                if (resource != null && img_status != null) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img_status.setImageDrawable(circularBitmapDrawable);
                }
            }
        });
         type=sel;

        String s;
        if(type=="Teachers"){
            s=getItem(pos).toString().split(";")[6];
        }else {
            s=getItem(pos).toString().split(";")[4];

        }

        img_home_list_student_status.setVisibility(s.equals("true") ? View.VISIBLE : View.GONE);

        String Checkin;

        if(status==true){
            abs=getItem(pos).toString().split(";")[29];
            img_status.setVisibility(att.contains(getItem(pos).toString()) ? View.VISIBLE : View.GONE);
            txt_absent.setVisibility(abs.equals("0") ? View.GONE : View.VISIBLE);
if(att.contains(getItem(pos).toString())){
    if (!((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
        ((HomeActivity) context).att_selected_std_id.add(getItem(pos).toString().split(";")[0]);
        vals1.add(vals.get(pos).split(";")[4]+"#"+pos);
    } else if (((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
        ((HomeActivity) context).att_selected_std_id.remove(getItem(pos).toString().split(";")[0]);
        vals1.remove(vals.get(pos).split(";")[4]+"#"+pos);
    }


    String m="";
    for (int i=0;i<vals1.size();i++)
    {
        m=m +","+vals1.get(i);
    }

    if (m.contains("true") && !m.contains("false")) {
        ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));

        ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
        ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

    } else if (m.contains("false")  && !m.contains("true")) {
        ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

        ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
        ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

    } else if (m.contains("true") && m.contains("false")) {
        /*Select ALL*/
        ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
        ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
        ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
        ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
    }else {
        ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
        ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

    }
}
        }else if(status==false) {

            abs=getItem(pos).toString().split(";")[29];

            Checkin=getItem(pos).toString().split(";")[4];
            img_status.setVisibility(Checkin.equals("true") ? View.VISIBLE : View.GONE);
if(SelectAction==true&&(Checkin.equals("true"))){


                if (!((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
                    ((HomeActivity) context).att_selected_std_id.add(getItem(pos).toString().split(";")[0]);
                    vals1.add(vals.get(pos).split(";")[4]+"#"+pos);
                } else if (((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
                    ((HomeActivity) context).att_selected_std_id.remove(getItem(pos).toString().split(";")[0]);
                    vals1.remove(vals.get(pos).split(";")[4]+"#"+pos);
                }


            String m="";
            for (int i=0;i<vals1.size();i++)
            {
                m=m +","+vals1.get(i);
            }

            if (m.contains("true") && !m.contains("false")) {
                ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));

                ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
                ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

            } else if (m.contains("false")  && !m.contains("true")) {
                ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));

                ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
                ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

            } else if (m.contains("true") && m.contains("false")) {
                ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
            }else {
                ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));

            }
    SelectAction=false;
}
            txt_absent.setVisibility(abs.equals("0") ? View.GONE : View.VISIBLE);

        }


        // Toast.makeText(context,type,Toast.LENGTH_LONG).show();
        /*if(type=="Teachers"){
             abs=getItem(pos).toString().split(";")[8];
            img_status.setVisibility(att.contains(getItem(pos).toString().split(";")[0]) ? View.VISIBLE : View.GONE);

        }else {
               abs=getItem(pos).toString().split(";")[29];
               img_status.setVisibility(att.contains(getItem(pos).toString()) ? View.VISIBLE : View.GONE);

        }*/
//        img_home_list_student_status.setVisibility(att.contains(getItem(pos).toString().split(";")[0]) ? View.VISIBLE : View.GONE);
       // img_status.setVisibility(att.contains(getItem(pos).toString().split(";")[0]) ? View.VISIBLE : View.GONE);
     //   img_status.setVisibility(att.contains(getItem(pos).toString()) ? View.VISIBLE : View.GONE);

        if (pos == att.size() - 1) {
            att.clear();
        }

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

                img_status.setVisibility(img_status.getVisibility() != View.VISIBLE ? View.VISIBLE : View.GONE);
                if(type=="Teachers"){
                    if (!((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
                        ((HomeActivity) context).att_selected_std_id.add(getItem(pos).toString().split(";")[0]);
                        vals1.add(vals.get(pos).split(";")[6]+"#"+pos);
                    } else if (((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
                        ((HomeActivity) context).att_selected_std_id.remove(getItem(pos).toString().split(";")[0]);
                        vals1.remove(vals.get(pos).split(";")[6]+"#"+pos);
                    }

                }else {
                    if (!((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
                        ((HomeActivity) context).att_selected_std_id.add(getItem(pos).toString().split(";")[0]);
                        vals1.add(vals.get(pos).split(";")[4]+"#"+pos);
                    } else if (((HomeActivity) context).att_selected_std_id.contains(getItem(pos).toString().split(";")[0])) {
                        ((HomeActivity) context).att_selected_std_id.remove(getItem(pos).toString().split(";")[0]);
                        vals1.remove(vals.get(pos).split(";")[4]+"#"+pos);
                    }

                }

                String m="";
                for (int i=0;i<vals1.size();i++)
                {
                    m=m +","+vals1.get(i);
                }

                if (m.contains("true") && !m.contains("false")) {
                    ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));

                    ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
                    ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

                } else if (m.contains("false")  && !m.contains("true")) {
                    ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

                    ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));
                    ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue));

                } else if (m.contains("true") && m.contains("false")) {
                    ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                }else {
                    ((HomeActivity) context).txt_attendance_chk_in.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_home_attendance_chk_absent.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_attendance_chk_out.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));
                    ((HomeActivity) context).txt_home_attendance_chk_move.setBackgroundColor(context.getResources().getColor(R.color.thene_blue_light));

                }
//                  if (!att.contains(getItem(pos).toString().split(";")[0]))
//                    att.add(getItem(pos).toString().split(";")[0]);
//                else if (att.contains(getItem(pos).toString().split(";")[0]))
//                    att.remove(getItem(pos).toString().split(";")[0]);
            }
        });
        return convertView;
    }

}
