package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kindertag.kindertag.Activity.HomeActivity;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.Utils.Common;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

public class HomeRoomListAdapter extends BaseAdapter {

    HomeActivity context;
    ArrayList<String> vals = new ArrayList<String>();
    ArrayList<String> search_vals = new ArrayList<String>();
    String search = "";
    Typeface tf_awsome, tf_material;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show all rooms.
    //
    public HomeRoomListAdapter(HomeActivity context, ArrayList<String> vals, String search, GridView list, TextView empty) {
        this.context = context;
        this.vals = vals;
        this.search = search;
        tf_awsome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");

        if (vals.size() > 0 && search.length() > 0) {
            for (int i = 0; i < vals.size(); i++)
                if (vals.get(i).split(";")[1].toLowerCase().contains(search.toLowerCase()))
                    search_vals.add(vals.get(i));
        }

        // check room list size
        // if room list is empty then show text No room is added.
        // and visible add room text
        list.setVisibility(vals.size() > 0 && (search.length() == 0 || (search.length() > 0 && search_vals.size() > 0)) ? View.VISIBLE : View.GONE);
        empty.setText("No Room is Added" + ((vals.size() > 0 && search.length() > 0 && search_vals.size() == 0) ? "\nas per search criteria" : ""));
        empty.setVisibility((vals.size() == 0 || (search.length() > 0 && search_vals.size() == 0)) ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getCount() {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.size() : vals.size() + 1;
    }

    @Override
    public Object getItem(int pos) {
        return (vals.size() > 0 && search.length() > 0) ? search_vals.get(pos) : pos < vals.size() ? vals.get(pos) : "0;Room Name";
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to views come from list
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_home_list_room, null);
        final String room_id = getItem(pos).toString().split(";")[0];
        final LinearLayout ll_room = (LinearLayout) convertView.findViewById(R.id.ll_home_list_room_details);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_home_list_room_name);
        final TextView edit = (TextView) convertView.findViewById(R.id.txt_home_list_roon_edit);
        final TextView delete = (TextView) convertView.findViewById(R.id.txt_home_list_room_delete);
        final LinearLayout ll_update_room = (LinearLayout) convertView.findViewById(R.id.ll_home_list_room_update);
        final EditText edit_room = (EditText) convertView.findViewById(R.id.edit_home_list_room_name);
        final TextView save = (TextView) convertView.findViewById(R.id.txt_home_list_room_save);
        final TextView close = (TextView) convertView.findViewById(R.id.txt_home_list_room_close);

        // set buttons to every room row
        // edit, delete buttons
        edit.setTypeface(tf_material);
        delete.setTypeface(tf_material);
        save.setTypeface(tf_material);
        close.setTypeface(tf_material);
        edit.setText(R.string.icon_edit);
        delete.setText(R.string.icon_delete);
        save.setText(R.string.icon_save);
        close.setText(R.string.icon_close);

        // set room name to name field
        name.setText(getItem(pos).toString().split(";")[1]);
        edit_room.setHint(pos < vals.size() ? "Room Name" : "Add A Room");
        edit_room.setText(pos < vals.size() ? getItem(pos).toString().split(";")[1] : "");
        close.setVisibility(pos < vals.size() ? View.VISIBLE : View.INVISIBLE);
        ll_room.setVisibility(pos < vals.size() ? View.VISIBLE : View.GONE);
        ll_update_room.setVisibility(pos < vals.size() ? View.GONE : View.VISIBLE);

        // set click listener on edit button
        // when click on edit button
        // make textview is editable
        // hide edit and delete buttons and show close and save buttons
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_room.setVisibility(View.GONE);
                ll_update_room.setVisibility(View.VISIBLE);
            }
        });

        // click on delete button
        // perform call delete room webservice and pass room_id as parameter
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // show confirmation dialog to delete room
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete Room");
                builder.setMessage("Are you sure you want to Delete this Room?");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                // if click on delete then call delete room webservice
                // passing parameter room_id
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        context.dialog = dialogInterface;
                        RequestParams params = new RequestParams();
                        params.add("room_id", getItem(pos).toString().split(";")[0]);
                        Common.callWebService(context, Common.wb_deleteroom, params, "DeleteRoom" + "%" + pos);
                    }
                });
                builder.create().show();
            }
        });

        // when click on close visible edit and delete and invisible save and close
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ll_update_room.setVisibility(View.GONE);
                ll_room.setVisibility(View.VISIBLE);
            }
        });

        // action perform edit call edit room webservice
        // passing parameter roomname and room id
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.validate(edit_room, 1, true)) {
                    RequestParams params = new RequestParams();
                    params.add(pos < vals.size() ? "room_id" : "user_id", pos < vals.size() ? getItem(pos).toString().split(";")[0] : context.uid);
                    params.add("roomName", edit_room.getText().toString());
                    params.add("roomAddress", "");
                    Common.callWebService(context, pos < vals.size() ? Common.wb_editroom : Common.wb_addroom, params, pos < vals.size() ? "EditRoom" + "%" + getItem(pos).toString().split(";")[0] + ";" + edit_room.getText().toString() + "%" + pos : "AddRoom" + "%" + edit_room.getText().toString());
                }
            }
        });

        // click on name show student list in that room
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity) context).sel_room = getItem(pos).toString();
                ((HomeActivity) context).onBackPressed();
            }
        });
        return convertView;
    }
}
