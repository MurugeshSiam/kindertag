package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindertag.kindertag.R;

import java.util.ArrayList;

/**
 * Created by bhupen on 1/5/17.
 */


public class NapValsAdapter extends BaseAdapter {

    ArrayList<String> vals;
    Context context;
    String sel = "";
    String search = "";
    ArrayList<String> search_vals = new ArrayList<String>();
    Typeface tf_awesome, tf_material;
    boolean isCat = false;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show nap values it showing in AddNap fragment when add Nap activity.
    //
    public NapValsAdapter(Context context, String search, boolean isCat, ArrayList<String> vals, String sel) {
        this.context = context;
        this.search = search;
        this.vals = vals;
        this.sel = sel;
        tf_awesome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        tf_material = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");

    }

    @Override
    public int getCount() {/*return(isCat && search.length()>0)?search_vals.size():*/
        return vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return /*(isCat && search.length()>0)?(search_vals.get(pos).contains(";")?search_vals.get(pos).split(";")[1]:search_vals.get(pos)):vals.get(pos).contains(";")?vals.get(pos).split(";")[1]:*/vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to views come from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_learning_vals, null);
        final TextView chk = (TextView) convertView.findViewById(R.id.txt_learning_vals_chk);
        final TextView name = (TextView) convertView.findViewById(R.id.txt_learning_vals_name);
        chk.setTypeface(tf_material);
        chk.setText(R.string.icon_chk);
        name.setText(getItem(pos).toString());
        chk.setTextColor(context.getResources().getColor(sel.contains(getItem(pos).toString()) ? R.color.thene_blue : R.color.tab_txt));
        name.setTextColor(context.getResources().getColor(sel.contains(getItem(pos).toString()) ? R.color.black : R.color.tab_txt));
        return convertView;
    }
}
