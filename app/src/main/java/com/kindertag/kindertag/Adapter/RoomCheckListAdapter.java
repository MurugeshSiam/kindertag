package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kindertag.kindertag.R;

import java.util.ArrayList;

/**
 * Created by bhupen on 1/5/17.
 */


public class RoomCheckListAdapter extends BaseAdapter {
    ArrayList<String> vals;
    Context context;

    // write constructor to call from activity
    // and receive all data data came from activity
    //
    public RoomCheckListAdapter(Context context, ArrayList<String> vals) {
        this.context = context;
        this.vals = vals;
    }

    @Override
    public int getCount() {
        return vals.size();
    }

    @Override
    public Object getItem(int pos) {
        return vals.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        // Inflate item layout find views by id
        // set data to views come from list

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.view_room_check, null);
        final TextView rooms = (TextView) convertView.findViewById(R.id.txt_nav_menu_room_check_rooms);
        final TextView students = (TextView) convertView.findViewById(R.id.txt_nav_menu_room_check_students);
        final TextView teachers = (TextView) convertView.findViewById(R.id.txt_nav_menu_room_check_teachers);
        final TextView ratio = (TextView) convertView.findViewById(R.id.txt_nav_menu_room_check_ratio);

        rooms.setTextColor(context.getResources().getColor(pos > 0 ? R.color.black : R.color.colorPrimaryDark));
        students.setTextColor(context.getResources().getColor(pos > 0 ? R.color.black : R.color.colorPrimaryDark));
        teachers.setTextColor(context.getResources().getColor(pos > 0 ? R.color.black : R.color.colorPrimaryDark));
        ratio.setTextColor(context.getResources().getColor(pos > 0 ? R.color.black : R.color.colorPrimaryDark));

        rooms.setTypeface(Typeface.DEFAULT, pos > 0 ? Typeface.NORMAL : Typeface.BOLD);
        students.setTypeface(Typeface.DEFAULT, pos > 0 ? Typeface.NORMAL : Typeface.BOLD);
        teachers.setTypeface(Typeface.DEFAULT, pos > 0 ? Typeface.NORMAL : Typeface.BOLD);
        ratio.setTypeface(Typeface.DEFAULT, pos > 0 ? Typeface.NORMAL : Typeface.BOLD);

        rooms.setText(getItem(pos).toString().split(";")[1]);
        students.setText(getItem(pos).toString().split(";")[2]);
        teachers.setText(getItem(pos).toString().split(";")[3]);
        ratio.setText(getItem(pos).toString().split(";")[4]);

        return convertView;
    }
}
