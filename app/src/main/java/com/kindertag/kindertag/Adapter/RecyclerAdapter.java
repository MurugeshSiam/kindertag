package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.kindertag.kindertag.Fragment.AddEventFragment;
import com.kindertag.kindertag.R;
import com.kindertag.kindertag.module.New_items;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    ArrayList<New_items> arr_name = new ArrayList<>();
    Context context;

    // write constructor to call from activity
    // and receive all data data came from activity
    // this adapter use to show room list in dialog with chekc box.
    //
    public RecyclerAdapter(Context context,ArrayList<New_items> arr_name) {

        this.arr_name = arr_name;
        this.context=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_spinner_row;
        CheckBox checkbox;

        public MyViewHolder(View view) {
            super(view);
            txt_spinner_row = (TextView) view.findViewById(R.id.txt_spinner_row);
            checkbox = (CheckBox) view.findViewById(R.id.checkbox);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spin_row_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return arr_name.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        // set check box for every room
        // if select any room then set check checkbox
        // if not select room unselect check box

        New_items new_items=arr_name.get(position);
        holder.txt_spinner_row.setText(new_items.name);

        if (new_items.ischeck==1)
        {
            holder.checkbox.setChecked(true);

        }else {
            holder.checkbox.setChecked(false);
        }

        // set Listener on check box.
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && position==0)
                {
                  for (int i=0;i<arr_name.size();i++)
                  {
                      arr_name.get(i).ischeck=1;
                  }
                    AddEventFragment.arrindices.clear();
                    notifyDataSetChanged();

                }else if (!b && position==0){
                    for (int i=0;i<arr_name.size();i++)
                    {
                        arr_name.get(i).ischeck=0;
                    }
                    AddEventFragment.arrindices.clear();
                    notifyDataSetChanged();
                }else if (b && position!=0)
                {
                    arr_name.get(position).ischeck=1;

                }else if (!b && position!=0)
                {
                    arr_name.get(position).ischeck=0;
                }

            }
        });
    }
}
