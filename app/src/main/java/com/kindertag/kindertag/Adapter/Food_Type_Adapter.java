package com.kindertag.kindertag.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kindertag.kindertag.R;
import com.kindertag.kindertag.module.Item_Type;

import java.util.ArrayList;

public class Food_Type_Adapter extends RecyclerView.Adapter<Food_Type_Adapter.MyViewHolder> {

    ArrayList<Item_Type> arr_name = new ArrayList<>();
    Context context;
    String type;

    // write constructor to call from activity
    // and receive all data data came from activity

    public Food_Type_Adapter(Context context, ArrayList<Item_Type> arr_name, String type) {

        this.arr_name = arr_name;
        this.context = context;
        this.type = type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_food_meal, parent, false);

        MyViewHolder holder = new MyViewHolder(itemView);
        holder.setIsRecyclable(false);

        return holder;
    }

    @Override
    public int getItemCount() {
        return arr_name.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Item_Type item_food = arr_name.get(position);

        // get every position items in Item_Food and set to textveiw food name
        holder.txt_name.setText(item_food.getItem_name());

        if (item_food.isItem_isCheck()) {
            holder.txt_name.setTextColor(context.getResources().getColor(R.color.black));
            holder.txt_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_food_checked_24dp, 0, 0, 0);
        }

        // click on itemview set check and uncheck food item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < arr_name.size(); i++) {
                    arr_name.get(i).setItem_isCheck(false);
                }

                arr_name.get(position).setItem_isCheck(true);
                notifyDataSetChanged();
            }
        });

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name;

        public MyViewHolder(View view) {
            super(view);
            // find view declared in xml
            txt_name = (TextView) view.findViewById(R.id.txt_name);
        }
    }
}
